package com.fotonicia.widget;

import android.widget.ListAdapter;

import java.util.List;

public interface DemoAdapter extends ListAdapter {

  /*void appendItems(List<DemoItem> newItems);

  void setItems(List<DemoItem> moreItems);*/

  void appendItems(List<String> newItems);

  void setItems(List<String> moreItems);
}
