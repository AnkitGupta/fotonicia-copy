package com.fotonicia.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fotonicia.R;
import com.fotonicia.model.DemoItem;
import com.fotonicia.photoselection.SelectPhotoActivity;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Sample adapter implementation extending from AsymmetricGridViewAdapter<DemoItem> This is the
 * easiest way to get started.
 */
//public class DefaultListAdapter extends ArrayAdapter<DemoItem> implements DemoAdapter {
public class DefaultListAdapter extends ArrayAdapter<String> implements DemoAdapter {

  private final LayoutInflater layoutInflater;
  private List<String> CameraImages;

 public DefaultListAdapter(Context context, List<DemoItem> items) {
    super(context, items.size());
    layoutInflater = LayoutInflater.from(context);
  }

  public DefaultListAdapter(Context context) {
    super(context, 0);
    layoutInflater = LayoutInflater.from(context);
  }

  /*public DefaultListAdapter(Context context, List<String> cameraImages) {
    super(context, cameraImages.size());
    CameraImages = cameraImages;
    layoutInflater = LayoutInflater.from(context);
  }*/


  @Override
  public View getView(int position, View convertView, @NonNull ViewGroup parent) {
    View v;
    System.out.println("getView >> ");
    // DemoItem item = getItem(position);

    boolean isRegular = getItemViewType(position) == 0;

    if (convertView == null) {
      v = layoutInflater.inflate(
              isRegular ? R.layout.adapter_item : R.layout.adapter_item_odd, parent, false);
    } else {
      v = convertView;
    }

 /*   TextView textView;
    if (isRegular) {
      textView = (TextView) v.findViewById(R.id.textview);
    } else {
      textView = (TextView) v.findViewById(R.id.textview_odd);
    }

    textView.setText(String.valueOf(item.getPosition()));*/

    ImageView cameraImage;
    if (isRegular) {
      cameraImage = (ImageView) v.findViewById(R.id.showImg);
    } else {
      cameraImage = (ImageView) v.findViewById(R.id.showImg_odd);
    }
    String fileImage = null;
    File imageDir = null;
    fileImage = Environment.getExternalStorageDirectory().toString()
            + "/DCIM/Camera/" + CameraImages.get(position);
    imageDir = new File(fileImage);
    Bitmap bmp = convertToBitmap(imageDir);
    cameraImage.setImageBitmap(bmp);
    //cameraImage.setText(String.valueOf(item.getPosition()));

    return v;
  }

  @Override
  public int getViewTypeCount() {
    int size = CameraImages.size();
    return CameraImages.size();
  }

  @Override
  public int getItemViewType(int position) {
    return position % 2 == 0 ? 1 : 0;
  }

   /* public void appendItems(List<DemoItem> newItems) {
        addAll(newItems);
        notifyDataSetChanged();
    }*/

  public void appendItems(List<String> newItems) {
    addAll(newItems);
    notifyDataSetChanged();
  }

   /* public void setItems(List<DemoItem> moreItems) {
        clear();
        appendItems(moreItems);
    }*/

  public void setItems(List<String> moreItems) {
    clear();
    appendItems(moreItems);
  }

  public static Bitmap convertToBitmap(File file) {
    URL url = null;
    try {
      url = file.toURL();
    } catch (MalformedURLException e1) {
      //Log.d(TAG, e1.toString());
    }//catch

    Bitmap bmp = null;
    try {
      bmp = BitmapFactory.decodeStream(url.openStream());
      //bmp.recycle();
    } catch (Exception e) {
      //Log.d(TAG, "Exception: "+e.toString());
    }//catch
    return bmp;
  }//convertToBitmap

}