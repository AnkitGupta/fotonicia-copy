package com.fotonicia.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.fotonicia.R;


/**
 * Utility class to display in-app notifications.
 * 
 *
 * 
 */
public class NotificationUtils {

	// Only one object for progress dialog will be required in complete
	// application.
	private static ProgressDialog mProgressDialog;
    //private static ProgressView progressView ;

	/**
	 * This method shows the application toast messages
	 * 
	 * @param argContext
	 *            Context of the active activity instance.
	 * @param argDisplayText
	 *            Text to be displayed in the notification.
	 */
	public static void showNotificationToast(Context argContext,
			String argDisplayText) {
		try {
		Toast toast =	Toast.makeText(argContext, argDisplayText, Toast.LENGTH_LONG);
		//toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL,0,250);
		toast.show();
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
	}
	public static void showNetworkStatusToastAndReturn(Activity argContext) {
		
			try {
				Toast toast =	Toast.makeText(argContext, "No Internet Connection", Toast.LENGTH_LONG);
				//toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL,0,250);
				toast.show();
				/*Toast.makeText(argContext, "No network connection", Toast.LENGTH_LONG)
						.show();*/
			} catch (NullPointerException e) {
				// TODO: handle exception
			}
			return;
		
		
	}
	public static void showNetworkStatusToast(Activity argContext) {
		try {
			Toast toast =	Toast.makeText(argContext, "No Internet Connection", Toast.LENGTH_LONG);
			//toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL,0,250);
			toast.show();
			/*Toast.makeText(argContext, "No network connection", Toast.LENGTH_LONG)
					.show();*/
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
		
	}
	/*public static void showNotificationDialog(final android.support.v4.app.FragmentManager manager,String title,
			String argDisplayText) {


        Dialog.Builder  builder = new SimpleDialog.Builder(R.style.SimpleDialogLight){
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                super.onPositiveActionClicked(fragment);
            }

        };

        ((SimpleDialog.Builder)builder).message(argDisplayText)
                .title(title)
                .positiveAction("OK");
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(manager, null);

	}*/

	/**
	 * Shows an indeterminate progress dialog.
	 * 
	 * @param argContext
	 *            Context of the active activity instance.
	 * @param argTitle
	 *            Title of the dialog box.
	 * @param argMessage
	 *            Message to be displayed in the dialog box.
	 */
	public static void showProgressDialog(Context argContext, String argTitle,
										  String argMessage) {


		dismissProgressDialog();

		if (argContext != null)


            mProgressDialog = ProgressDialog.show(argContext, null, ""
					+ argMessage, true, false);

	}

	/**
	 * Dismisses open (if any) progress dialog box.
	 */
	public static void dismissProgressDialog() {
		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
	}
/*	public static void showCustomDialog(final Activity activity, String title, String message, String button, final boolean isFinishActivity, boolean isDialogCancelOnBack, boolean isDialogCancelOnOutsideTouch){
		final android.app.Dialog dialog = new android.app.Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layout_difficulty_popup);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
       	dialog.setCancelable(isDialogCancelOnBack);
		dialog.setCanceledOnTouchOutside(isDialogCancelOnOutsideTouch);
		CustomRegularTextView titleText = (CustomRegularTextView) dialog.findViewById(R.id.titleText);
		CustomRegularTextView messageText = (CustomRegularTextView) dialog.findViewById(R.id.messageText);
		CustomRegularTextView buttonText = (CustomRegularTextView) dialog.findViewById(R.id.gotItText);
		titleText.setText(title);
		messageText.setText(message);
		buttonText.setText(button);

		buttonText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if(isFinishActivity)
					activity.finish();
			}
		});
		dialog.show();

	}*/



	/*Logout dialog*/
/*	public static void showLogoutDialog(final Activity activity, String title, String message){
		final android.app.Dialog dialog = new android.app.Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_logout);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);
		CustomRegularTextView titleText = (CustomRegularTextView) dialog.findViewById(R.id.titleText);
		CustomRegularTextView messageText = (CustomRegularTextView) dialog.findViewById(R.id.messageText);
		CustomRegularTextView okText = (CustomRegularTextView) dialog.findViewById(R.id.okText);
		CustomRegularTextView cancelText = (CustomRegularTextView) dialog.findViewById(R.id.cancelText);
		titleText.setText(title);
		messageText.setText(message);

		okText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				Intent startMain = new Intent(activity,LoginActivity.class);
				SharedPreferenceUtil.savePreferences(activity, SharedPreferenceUtil.IS_LOGIN, false);
				*//*if(SharedPreferencesUtil.getPreferences(activity, Constants.LOGIN_TYPE, null).equals(Constants.REGISTRATION_FACEBOOK))
					LoginManager.getInstance().logOut();*//*
				activity.startActivity(startMain);
				activity.finish();

			}
		});
		cancelText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();

	}*/


	/*public static void showCustomDialogQuiz(final Activity activity, String title, String message, String button, final boolean isFinishActivity, boolean isDialogCancelOnBack, boolean isDialogCancelOnOutsideTouch){
		final android.app.Dialog dialog = new android.app.Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layout_difficulty_popup);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.setCancelable(isDialogCancelOnBack);
		dialog.setCanceledOnTouchOutside(isDialogCancelOnOutsideTouch);
		CustomBoldTextView titleText = (CustomBoldTextView) dialog.findViewById(R.id.titleText);
		CustomRegularTextView messageText = (CustomRegularTextView) dialog.findViewById(R.id.messageText);
		messageText.setGravity(Gravity.CENTER_VERTICAL);
		CustomLightTextView buttonText = (CustomLightTextView) dialog.findViewById(R.id.gotItText);
		titleText.setText(title);
		messageText.setText(message);
		buttonText.setText(button);

		buttonText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if(isFinishActivity)
					activity.finish();
			}
		});
		dialog.show();

	}

	public static void showBackDialog(final Activity activity, String title, String message){
		final android.app.Dialog dialog = new android.app.Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_logout);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);
		CustomBoldTextView titleText = (CustomBoldTextView) dialog.findViewById(R.id.titleText);
		CustomRegularTextView messageText = (CustomRegularTextView) dialog.findViewById(R.id.messageText);
		CustomBoldTextView okText = (CustomBoldTextView) dialog.findViewById(R.id.okText);
		CustomBoldTextView cancelText = (CustomBoldTextView) dialog.findViewById(R.id.cancelText);
		titleText.setText(title);
		messageText.setText(message);

		okText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				Intent startMain = new Intent(Intent.ACTION_MAIN);
				startMain.addCategory(Intent.CATEGORY_HOME);
				startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				activity.startActivity(startMain);
				activity.finish();

			}
		});
		cancelText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();

	}

	public static void showCustomDialogTwoButton(final Activity activity, String title, String message, String positiveButtonText, String negativeButtonText, final boolean isFinishActivity){
		final android.app.Dialog dialog = new android.app.Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_logout);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);
		CustomBoldTextView titleText = (CustomBoldTextView) dialog.findViewById(R.id.titleText);
		CustomRegularTextView messageText = (CustomRegularTextView) dialog.findViewById(R.id.messageText);
		CustomBoldTextView okText = (CustomBoldTextView) dialog.findViewById(R.id.okText);
		CustomBoldTextView cancelText = (CustomBoldTextView) dialog.findViewById(R.id.cancelText);
		titleText.setText(title);
		messageText.setText(message);
		okText.setText(positiveButtonText);
		cancelText.setText(negativeButtonText);
		okText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if(isFinishActivity)
					activity.finish();

			}
		});
		cancelText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();

	}
	public static void showRegistrationDialog(final Activity activity, String title, String message, String button, final boolean isFinishActivity, boolean isDialogCancelOnBack, boolean isDialogCancelOnOutsideTouch){
		final android.app.Dialog dialog = new android.app.Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.layout_register_popup);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.setCancelable(isDialogCancelOnBack);
		dialog.setCanceledOnTouchOutside(isDialogCancelOnOutsideTouch);
		CustomBoldTextView titleText = (CustomBoldTextView) dialog.findViewById(R.id.titleText);
		CustomRegularTextView messageText = (CustomRegularTextView) dialog.findViewById(R.id.messageText);
		CustomLightTextView buttonText = (CustomLightTextView) dialog.findViewById(R.id.gotItText);
		titleText.setText(title);
		messageText.setText(message);
		buttonText.setText(button);

		buttonText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if (isFinishActivity) {
					Intent intent = new Intent(activity, LoginActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
					activity.startActivity(intent);
					activity.finish();
				}
			}
		});
		dialog.show();

	}*/
}
