package com.fotonicia.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.Set;

public class PreferenceManager {
    private SharedPreferences pref;
    private Editor editor;
    private Context mContext;

    /*
    *  LastUpdated key for updating the master content in the database  */
    private String LAST_UPDATED = "last_updated_master_content";
    private final String PREF_NAME = "LOGIN_AUTHENTICATION";
    private String email = "email";
    private String password = "password";
    private String userid = "userid";
    private String username = "username";
    private String regId = "regId";
    private boolean isOTPVerified = false;
    private boolean isGPSStatus = false;
    private String guestStatus = "false";
    private String latitude = "latitude";
    private String longitude = "longitude";
    private String user_phone_number = "user_phone_number";

    public Set<String> getServiceList() {
        return pref.getStringSet("serviceList", null);
    }

    public void setServiceList(Set<String> serviceList) {
        editor.putStringSet("serviceList", serviceList);
        editor.commit();
    }





    /*private long currentLatitude=0L ;
        private long currentLongitude=0L;*/

	
	
	public String getUser_phone_number() {
        return pref.getString(user_phone_number, null);
    }

    public void setUser_phone_number(String user_phone_number)
    {
        editor.putString(this.user_phone_number,user_phone_number);
        editor.commit();
    }

    public boolean isGPSStatus() {
        return isGPSStatus;
    }

    public void setIsGPSStatus(boolean isGPSStatus) {
        this.isGPSStatus = isGPSStatus;
    }

    public boolean isOTPVerified() {
        return isOTPVerified;
    }

    public void setIsOTPVerified(boolean isOTPVerified) {
        this.isOTPVerified = isOTPVerified;
    }



    public PreferenceManager(Context context) {
        this.mContext = context;
        pref = mContext.getSharedPreferences(PREF_NAME, 0);
        editor = pref.edit();
    }

    public String getEmail() {
        return pref.getString(email, null);
    }

    public void setEmail(String email) {
        editor.putString(this.email, email);
        editor.commit();
    }

    public String getPassword() {
        return pref.getString(password, null);
    }

    public void setPassword(String password) {
        editor.putString(this.password, password);
        editor.commit();
    }

    public String getUserid() {
        return pref.getString(userid, null);
    }

    public void setUserid(String userid) {
        editor.putString(this.userid, userid);
        editor.commit();
    }

    public String getUsername() {
        return pref.getString(username, null);
    }

    public void setUsername(String username) {
        editor.putString(this.username, username);
        editor.commit();
    }


    public String getServiceResponse(String id) {
        String value = pref.getString(id, null);
        return value;
    }


    public void saveServiceResponse(String id, String message) {
        editor.putString(id, message);
        editor.commit();
    }

    public String getRegId() {
        return pref.getString(regId, null);
    }

    public void setRegId(String regId) {
        editor.putString(this.regId, regId);
        editor.commit();
    }



   /* public boolean setIsGuest(boolean isGuest) {
        editor.putBoolean(this.isGuest, isGuest);
        editor.commit();
    }*/


    public String getGuestStatus() {
        return pref.getString(guestStatus, null);
    }

    public void setGuestStatus(String guestStatus) {
        editor.putString(this.guestStatus, guestStatus);
        editor.commit();
    }



    public String getLatitude() {
        return pref.getString(latitude, null);
    }

    public void setLatitude(String latitude) {
        editor.putString(this.latitude, latitude);
        editor.commit();
    }

    public String getLongitude() {
        return pref.getString(longitude, null);
    }

    public void setLongitude(String longitude) {
        editor.putString(this.longitude, longitude);
        editor.commit();
    }

    public void clearApplication() {
	editor.clear();
        editor.putString(this.user_phone_number, null);
        editor.putString(this.email, null);
        editor.putBoolean(String.valueOf(this.isOTPVerified), false);
        editor.putString(this.password, "0");
        editor.putString(this.userid, null);
        editor.putString(this.username, null);
        editor.commit();
    }
}
