package com.fotonicia.util;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Outline;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final boolean isDebug = true;
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";
    /**
     * Utility method to check Internet connection availability
     *
     * @return boolean value indicating the presence of Internet connection
     * availability
     */
    public static JSONObject getDeviceInfo(Context mContext) {
        JSONObject deviceInfoJson = new JSONObject();

        SharedPreferences myPref = mContext.getSharedPreferences("Jamboree", Context.MODE_PRIVATE);
        try {
            TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);

            deviceInfoJson.put("device_token", /*myPref.getString(PROPERTY_REG_ID, "")*/"");
            deviceInfoJson.put("device_id", "" + telephonyManager.getDeviceId());
            deviceInfoJson.put("device_osversion", "" + Build.VERSION.RELEASE);
            deviceInfoJson.put("device_model", "" + Build.DEVICE);
            deviceInfoJson.put("device_name", "" + Build.PRODUCT);
            deviceInfoJson.put("app_version", "" + myPref.getInt(PROPERTY_APP_VERSION, 1));
            deviceInfoJson.put("device_type", "Android");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return deviceInfoJson;
    }

    static public int dp2px(Context context, float dp) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
        return Math.round(px);
    }

    @TargetApi(21)
    static public class ShadowOutline extends ViewOutlineProvider {

        int width;
        int height;

        public ShadowOutline(int width, int height) {
            this.width = width;
            this.height = height;
        }

        @Override
        public void getOutline(View view, Outline outline) {
            outline.setRect(0, 0, width, height);
        }
    }

    static DisplayMetrics getDisplayMetrics(final Context context) {
        final WindowManager
                windowManager =
                (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final DisplayMetrics metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }

    public static int dpToPx(final Context context, final float dp) {
        // Took from http://stackoverflow.com/questions/8309354/formula-px-to-dp-dp-to-px-android
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) ((dp * scale) + 0.5f);
    }

    public static void showToast(String msg , Context context)
    {
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

  /*  public static String generateMD5Key(String email, String password){

        String passwordToHash;
        if(password.equals(""))
                passwordToHash = email + Constants.SERVICE_SECURE_KEY;
        else
            passwordToHash = email + Constants.SERVICE_SECURE_KEY + password;

        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(passwordToHash.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        System.out.println(generatedPassword);
        return generatedPassword;
    }*/
    public static String getSHA512SecurePassword(String passwordToHash)
    {
        String generatedPassword = null;

        try {
            String salt = getSalt();
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt.getBytes());
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return generatedPassword;
    }



    public static String hashString(String password){
        String generatedString= null;
    try{
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(password.getBytes());

        byte byteData[] = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        System.out.println("Hex format : " + sb.toString());

        //convert the byte to hex format method 2
        StringBuffer hexString = new StringBuffer();
        for (int i=0;i<byteData.length;i++) {
            String hex= Integer.toHexString(0xff & byteData[i]);
            if(hex.length()==1) hexString.append('0');
            hexString.append(hex);
            generatedString= hexString.toString();
        }
        System.out.println("Hex format : " + hexString.toString());
    }
    catch (Exception e){
        e.printStackTrace();
    }
        return generatedString;
    }
    private static String getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt.toString();
    }

    public static boolean isNetworkAvailable(Activity argActivity) {
        if (argActivity == null) {
            return false;
        }

        ConnectivityManager connectivityManager;
        NetworkInfo activeNetworkInfo = null;
        try {
            connectivityManager = (ConnectivityManager) argActivity
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return activeNetworkInfo != null;
    }

    public final static boolean isValidMobile(String phone)
    {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    /**
     * Validates an email based on regex -
     * "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
     * "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
     *
     * @param email String containing email address
     * @return True if the email is valid; false otherwise.
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        try {
            // Initialize reg ex for email.
            String expression = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            CharSequence inputStr = email;
            // Make the comparison case-insensitive.
            Pattern pattern = Pattern.compile(expression,
                    Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(inputStr);
            if (matcher.matches()) {
                isValid = true;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return isValid;
    }
    public final static boolean isValidEmail(String target) {
        //if (target != null) {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        //}
         /*else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }*/
    }
    /**
     * Show device native message composer application.
     *
     * @param argContext
     * @param argEmail   email address. Currently only support one
     * @param argSubject email subject
     * @param argBody    email body
     */
    public static void showEmailComposer(Context argContext, String argEmail,
                                         String argSubject, String argBody) {
        try {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("plain/text");
            emailIntent.putExtra(Intent.EXTRA_EMAIL,
                    new String[]{argEmail});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT,
                    argSubject);
            emailIntent.putExtra(Intent.EXTRA_TEXT, argBody);
            argContext
                    .startActivity(Intent.createChooser(emailIntent, "Share via"));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    public static int getScreenWidth(Context context) {
        Resources res = context.getResources();
        DisplayMetrics metrics = res.getDisplayMetrics();
        int displayWidth = res.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? Math
                .max(metrics.widthPixels, metrics.heightPixels) : Math.min(
                metrics.widthPixels, metrics.heightPixels);
        return displayWidth;

    }

    public static int convertDensityPixelToPixel(Context context, int i) {
        return (int) ((i * context.getResources().getDisplayMetrics().density) + 0.5);
    }


    public static int convertPixelToDensityPixel(Context context, int px) {
        return (int) ((px / context.getResources().getDisplayMetrics().density) + 0.5);
    }

    public static LayoutParams getImageLayoutParam(Activity activity) {
        int width = Utils.getScreenWidth(activity);
        int margin = Utils.convertDensityPixelToPixel(activity, 50);
        int bookWidth = (width - margin) / 3;
        int bookHeight = (int) (bookWidth * 1.4);
        LayoutParams layoutParams = new LayoutParams(bookWidth, bookHeight);
        return layoutParams;
    }

    public static File getPdfFile(Activity mActivity, String filename) {
        File cacheDir = mActivity.getExternalCacheDir();
        File pdfFile = new File(cacheDir, filename + ".pdf");
        return pdfFile;

    }

    public static File getCoverImage(Activity mActivity, String filename) {
        File cacheDir = mActivity.getExternalCacheDir();
        File cuverImage = new File(cacheDir, filename + ".png");
        return cuverImage;
    }

    public static String getDecodedString(String text) {
        String name = "";
        try {
            name = new String(text.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        }

        String decodedName = Html.fromHtml(name).toString();
        return decodedName;

    }

    public static String getDateFromMilliseconds(long millisecond) {
        String resultDate = "Date not found";

        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                "MMMM dd,yyyy");
        //dateFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            Date newDate = new Date(millisecond * 1000);
            resultDate =  dateFormatter.format(newDate);
        return resultDate;

    }
    public static String getDateFromMillisecondsAttempts(long millisecond) {
        String resultDate = "Date not found";

        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                "dd MMM yyyy");                                    //2 Sep 2015
        Date newDate = new Date(millisecond);
        resultDate =  dateFormatter.format(newDate);
        return resultDate;

    }

    public static String getRemovedExtensionName(String name) {
        String baseName;
        if (name.lastIndexOf(".") == -1) {
            baseName = name;

        } else {
            int index = name.lastIndexOf(".");
            baseName = name.substring(0, index);
        }
        return baseName;
    }

    public static String combinedString(List<String> listUpdate, String regularExpression) {

        int size = listUpdate.size();
        int i = 0;
        String ss = "";
        Iterator itr = listUpdate.iterator();
        while (itr.hasNext()) {
            if (i < size - 1) {
                String s = (String) itr.next();
                ss = ss.concat(s + regularExpression); // "@#"
                i++;
            } else {
                String s = (String) itr.next();
                ss = ss.concat(s);
            }

        }
        System.out.println("combinedString=" + ss);
        return ss;

    }

    public static String currentDateFormat() {
        String resultDate = "Date not found";
        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                "yyyy-MM-dd");          //2015-06-09
        //dateFormatter.setTimeZone(TimeZone.getTimeZone("IST"));

        Date newDate = new Date(System.currentTimeMillis());
        resultDate = dateFormatter.format(newDate);
        return resultDate;

    }

    public static String getDateFormat(String dateFormat) {
        String resultDate = "Date not found";
        SimpleDateFormat sourceDateFormatter = new SimpleDateFormat(
                "MMM d, yyyy");
        try {
            Date newDate = sourceDateFormatter.parse(dateFormat);
            SimpleDateFormat dateFormatter = new SimpleDateFormat(
                    "yyyy-MM-dd");          //2015-06-09
            //dateFormatter.setTimeZone(TimeZone.getTimeZone("IST"));

            resultDate = dateFormatter.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return resultDate;

    }


    public static void printValue(String tag, String result) {
        if (isDebug)
            System.out.println(tag + "-->" + result);
    }

    public static void logVerbose(String tag, String result) {
        if (isDebug)
            Log.v(tag + "-->", result);
    }

    public static void logError(String tag, String result) {
        if (isDebug)
            Log.e(tag + "-->", result);
    }

    public static void logDebug(String tag, String result) {
        if (isDebug)
            Log.v(tag + "-->", result);
    }

    public static int getCurrentMonthDay() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.DAY_OF_MONTH);
    }

    public static int convertStringToInteger(String id, int defaultValue) {
        int numericId = 0;
        try {
            numericId = Integer.parseInt(id);
        } catch (NumberFormatException numberFormatException) {
            numericId = defaultValue;
        } catch (Exception e) {
            numericId = defaultValue;
        }
        return numericId;
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static boolean isPreviousDate(String previousDate, String currentDate) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                "yyyy-MM-dd");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = dateFormatter.parse(previousDate);
            d2 = dateFormatter.parse(currentDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d1.before(d2))
            return true;
        else
            return false;
    }

    public static String dayPreviousDateFormat() {
        String resultDate = "Date not found";
        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                "yyyy-MM-dd");          //2015-06-09
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        resultDate = dateFormatter.format(cal.getTime());
        return resultDate;
    }
    public static String getCityFromLocation(Context context, double latitude, double longitude){
        String cityName=null;
        try {
            Geocoder gcd = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = gcd.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0)
                //System.out.println(addresses.get(0).getLocality());
                cityName = addresses.get(0).getLocality();
        }
        catch(Exception exception){
            return cityName;
        }
        return cityName;
    }
    public static SpannableStringBuilder setTextsColorInsideString(String fulltext, String subtext, int foregroundColor, int backgroundColor) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(fulltext);
        Pattern p = Pattern.compile(subtext, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(fulltext);
        while (m.find()){
            spannableStringBuilder.setSpan(new ForegroundColorSpan(foregroundColor), m.start(), m.end(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            spannableStringBuilder.setSpan(new BackgroundColorSpan(backgroundColor), m.start(),m.end(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }
        return spannableStringBuilder;
    }
    public static String getTimeMillisecondToMM(long millisUntilFinished) {
        return String.format("%2d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)));
    }
    public static long calculateSeconds(int hours, int minutes){
        long totalSeconds = 0;
        totalSeconds = hours * (60*60) + minutes * (60);
        return totalSeconds;
    }
    public static String[] splitTextIntoArray(String content){

        String array[]=null;
        if(content!=null || !content.equals(""))
            array = content.split(",");
        return  array;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
