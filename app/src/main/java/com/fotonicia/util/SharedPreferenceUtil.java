package com.fotonicia.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by moumita on 3/8/2017.
 */

public class SharedPreferenceUtil {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // file name
    private static final String PREF_NAME = "spaceo-demo";
    private static final String IS_FIRST_TIME = "IsFirstTime";
    public static final int MODE_PRIVATE = 0;
    public static final String USER_NAME = "username";
    public static final String PASSWORD = "password";
    public static final String REG_ID = "registration_id";
    public static final String APP_VERSION = "appVersion";
    public static final String IS_LOGIN = "isLogin";

    public SharedPreferenceUtil(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME, true);
    }



    public static void savePreferences(Context context, String key,
                                       String value) {
        SharedPreferences sPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sPrefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void savePreferences(Context context, String key,
                                       Integer value) {
        SharedPreferences sPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sPrefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void savePreferences(Context context, String key,
                                       Boolean value) {
        SharedPreferences sPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sPrefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void deletePreferences(Context context, String key) {
        SharedPreferences sPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sPrefs.edit();
        editor.remove(key);
        editor.commit();
    }

    public static Boolean getPreferences(Context context, String key, boolean defValue) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        Boolean savedPref = sharedPreferences.getBoolean(key, defValue);
        return savedPref;
    }

    public static String getPreferences(Context context, String key, String defValue) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String savedPref = sharedPreferences.getString(key, defValue);
        return savedPref;
    }

    public static Integer getPreferences(Context context, String key, int defValue) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        Integer savedPref = sharedPreferences.getInt(key, defValue);
        return savedPref;
    }

    public static void clearAllSharedPreferencesList(Context context) {
        SharedPreferences sPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor sEdit = sPrefs.edit();
        sEdit.clear();
        sEdit.commit();
    }

}

