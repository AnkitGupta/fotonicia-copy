package com.fotonicia.category;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.fotonicia.R;
import com.fotonicia.adapter.AspectRatioAdapter;
import com.fotonicia.adapter.ItemSizesAdapter;
import com.fotonicia.adapter.SelectPhotoAdapter;
import com.fotonicia.model.ItemObjects;
import com.fotonicia.photoselection.DemoUtils;
import com.fotonicia.util.NotificationUtils;
import com.fotonicia.util.Utils;
import com.fotonicia.widget.DemoAdapter;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static android.content.ContentValues.TAG;

public class AspectRatioAndSizesActivity extends Activity implements View.OnClickListener {

    private Activity activity;
    private RecyclerView aspect_ratio_recycler_view, item_sizes_recycler_view;
    private GridLayoutManager lLayout;


    private AspectRatioAdapter aspectRatioAdapter;
    private ItemSizesAdapter itemSizesAdapter;
    private List<String> strAspectRatio = new ArrayList<>();
    private List<String> strItemSizes = new ArrayList<>();

    private RelativeLayout layItemSizes;
    private ImageView imgCancel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aspect_ratio_sizes);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.BOTTOM);
        activity = this;

/*
* WindowManager.LayoutParams params = getWindow().getAttributes();
params.x = ...;
params.y = ...;
params.width = ...;
params.height = ...;

this.getWindow().setAttributes(params);
* */
        final float scale = this.getResources().getDisplayMetrics().density;
        int pixels = (int) (200 * scale + 0.5f);

        RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, pixels);
        rel_btn.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        /*layItemSizes.setLayoutParams(rel_btn);
        layItemSizes.setScrollingEnabled(false);*/
        layItemSizes = (RelativeLayout) findViewById(R.id.layItemSizes);
        imgCancel = (ImageView) findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(this);
        /*layItemSizes.getLayoutParams().height = px;
        layItemSizes.getLayoutParams().width = px;*/

        aspect_ratio_recycler_view = (RecyclerView) findViewById(R.id.aspect_ratio_recycler_view);
        strAspectRatio.add("1:1");
        strAspectRatio.add("2:3");
        strAspectRatio.add("3:4");
        strAspectRatio.add("16:9");
        strAspectRatio.add("5:6");
        aspectRatioAdapter = new AspectRatioAdapter(strAspectRatio);
        RecyclerView.LayoutManager catLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        aspect_ratio_recycler_view.setLayoutManager(catLayoutManager);
        aspect_ratio_recycler_view.setItemAnimator(new DefaultItemAnimator());
        aspect_ratio_recycler_view.setAdapter(aspectRatioAdapter);
        aspect_ratio_recycler_view.setHasFixedSize(true);

        item_sizes_recycler_view = (RecyclerView) findViewById(R.id.item_sizes_recycler_view);
        strItemSizes.add("4x4");
        strItemSizes.add("8x10");
        strItemSizes.add("8x8");
        strItemSizes.add("12x15");
        strItemSizes.add("10x10");
        strItemSizes.add("20x20");
        strItemSizes.add("11x15");
        strItemSizes.add("15x20");
        strItemSizes.add("20x30");
        strItemSizes.add("4x4");
        strItemSizes.add("8x10");
        strItemSizes.add("8x8");
        strItemSizes.add("12x15");
        strItemSizes.add("10x10");
        strItemSizes.add("20x20");
        strItemSizes.add("11x15");
        strItemSizes.add("15x20");
        strItemSizes.add("20x30");
        lLayout = new GridLayoutManager(this, 2);
        item_sizes_recycler_view.setHasFixedSize(true);
        item_sizes_recycler_view.setLayoutManager(lLayout);
        itemSizesAdapter = new ItemSizesAdapter(getApplicationContext(), strItemSizes);
        item_sizes_recycler_view.setAdapter(itemSizesAdapter);

    }

    /*private List<ItemObjects> getListItemData() {
        List<ItemObjects> listViewItems = new ArrayList<ItemObjects>();
        listViewItems.add(new ItemObjects("Alkane", R.drawable.one));
        listViewItems.add(new ItemObjects("Ethane", R.drawable.two));
        listViewItems.add(new ItemObjects("Alkyne", R.drawable.three));
        listViewItems.add(new ItemObjects("Benzene", R.drawable.four));
        listViewItems.add(new ItemObjects("Amide", R.drawable.one));
        listViewItems.add(new ItemObjects("Amino Acid", R.drawable.two));
        listViewItems.add(new ItemObjects("Phenol", R.drawable.three));
        listViewItems.add(new ItemObjects("Carbonxylic", R.drawable.four));
        listViewItems.add(new ItemObjects("Nitril", R.drawable.one));
        listViewItems.add(new ItemObjects("Ether", R.drawable.two));
        listViewItems.add(new ItemObjects("Ester", R.drawable.three));
        listViewItems.add(new ItemObjects("Alcohol", R.drawable.four));

        return listViewItems;
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgCancel:
                Intent intent=new Intent();
                setResult(2,intent);
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_out_down);
                break;
        }

    }


}