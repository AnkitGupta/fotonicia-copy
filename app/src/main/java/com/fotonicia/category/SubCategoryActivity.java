package com.fotonicia.category;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.fotonicia.R;
import com.fotonicia.adapter.SubCategoryAdapter;
import com.fotonicia.model.SubCategoryDetails;
import com.fotonicia.util.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

public class SubCategoryActivity extends Activity implements View.OnClickListener {

    PreferenceManager preferenceManager;
    private Activity activity;
    private ImageView imgBack;
    private List<SubCategoryDetails> subCatDetails = new ArrayList<>();
    private RecyclerView recyclerView;
    private SubCategoryAdapter mAdapter;
    private RelativeLayout layBack;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        activity = this;
        preferenceManager = new PreferenceManager(getApplicationContext());

        setContentView(R.layout.activity_sub_category);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.layout_title_bar_subcat);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getContents();

    }

    private void getContents() {

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.subcat_recycler_view);
        mAdapter = new SubCategoryAdapter(this, subCatDetails);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setHasFixedSize(true);

        getSubCategoryItems();
    }

    private void getSubCategoryItems() {
        SubCategoryDetails det = new SubCategoryDetails("1", "METAL PRINT");
        subCatDetails.add(det);

        det = new SubCategoryDetails("2", "MAGNET PRINT");
        subCatDetails.add(det);

        det = new SubCategoryDetails("3", "PHOTO-PAPER PRINT");
        subCatDetails.add(det);

        det = new SubCategoryDetails("4", "POSTERS");
        subCatDetails.add(det);

        det = new SubCategoryDetails("5", "CALENDERS");
        subCatDetails.add(det);

        det = new SubCategoryDetails("6", "GREETING CARDS");
        subCatDetails.add(det);

        det = new SubCategoryDetails("7", "CALENDERS");
        subCatDetails.add(det);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                finish();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }




}
