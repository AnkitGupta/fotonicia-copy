package com.fotonicia.category;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Menu;
import android.app.ActionBar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fotonicia.R;
import com.fotonicia.adapter.ReviewTabsPagerAdapter;
import com.fotonicia.adapter.SliderPagerAdapter;
import com.fotonicia.adapter.SubCategoryItemAdapter;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.model.SubCategoryItemDetails;
import com.fotonicia.util.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

public class RateAndReviewActivity extends FragmentActivity implements View.OnClickListener {

    PreferenceManager preferenceManager;
    private Activity activity;
    private ImageView imgBack;
    private ViewPager vp_slider;
    private CustomRegularHGRTextView txtSubCatName;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        activity = this;
        preferenceManager = new PreferenceManager(getApplicationContext());

        setContentView(R.layout.activity_rate_review);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.layout_title_bar_subcat);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getContents();

    }

    private void getContents() {

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);

        txtSubCatName = (CustomRegularHGRTextView) findViewById(R.id.txtSubCatName);
        txtSubCatName.setText("RATE & REVIEW");

        vp_slider = (ViewPager) findViewById(R.id.vp_slider);
        vp_slider.setAdapter(new ReviewTabsPagerAdapter(getSupportFragmentManager()));




    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {

        }

    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                finish();
                break;


        }
    }



    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }




}
