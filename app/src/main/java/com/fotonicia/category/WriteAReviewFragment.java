package com.fotonicia.category;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fotonicia.R;
import com.fotonicia.adapter.SliderPagerAdapter;
import com.fotonicia.adapter.SubCategoryItemAdapter;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.model.SubCategoryItemDetails;
import com.fotonicia.util.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

public class WriteAReviewFragment  extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_write_review, container, false);

      /*  TextView tv = (TextView) v.findViewById(R.id.tvFragFirst);
        tv.setText(getArguments().getString("msg"));*/

        return v;
    }

    public static WriteAReviewFragment newInstance(String text) {

        WriteAReviewFragment f = new WriteAReviewFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}