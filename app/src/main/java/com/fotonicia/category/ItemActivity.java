package com.fotonicia.category;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.fotonicia.R;
import com.fotonicia.adapter.AspectRatioAdapter;
import com.fotonicia.adapter.ItemSizesAdapter;
import com.fotonicia.adapter.SliderPagerAdapter;
import com.fotonicia.adapter.SubCategoryItemAdapter;
import com.fotonicia.components.CustomRegularEditText;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.model.AspectRatioDetails;
import com.fotonicia.model.SubCategoryItemDetails;
import com.fotonicia.util.NotificationUtils;
import com.fotonicia.util.PreferenceManager;
import com.fotonicia.util.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ItemActivity extends Activity implements View.OnClickListener {

    PreferenceManager preferenceManager;
    private Activity activity;
    private ImageView imgBack, imgYoutube;
    private List<SubCategoryItemDetails> subCatDetails = new ArrayList<>();
    private RecyclerView recyclerView;
    private SubCategoryItemAdapter mAdapter;
    private RelativeLayout layCreateItem, layDetailsItem, layItemSizes;
    private LinearLayout ll_dots, layMore, layCreate, layCancel, layViewAllReviews, layBottom;
    private RatingBar rating;
    private ViewPager vp_slider;
    SliderPagerAdapter sliderPagerAdapter;
    private TextView[] dots;
    int page_position = 0;
    private CustomRegularHGRTextView txtCreateDetails, txtCreate;
    private static final Integer[] IMAGES = {R.drawable.img_b,
            R.drawable.img_c,
            R.drawable.img_d,
            R.drawable.img_e,
            R.drawable.images_f};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        activity = this;
        preferenceManager = new PreferenceManager(getApplicationContext());

        setContentView(R.layout.activity_item);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.layout_title_bar_subcat);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getContents();

    }

    private void getContents() {

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);

        imgYoutube = (ImageView) findViewById(R.id.imgYoutube);
        imgYoutube.setVisibility(View.VISIBLE);
        imgYoutube.setOnClickListener(this);


        vp_slider = (ViewPager) findViewById(R.id.vp_slider);
        ll_dots = (LinearLayout) findViewById(R.id.ll_dots);
        layMore = (LinearLayout) findViewById(R.id.layMore);
        layCreate = (LinearLayout) findViewById(R.id.layCreate);
        layBottom = (LinearLayout) findViewById(R.id.layBottom);

        layCancel = (LinearLayout) findViewById(R.id.layCancel);
        layCancel.setOnClickListener(this);

        layViewAllReviews = (LinearLayout) findViewById(R.id.layViewAllReviews);
        layViewAllReviews.setOnClickListener(this);

        txtCreate = (CustomRegularHGRTextView) findViewById(R.id.txtCreate);
        txtCreate.setOnClickListener(this);
        txtCreateDetails = (CustomRegularHGRTextView) findViewById(R.id.txtCreateDetails);
        txtCreateDetails.setOnClickListener(this);

        layCreateItem = (RelativeLayout) findViewById(R.id.layCreateItem);
        layDetailsItem = (RelativeLayout) findViewById(R.id.layDetailsItem);
      //  layItemSizes = (RelativeLayout) findViewById(R.id.layItemSizes);

        layDetailsItem.setVisibility(View.GONE);
     //   layItemSizes.setVisibility(View.GONE);
        layCreateItem.setVisibility(View.VISIBLE);

        layMore.setOnClickListener(this);
        layCreate.setOnClickListener(this);


        rating = (RatingBar) findViewById(R.id.rating);
        rating.setRating(3.5f);

        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rate, boolean fromUser) {
                // TODO Auto-generated method stub
                //text.setText("Rating: "+String.valueOf(rating));
                //Toast.makeText(activity, String.valueOf(rating.getRating(), Toast.LENGTH_SHORT).show();
                System.out.println("Rating >> " + rating.getRating());
                // Toast.makeText(activity, rating.getRating(), Toast.LENGTH_SHORT).show();
            }
        });


        for (int i = 0; i < IMAGES.length; i++)
            ImagesArray.add(IMAGES[i]);


        sliderPagerAdapter = new SliderPagerAdapter(ItemActivity.this, ImagesArray);
        vp_slider.setAdapter(sliderPagerAdapter);

        vp_slider.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

                /*Slider banner handler*/
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            public void run() {
                if (page_position == ImagesArray.size()) {
                    page_position = 0;
                } else {
                    page_position = page_position + 1;
                }
                vp_slider.setCurrentItem(page_position, true);
            }
        };

       /* new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 500, 5000);*/
        //getSubCategoryItems();
    }

    private void getSubCategoryItems() {
        SubCategoryItemDetails det = new SubCategoryItemDetails("1", "METAL PRINT");
        subCatDetails.add(det);

        det = new SubCategoryItemDetails("2", "MAGNET PRINT");
        subCatDetails.add(det);

        det = new SubCategoryItemDetails("3", "PHOTO-PAPER PRINT");
        subCatDetails.add(det);

        det = new SubCategoryItemDetails("4", "POSTERS");
        subCatDetails.add(det);

        det = new SubCategoryItemDetails("5", "CALENDERS");
        subCatDetails.add(det);

        det = new SubCategoryItemDetails("6", "GREETING CARDS");
        subCatDetails.add(det);

        det = new SubCategoryItemDetails("7", "CALENDERS");
        subCatDetails.add(det);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {

        }

    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                finish();
                break;

            case R.id.imgYoutube:
                Intent intent_video = new Intent(this, ItemVideoActivity.class);
                startActivity(intent_video);
                break;

            case R.id.layMore:
                imgBack.setVisibility(View.GONE);
                layCreateItem.setVisibility(View.GONE);
              //  layItemSizes.setVisibility(View.GONE);
                layDetailsItem.setVisibility(View.VISIBLE);
                break;

            case R.id.layCancel:
                imgBack.setVisibility(View.VISIBLE);
                layCreateItem.setVisibility(View.VISIBLE);
                layDetailsItem.setVisibility(View.GONE);
                break;

            case R.id.txtCreate:
                imgBack.setVisibility(View.VISIBLE);
               // layCreateItem.setVisibility(View.VISIBLE);
               // layDetailsItem.setVisibility(View.GONE);
               // layItemSizes.setVisibility(View.VISIBLE);
              //  layMore.setVisibility(View.GONE);
               // layBottom.setVisibility(View.GONE);

                callItemSizesDialog();
                break;

            case R.id.txtCreateDetails:
                imgBack.setVisibility(View.VISIBLE);
              //  layCreateItem.setVisibility(View.VISIBLE);
               // layDetailsItem.setVisibility(View.GONE);
               // layItemSizes.setVisibility(View.VISIBLE);
               // layBottom.setVisibility(View.GONE);
              //  layMore.setVisibility(View.GONE);
                callItemSizesDialog();
                break;

            case R.id.layViewAllReviews:
                Intent reviews_intent = new Intent(this, RateAndReviewActivity.class);
                startActivity(reviews_intent);
                break;

        }
    }


    private void callItemSizesDialog() {
      Intent aspect_ratio_intent = new Intent(this, AspectRatioAndSizesActivity.class);
        startActivityForResult(aspect_ratio_intent, 2);
        overridePendingTransition(R.anim.slide_up_dialog, R.anim.stay);

       /* RecyclerView aspect_ratio_recycler_view, item_sizes_recycler_view;
        GridLayoutManager lLayout;


        AspectRatioAdapter aspectRatioAdapter;
        ItemSizesAdapter itemSizesAdapter;
        List<String> strAspectRatio = new ArrayList<>();
        List<String> strItemSizes = new ArrayList<>();
        ImageView imgCancel;

        final Dialog openDialog = new Dialog(activity);
        openDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.BOTTOM);
        openDialog.setContentView(R.layout.activity_aspect_ratio_sizes);

        imgCancel = (ImageView) openDialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(this);

        aspect_ratio_recycler_view = (RecyclerView) openDialog.findViewById(R.id.aspect_ratio_recycler_view);
        strAspectRatio.add("1:1");
        strAspectRatio.add("2:3");
        strAspectRatio.add("3:4");
        strAspectRatio.add("16:9");
        strAspectRatio.add("5:6");
        aspectRatioAdapter = new AspectRatioAdapter(strAspectRatio);
        RecyclerView.LayoutManager catLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        aspect_ratio_recycler_view.setLayoutManager(catLayoutManager);
        aspect_ratio_recycler_view.setItemAnimator(new DefaultItemAnimator());
        aspect_ratio_recycler_view.setAdapter(aspectRatioAdapter);
        aspect_ratio_recycler_view.setHasFixedSize(true);

        item_sizes_recycler_view = (RecyclerView) openDialog.findViewById(R.id.item_sizes_recycler_view);
        strItemSizes.add("4x4");
        strItemSizes.add("8x10");
        strItemSizes.add("8x8");
        strItemSizes.add("12x15");
       *//* strItemSizes.add("10x10");
        strItemSizes.add("20x20");
        strItemSizes.add("11x15");
        strItemSizes.add("15x20");
        strItemSizes.add("20x30");
        strItemSizes.add("4x4");
        strItemSizes.add("8x10");
        strItemSizes.add("8x8");
        strItemSizes.add("12x15");
        strItemSizes.add("10x10");
        strItemSizes.add("20x20");
        strItemSizes.add("11x15");
        strItemSizes.add("15x20");
        strItemSizes.add("20x30");*//*
        lLayout = new GridLayoutManager(this, 2);
        item_sizes_recycler_view.setHasFixedSize(true);
        item_sizes_recycler_view.setLayoutManager(lLayout);
        itemSizesAdapter = new ItemSizesAdapter(getApplicationContext(), strItemSizes);
        item_sizes_recycler_view.setAdapter(itemSizesAdapter);

        openDialog.show();
*/

    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[ImagesArray.size()];

        ll_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.parseColor("#ffffff"));
            ll_dots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor("#F8A700"));
    }


}
