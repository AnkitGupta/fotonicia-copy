package com.fotonicia.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemSizesDetails implements Parcelable {//{"status_code":"200","message":"Login successful","user_id":"3"}
    private String ItemSize;
    private String Price;

    public ItemSizesDetails(String itemSize, String price) {
        this.ItemSize = itemSize;
        this.Price = price;
    }


    public String getItemSize() {
        return ItemSize;
    }

    public void setItemSize(String itemSize) {
        ItemSize = itemSize;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(ItemSize);
        parcel.writeString(Price);
    }

    @SuppressWarnings("unused")
    public static final Creator<ItemSizesDetails> CREATOR = new Creator<ItemSizesDetails>() {
        @Override
        public ItemSizesDetails createFromParcel(Parcel in) {
            return new ItemSizesDetails(in);
        }

        @Override
        public ItemSizesDetails[] newArray(int size) {
            return new ItemSizesDetails[size];
        }
    };

    protected ItemSizesDetails(Parcel in) {
        ItemSize = in.readString();
        Price = in.readString();
    }
    public ItemSizesDetails(){}

}
