package com.fotonicia.model;

import android.os.Parcel;
import android.os.Parcelable;

public class AboutUsDetails implements Parcelable {//{"status_code":"200","message":"Login successful","user_id":"3"}

    private String HeaderText;
    private String DescriptionText;
    private String Image;

    public AboutUsDetails(String headerText, String descriptionText, String image) {
        this.HeaderText = headerText;
        this.DescriptionText = descriptionText;
        this.Image = image;
    }

    public String getHeaderText() {
        return HeaderText;
    }

    public void setHeaderText(String headerText) {
        HeaderText = headerText;
    }

    public String getDescriptionText() {
        return DescriptionText;
    }

    public void setDescriptionText(String descriptionText) {
        DescriptionText = descriptionText;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(HeaderText);
        parcel.writeString(DescriptionText);
        parcel.writeString(Image);
    }

    @SuppressWarnings("unused")
    public static final Creator<RegistrationDetails> CREATOR = new Creator<RegistrationDetails>() {
        @Override
        public RegistrationDetails createFromParcel(Parcel in) {
            return new RegistrationDetails(in);
        }

        @Override
        public RegistrationDetails[] newArray(int size) {
            return new RegistrationDetails[size];
        }
    };

    protected AboutUsDetails(Parcel in) {
        HeaderText = in.readString();
        DescriptionText = in.readString();
        Image = in.readString();
    }
    public AboutUsDetails(){}

}
