package com.fotonicia.model;

import android.os.Parcel;
import android.os.Parcelable;

public class HomeCouponsDetails implements Parcelable {//{"status_code":"200","message":"Login successful","user_id":"3"}
    private String Id;
    private int Image;
    private String Status;

    //public SubCategoryDetails(String subCatId, String subCatName, String subCatImageURL) {
        public HomeCouponsDetails(int image) {
        this.Image = image;

    }


    public int getImage() {
        return Image;
    }

    public void setImage(int image) {
        Image = image;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(Image);
    }

    @SuppressWarnings("unused")
    public static final Creator<HomeCouponsDetails> CREATOR = new Creator<HomeCouponsDetails>() {
        @Override
        public HomeCouponsDetails createFromParcel(Parcel in) {
            return new HomeCouponsDetails(in);
        }

        @Override
        public HomeCouponsDetails[] newArray(int size) {
            return new HomeCouponsDetails[size];
        }
    };

    protected HomeCouponsDetails(Parcel in) {
        Image = in.readInt();
    }
    public HomeCouponsDetails(){}

}
