package com.fotonicia.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SubCategoryItemDetails implements Parcelable {//{"status_code":"200","message":"Login successful","user_id":"3"}
    private String SubCatItemId;
    private String SubCatItemName;
    private String SubCatItemImageURL;

    //public SubCategoryDetails(String subCatId, String subCatName, String subCatItemImageURL) {
        public SubCategoryItemDetails(String subCatItemId, String subCatItemName) {
        this.SubCatItemId = subCatItemId;
        this.SubCatItemName = subCatItemName;
      //  this.SubCatItemImageURL = subCatItemImageURL;
    }


    public String getSubCatItemId() {
        return SubCatItemId;
    }

    public void setSubCatItemId(String subCatItemId) {
        SubCatItemId = subCatItemId;
    }



    public String getSubCatItemName() {
        return SubCatItemName;
    }

    public void setSubCatItemName(String subCatItemName) {
        SubCatItemName = subCatItemName;
    }



    public String getSubCatItemImageURL() {
        return SubCatItemImageURL;
    }

    public void setSubCatItemImageURL(String subCatItemImageURL) {
        SubCatItemImageURL = subCatItemImageURL;
    }




    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(SubCatItemId);
        parcel.writeString(SubCatItemName);
        parcel.writeString(SubCatItemImageURL);
    }

    @SuppressWarnings("unused")
    public static final Creator<SubCategoryItemDetails> CREATOR = new Creator<SubCategoryItemDetails>() {
        @Override
        public SubCategoryItemDetails createFromParcel(Parcel in) {
            return new SubCategoryItemDetails(in);
        }

        @Override
        public SubCategoryItemDetails[] newArray(int size) {
            return new SubCategoryItemDetails[size];
        }
    };

    protected SubCategoryItemDetails(Parcel in) {
        SubCatItemId = in.readString();
        SubCatItemName = in.readString();
        SubCatItemImageURL = in.readString();
    }
    public SubCategoryItemDetails(){}

}
