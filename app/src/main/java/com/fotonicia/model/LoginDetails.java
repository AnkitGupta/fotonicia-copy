package com.fotonicia.model;

import android.os.Parcel;
import android.os.Parcelable;

public class LoginDetails implements Parcelable {//{"status_code":"200","message":"Login successful","user_id":"3"}
   // private String UserRegdID;
    private String UserId;
    private String Status;
    private String StatusCode;

  /*  public String getUserRegdID() {
        return UserRegdID;
    }

    public void setUserRegdID(String userRegdID) {
        UserRegdID = userRegdID;
    }*/

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String user_id) {
        UserId = user_id;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String message) {
        Status = message;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String status_code) {
        StatusCode = status_code;
    }




    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
       // parcel.writeString(UserRegdID);
        parcel.writeString(UserId);
        parcel.writeString(Status);
    }

    @SuppressWarnings("unused")
    public static final Creator<RegistrationDetails> CREATOR = new Creator<RegistrationDetails>() {
        @Override
        public RegistrationDetails createFromParcel(Parcel in) {
            return new RegistrationDetails(in);
        }

        @Override
        public RegistrationDetails[] newArray(int size) {
            return new RegistrationDetails[size];
        }
    };

    protected LoginDetails(Parcel in) {
      //  UserRegdID = in.readString();
        UserId = in.readString();
        Status = in.readString();
    }
    public LoginDetails(){}

}
