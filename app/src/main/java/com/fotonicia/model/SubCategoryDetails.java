package com.fotonicia.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SubCategoryDetails implements Parcelable {//{"status_code":"200","message":"Login successful","user_id":"3"}
    private String SubCatId;
    private String SubCatName;
    private String SubCatImageURL;

    //public SubCategoryDetails(String subCatId, String subCatName, String subCatImageURL) {
        public SubCategoryDetails(String subCatId, String subCatName) {
        this.SubCatId = subCatId;
        this.SubCatName = subCatName;
      //  this.SubCatImageURL = subCatImageURL;
    }


    public String getSubCatId() {
        return SubCatId;
    }

    public void setSubCatId(String subCatId) {
        SubCatId = subCatId;
    }



    public String getSubCatName() {
        return SubCatName;
    }

    public void setSubCatName(String subCatName) {
        SubCatName = subCatName;
    }



    public String getSubCatImageURL() {
        return SubCatImageURL;
    }

    public void setSubCatImageURL(String subCatImageURL) {
        SubCatImageURL = subCatImageURL;
    }




    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(SubCatId);
        parcel.writeString(SubCatName);
        parcel.writeString(SubCatImageURL);
    }

    @SuppressWarnings("unused")
    public static final Creator<SubCategoryDetails> CREATOR = new Creator<SubCategoryDetails>() {
        @Override
        public SubCategoryDetails createFromParcel(Parcel in) {
            return new SubCategoryDetails(in);
        }

        @Override
        public SubCategoryDetails[] newArray(int size) {
            return new SubCategoryDetails[size];
        }
    };

    protected SubCategoryDetails(Parcel in) {
        SubCatId = in.readString();
        SubCatName = in.readString();
        SubCatImageURL = in.readString();
    }
    public SubCategoryDetails(){}

}
