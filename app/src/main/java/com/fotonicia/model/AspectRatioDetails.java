package com.fotonicia.model;

import android.os.Parcel;
import android.os.Parcelable;

public class AspectRatioDetails implements Parcelable {//{"status_code":"200","message":"Login successful","user_id":"3"}
    private String AspectRatio;

    //public SubCategoryDetails(String subCatId, String subCatName, String subCatImageURL) {
        public AspectRatioDetails(String aspectRatio) {
        this.AspectRatio = aspectRatio;

    }


    public String getAspectRatio() {
        return AspectRatio;
    }

    public void setAspectRatio(String aspectRatio) {
        AspectRatio = aspectRatio;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(AspectRatio);
    }

    @SuppressWarnings("unused")
    public static final Creator<AspectRatioDetails> CREATOR = new Creator<AspectRatioDetails>() {
        @Override
        public AspectRatioDetails createFromParcel(Parcel in) {
            return new AspectRatioDetails(in);
        }

        @Override
        public AspectRatioDetails[] newArray(int size) {
            return new AspectRatioDetails[size];
        }
    };

    protected AspectRatioDetails(Parcel in) {
        AspectRatio = in.readString();
    }
    public AspectRatioDetails(){}

}
