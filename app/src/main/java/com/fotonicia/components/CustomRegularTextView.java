package com.fotonicia.components;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Ankit on 21-09-2015.
 */
public class CustomRegularTextView extends TextView {

    public CustomRegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomRegularTextView(Context context) {
        super(context);
        init();
    }

    private void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "Roboto-Regular.ttf");

        setTypeface(tf);

    }
}
