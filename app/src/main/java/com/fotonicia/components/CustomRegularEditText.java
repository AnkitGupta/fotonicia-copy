package com.fotonicia.components;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Ankit on 21-09-2015.
 */
public class CustomRegularEditText extends EditText {

    public CustomRegularEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomRegularEditText(Context context) {
        super(context);
        init();
    }

    private void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "Roboto-Regular.ttf");

        setTypeface(tf);

    }
}
