package com.fotonicia.components;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Ankit on 21-09-2015.
 */
public class CustomRegularHGREditText extends TextView {

    public CustomRegularHGREditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomRegularHGREditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomRegularHGREditText(Context context) {
        super(context);
        init();
    }

    private void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "HalisGR-Regular.otf");

        setTypeface(tf);
    }
}
