package com.fotonicia.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fotonicia.R;
import com.fotonicia.model.AboutUsDetails;
import com.fotonicia.model.DonateToEducateDetails;

import java.util.List;

/**
 * Created by moumita on 3/24/2017.
 */
public class DonateToEducateAdapter extends RecyclerView.Adapter<DonateToEducateAdapter.MyViewHolder> {

    private static final int TYPE_TOP_VIDEO = 0;  // Declaring Variable to Understand which View is being worked on
    // IF the view under inflation and population is header or Item
    private static final int TYPE_DESCRIPTION = 1;
    private static final int TYPE_BOTTOM_VIDEO = 2;
    private List<DonateToEducateDetails> items;
    private Context context;

    public DonateToEducateAdapter(List<DonateToEducateDetails> donateToEducate_details) {
        this.items = donateToEducate_details;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgForm;
        int Holderid;

        public MyViewHolder(View view, int ViewType) {
            super(view);
            if (ViewType == TYPE_TOP_VIDEO) {
               /* textView = (TextView) itemView.findViewById(R.id.rowText); // Creating TextView object with the id of textView from row_left_nav_drawer_nav_drawer.xml
                imageView = (ImageView) itemView.findViewById(R.id.rowIcon);// Creating ImageView object with the id of ImageView from row_left_nav_drawer.xml_drawer.xml
               */
                Holderid = 0;                                               // setting holder id as 1 as the object being populated are of type item row
            } else if (ViewType == TYPE_DESCRIPTION) {
                /*Name = (TextView) itemView.findViewById(R.id.name);         // Creating Text View object from header.xml for name
                email = (TextView) itemView.findViewById(R.id.email);       // Creating Text View object from header.xml for email
                profile = (ImageView) itemView.findViewById(R.id.circleView);// Creating Image view object from header.xml for profile pic
               */
                Holderid = 1;                                                // Setting holder id = 0 as the object being populated are of type header view
            } else if (ViewType == TYPE_BOTTOM_VIDEO) {
                /*Name = (TextView) itemView.findViewById(R.id.name);         // Creating Text View object from header.xml for name
                email = (TextView) itemView.findViewById(R.id.email);       // Creating Text View object from header.xml for email
                profile = (ImageView) itemView.findViewById(R.id.circleView);// Creating Image view object from header.xml for profile pic
               */
                Holderid = 2;                                                // Setting holder id = 0 as the object being populated are of type header view
            }

        }
    }

   /* public AspectRatioAdapter(List<SubCategoryItemDetails> items) {
        this.items = items;
    }*/


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_TOP_VIDEO) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_donate_to_educate_top_video, parent, false); //Inflating the layout

            DonateToEducateAdapter.MyViewHolder vhItem = new DonateToEducateAdapter.MyViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view

            context = parent.getContext();
            return vhItem; // Returning the created object

            //inflate your layout and pass it to view holder

        } else if (viewType == TYPE_DESCRIPTION) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_donatetoeducate, parent, false); //Inflating the layout

            DonateToEducateAdapter.MyViewHolder vhHeader = new DonateToEducateAdapter.MyViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view
            context = parent.getContext();
            return vhHeader; //returning the object created
        } else if (viewType == TYPE_BOTTOM_VIDEO) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_donate_to_educate_bottom_video, parent, false); //Inflating the layout

            DonateToEducateAdapter.MyViewHolder vBottom = new DonateToEducateAdapter.MyViewHolder(v, viewType); //Creating ViewHolder and passing the object of type view
            context = parent.getContext();
            return vBottom; //returning the object created
        }
        return null;

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        if (holder.Holderid == 1) {

        } else {

        }


    }

    @Override
    public int getItemCount() {
        // return items.size();
        return 10;
    }


    @Override
    public int getItemViewType(int position) {
       /* if (isPositionSocialLinks(9))
            return TYPE_SOCIAL_LINKS;
       *//* else if(isPositionLogo(mNavTitles.length+1))
            return TYPE_LOGO;*//*

        return TYPE_ABOUTUS;*/
        if (position == 0) {
            return TYPE_TOP_VIDEO;
        } else if ((position == 9)) {
            return TYPE_BOTTOM_VIDEO;
        }
        return TYPE_DESCRIPTION;
    }


}
