package com.fotonicia.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fotonicia.R;
import com.fotonicia.model.AboutUsDetails;

import java.util.List;

/**
 * Created by moumita on 3/24/2017.
 */
public class AboutUsAdapter extends RecyclerView.Adapter<AboutUsAdapter.MyViewHolder> {

    private static final int TYPE_ABOUTUS = 0;  // Declaring Variable to Understand which View is being worked on
    // IF the view under inflation and population is header or Item
    private static final int TYPE_SOCIAL_LINKS = 1;
    private List<AboutUsDetails> items;
    private Context context;

    public AboutUsAdapter(List<AboutUsDetails> aboutus_details) {
        this.items = aboutus_details;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgForm;
        int Holderid;

        public MyViewHolder(View view, int ViewType) {
            super(view);

            if(ViewType == TYPE_ABOUTUS) {
               /* textView = (TextView) itemView.findViewById(R.id.rowText); // Creating TextView object with the id of textView from row_left_nav_drawer_nav_drawer.xml
                imageView = (ImageView) itemView.findViewById(R.id.rowIcon);// Creating ImageView object with the id of ImageView from row_left_nav_drawer.xml_drawer.xml
               */ Holderid = 0;                                               // setting holder id as 1 as the object being populated are of type item row
            }
            else if (ViewType == TYPE_SOCIAL_LINKS){
                /*Name = (TextView) itemView.findViewById(R.id.name);         // Creating Text View object from header.xml for name
                email = (TextView) itemView.findViewById(R.id.email);       // Creating Text View object from header.xml for email
                profile = (ImageView) itemView.findViewById(R.id.circleView);// Creating Image view object from header.xml for profile pic
               */ Holderid = 1;                                                // Setting holder id = 0 as the object being populated are of type header view
            }

        }
    }

   /* public AspectRatioAdapter(List<SubCategoryItemDetails> items) {
        this.items = items;
    }*/


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ABOUTUS) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_aboutus,parent,false); //Inflating the layout

            AboutUsAdapter.MyViewHolder vhItem = new AboutUsAdapter.MyViewHolder(v,viewType); //Creating ViewHolder and passing the object of type view

            context = parent.getContext();
            return vhItem; // Returning the created object

            //inflate your layout and pass it to view holder

        } else if (viewType == TYPE_SOCIAL_LINKS) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_about_us_social_links,parent,false); //Inflating the layout

            AboutUsAdapter.MyViewHolder vhHeader = new AboutUsAdapter.MyViewHolder(v,viewType); //Creating ViewHolder and passing the object of type view
            context = parent.getContext();
            return vhHeader; //returning the object created


        }

        /*View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_aboutus, parent, false);

        context = parent.getContext();
        return new MyViewHolder(itemView);*/
        return null;

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        if(holder.Holderid ==1)
        {

        }
        else
        {

        }
       // AboutUsDetails details = items.get(position);
       // Picasso.with(context).load(items.get(position)).into(holder.imgForm);
        /*holder.txtCategoryName.setText(details.getSubCatName());
        Picasso.with(context).load(R.drawable.category).into(holder.imgSubCat);*/

      /*  holder.imgForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //holder.layAspectRatio.setBackgroundResource(context.getResources().getColor(R.drawable.create_oval_filled_blue));
            }
        });*/


    }

    @Override
    public int getItemViewType(int position) {
       /* if (isPositionSocialLinks(9))
            return TYPE_SOCIAL_LINKS;
       *//* else if(isPositionLogo(mNavTitles.length+1))
            return TYPE_LOGO;*//*

        return TYPE_ABOUTUS;*/
        if (position == 10) {
            return TYPE_SOCIAL_LINKS;
        }
        return TYPE_ABOUTUS;
    }

    @Override
    public int getItemCount() {
       // return items.size();
        return 11;
    }

    private boolean isPositionSocialLinks(int position) {
        return position == 10;
    }


}
