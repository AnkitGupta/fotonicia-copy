package com.fotonicia.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fotonicia.R;

/**
 * Created by moumita on 5/6/2017.
 */

public class CameraViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView photo;
    public RelativeLayout card_view;

    public CameraViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        photo = (ImageView) itemView.findViewById(R.id.showImg_odd);
        card_view = (RelativeLayout) itemView.findViewById(R.id.card_view);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}

