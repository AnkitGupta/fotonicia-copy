package com.fotonicia.adapter;

/**
 * Created by moumita on 4/27/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fotonicia.R;
import com.fotonicia.leftnavigation.AboutUsActivity;
import com.fotonicia.leftnavigation.ContactUsActivity;
import com.fotonicia.leftnavigation.DonateToEducateActivity;
import com.fotonicia.leftnavigation.FeedbackActivity;
import com.fotonicia.leftnavigation.FollowUsActivity;
import com.fotonicia.leftnavigation.HelpFAQActivity;
import com.fotonicia.leftnavigation.LikeUsActivity;
import com.fotonicia.leftnavigation.OffersActivity;
import com.fotonicia.leftnavigation.RateUsActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by hp1 on 28-12-2014.
 */
public class LeftNavigationAdapter extends RecyclerView.Adapter<LeftNavigationAdapter.ViewHolder> {

    private static final int TYPE_HEADER = 0;  // Declaring Variable to Understand which View is being worked on
    // IF the view under inflation and population is header or Item
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_LOGO = 2;

    private String mNavTitles[]; // String Array to store the passed titles Value from MainActivity.java
    private int mIcons[];       // Int Array to store the passed icons resource value from MainActivity.java

    private String name;        //String Resource for header View Name
    private int profile;        //int Resource for header view profile picture
    private String email;       //String Resource for header view email
    private Context context;


    // Creating a ViewHolder which extends the RecyclerView View Holder
    // ViewHolder are used to to store the inflated views in order to recycle them

    public static class ViewHolder extends RecyclerView.ViewHolder {
        int Holderid;

        TextView textView, Name, email;
        ImageView imageView, imgLogo, profile;

        public ViewHolder(View itemView,int ViewType) {                 // Creating ViewHolder Constructor with View and viewType As a parameter
            super(itemView);


            // Here we set the appropriate view in accordance with the the view type as passed when the holder object is created

            if(ViewType == TYPE_ITEM) {
                textView = (TextView) itemView.findViewById(R.id.rowText); // Creating TextView object with the id of textView from row_left_nav_drawer_nav_drawer.xml
                imageView = (ImageView) itemView.findViewById(R.id.rowIcon);// Creating ImageView object with the id of ImageView from row_left_nav_drawer.xml_drawer.xml
                Holderid = 1;                                               // setting holder id as 1 as the object being populated are of type item row
            }
            else if (ViewType == TYPE_HEADER){
                Name = (TextView) itemView.findViewById(R.id.name);         // Creating Text View object from header.xml for name
                email = (TextView) itemView.findViewById(R.id.email);       // Creating Text View object from header.xml for email
                profile = (ImageView) itemView.findViewById(R.id.circleView);// Creating Image view object from header.xml for profile pic
                Holderid = 0;                                                // Setting holder id = 0 as the object being populated are of type header view
            }
            else
            {
                imgLogo = (ImageView) itemView.findViewById(R.id.imgLogo);         // Creating Text View object from header.xml for name
                Holderid = 2;
            }
        }


    }



    public LeftNavigationAdapter(String Titles[],int Icons[],String Name,String Email, int Profile){ // MyAdapter Constructor with titles and icons parameter
        // titles, icons, name, email, profile pic are passed from the main activity as we
        mNavTitles = Titles;                //have seen earlier
        mIcons = Icons;
        name = Name;
        email = Email;
        profile = Profile;                     //here we assign those passed values to the values we declared here
        //in adapter



    }



    //Below first we ovverride the method onCreateViewHolder which is called when the ViewHolder is
    //Created, In this method we inflate the row_left_nav_drawer_nav_drawer.xml layout if the viewType is Type_ITEM or else we inflate header.xml
    // if the viewType is TYPE_HEADER
    // and pass it to the view holder

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_left_nav_drawer,parent,false); //Inflating the layout

            ViewHolder vhItem = new ViewHolder(v,viewType); //Creating ViewHolder and passing the object of type view

            context = parent.getContext();
            return vhItem; // Returning the created object

            //inflate your layout and pass it to view holder

        } else if (viewType == TYPE_HEADER) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header,parent,false); //Inflating the layout

            ViewHolder vhHeader = new ViewHolder(v,viewType); //Creating ViewHolder and passing the object of type view
            context = parent.getContext();
            return vhHeader; //returning the object created


        }
        else if (viewType == TYPE_LOGO) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_left_navigation_bottom_logo,parent,false); //Inflating the layout

            ViewHolder vLogo = new ViewHolder(v,viewType); //Creating ViewHolder and passing the object of type view
            context = parent.getContext();
            return vLogo; //returning the object created


        }
        return null;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if(holder.Holderid ==1) {                              // as the list view is going to be called after the header view so we decrement the
            // position by 1 and pass it to the holder while setting the text and image
            holder.textView.setText(mNavTitles[position - 1]); // Setting the Text with the array of our Titles
            holder.imageView.setImageResource(mIcons[position -1]);// Settimg the image with array of our icons

            holder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(context, holder.textView.getText(), Toast.LENGTH_SHORT).show();
                    if(holder.textView.getText().equals("About Us"))
                    {
                        Intent intent = new Intent(context, AboutUsActivity.class);
                        context.startActivity(intent);
                    }
                    else if(holder.textView.getText().equals("My Account"))
                    {
                        Intent intent = new Intent(context, HelpFAQActivity.class);
                        context.startActivity(intent);
                    }
                    else if(holder.textView.getText().equals("Refer and Earn"))
                    {
                        Intent intent = new Intent(context, HelpFAQActivity.class);
                        context.startActivity(intent);
                    }
                    else if(holder.textView.getText().equals("Offers"))
                    {
                        Intent intent = new Intent(context, OffersActivity.class);
                        context.startActivity(intent);
                    }
                    else if(holder.textView.getText().equals("Donate to Educate"))
                    {
                        Intent intent = new Intent(context, DonateToEducateActivity.class);
                        context.startActivity(intent);
                    }
                    else if(holder.textView.getText().equals("Help & FAQ"))
                    {
                        Intent intent = new Intent(context, HelpFAQActivity.class);
                        context.startActivity(intent);
                    }
                    else if(holder.textView.getText().equals("Like Us"))
                    {
                        Intent intent = new Intent(context, LikeUsActivity.class);
                        context.startActivity(intent);
                    }
                    else if(holder.textView.getText().equals("Follow Us"))
                    {
                        Intent intent = new Intent(context, FollowUsActivity.class);
                        context.startActivity(intent);
                    }
                    else if(holder.textView.getText().equals("Rate Us"))
                    {
                        Intent intent = new Intent(context, RateUsActivity.class);
                        context.startActivity(intent);
                    }
                    else if(holder.textView.getText().equals("Contact Us"))
                    {
                        sendEmail();

                        /*Intent intent = new Intent(context, ContactUsActivity.class);
                        context.startActivity(intent);*/
                    }
                    else if(holder.textView.getText().equals("Feedback"))
                    {
                        Intent intent = new Intent(context, FeedbackActivity.class);
                        context.startActivity(intent);
                    }
                }
            });
        }
        else if(holder.Holderid ==0){
            holder.profile.setImageResource(profile);           // Similarly we set the resources for header view
            holder.Name.setText(name);
            holder.email.setText(email);
        }
        else
        {
            holder.imgLogo.setImageDrawable(context.getResources().getDrawable(R.drawable.logo));
          //  holder.imgLogo.setImageResource(R.drawable.logo);
        }
    }

    private void sendEmail() {

        //String[] recipients = {recipient.getText().toString()};
        String[] recipients = {"moumita@zataksoftech.com"};
        String sub = "Test Mail : Fotonicia";
        String body = "wfhkwehfjw jklehfjweh jewhfjwehf jehfjwehf ewhfjwehf ejhfjwehfj eiwhfkwehf jehfjweh iefiewu";

        Intent email = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));

        // prompts email clients only
        email.setType("message/rfc822");
        email.putExtra(Intent.EXTRA_EMAIL, recipients);
        /*email.putExtra(Intent.EXTRA_SUBJECT, subject.getText().toString());
        email.putExtra(Intent.EXTRA_TEXT, body.getText().toString());*/
        email.putExtra(Intent.EXTRA_SUBJECT, sub);
        email.putExtra(Intent.EXTRA_TEXT, body);
        try {
            // the user can choose the email client
            context.startActivity(Intent.createChooser(email, "Choose an email client from..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "No email client installed.",
                    Toast.LENGTH_LONG).show();

        }

    }


    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    /*@Override
    public void onBindViewHolder(LeftNavigationAdapter.ViewHolder holder, int position) {
        if(holder.Holderid ==1) {                              // as the list view is going to be called after the header view so we decrement the
            // position by 1 and pass it to the holder while setting the text and image
            holder.textView.setText(mNavTitles[position - 1]); // Setting the Text with the array of our Titles
            holder.imageView.setImageResource(mIcons[position -1]);// Settimg the image with array of our icons
        }
        else{

            holder.profile.setImageResource(profile);           // Similarly we set the resources for header view
            holder.Name.setText(name);
            holder.email.setText(email);
        }
    }
*/
    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        return mNavTitles.length+2; // the number of items in the list will be +1 the titles including the header view.
    }


    // Witht the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
       /* if (isPositionHeader(position))
            return TYPE_HEADER;
       *//* else if(isPositionLogo(mNavTitles.length+1))
            return TYPE_LOGO;*//*

        return TYPE_ITEM;*/
        if (position == 0) {
            return TYPE_HEADER;
        } else if (position == mNavTitles.length + 1) {
            return TYPE_LOGO;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionLogo(int position) {
        return position == mNavTitles.length+1;
    }

   /* @Override
    public int getItemViewType(int position) {
        return (position == mData.size()) ? VIEW_TYPE_FOOTER : VIEW_TYPE_CELL;
    }*/

}
