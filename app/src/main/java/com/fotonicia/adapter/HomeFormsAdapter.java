package com.fotonicia.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.fotonicia.R;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by moumita on 3/24/2017.
 */
public class HomeFormsAdapter extends RecyclerView.Adapter<HomeFormsAdapter.MyViewHolder> {

    // private List<SubCategoryItemDetails> items;
    private List<Integer> items;
    private Context context;

    public HomeFormsAdapter(List<Integer> forms) {
        this.items = forms;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgForm;


        public MyViewHolder(View view) {
            super(view);
            imgForm = (ImageView) itemView.findViewById(R.id.imgForm);
        }
    }

   /* public AspectRatioAdapter(List<SubCategoryItemDetails> items) {
        this.items = items;
    }*/


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_home_forms, parent, false);

        context = parent.getContext();
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        //SubCategoryItemDetails details = items.get(position);
        Picasso.with(context).load(items.get(position)).into(holder.imgForm);
        /*holder.txtCategoryName.setText(details.getSubCatName());
        Picasso.with(context).load(R.drawable.category).into(holder.imgSubCat);*/

        holder.imgForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //holder.layAspectRatio.setBackgroundResource(context.getResources().getColor(R.drawable.create_oval_filled_blue));
            }
        });


    }

    @Override
    public int getItemCount() {
        return items.size();
    }


}
