package com.fotonicia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.fotonicia.R;
import com.fotonicia.category.ItemActivity;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.model.SubCategoryDetails;
import com.fotonicia.model.SubCategoryItemDetails;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by moumita on 3/24/2017.
 */
public class SubCategoryItemAdapter extends RecyclerView.Adapter<SubCategoryItemAdapter.MyViewHolder> {

    private List<SubCategoryItemDetails> items;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout laySubCat, layBottom;
        private ImageView imgSubCat;
        private CustomRegularHGRTextView txtCategoryName;


        public MyViewHolder(View view) {
            super(view);
            laySubCat = (RelativeLayout) itemView.findViewById(R.id.laySubCat);
           // txtCategoryName = (CustomRegularHGRTextView) itemView.findViewById(R.id.txtCategoryName);
        }
    }

    public SubCategoryItemAdapter(List<SubCategoryItemDetails> items) {
        this.items = items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_subcategory_item, parent, false);

        context = parent.getContext();
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        SubCategoryItemDetails details = items.get(position);

        /*holder.txtCategoryName.setText(details.getSubCatName());
        Picasso.with(context).load(R.drawable.category).into(holder.imgSubCat);*/

   /*     holder.layTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.layBottom.getVisibility() == View.VISIBLE) {
                    holder.imgToggle.setBackgroundResource(R.drawable.green_top_arrow);
                    holder.layBottom.setVisibility(View.GONE);
                } else {
                    holder.imgToggle.setBackgroundResource(R.drawable.green_down_icon);
                    holder.layBottom.setVisibility(View.VISIBLE);
                }
            }
        });*/

        holder.laySubCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ItemActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


}
