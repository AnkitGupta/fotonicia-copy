package com.fotonicia.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.fotonicia.R;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.model.ItemObjects;
import com.fotonicia.photoselection.SelectPhotoActivity;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Random;

/**
 * Created by moumita on 3/24/2017.
 */
public class SelectPhotoAdapter  extends RecyclerView.Adapter<CameraViewHolder> {
    List<String> cameraImages;
    private Random mRandom = new Random();
   /* //private List<ItemObjects> itemList;
    List<String> cameraImages;
    private Context context;
    private Random mRandom = new Random();

    *//*public SolventRecyclerViewAdapter(Context context, List<ItemObjects> itemList) {
        this.itemList = itemList;
        this.context = context;
    }*//*

    public SelectPhotoAdapter(SelectPhotoActivity context, List<String> cameraImages) {
        this.cameraImages = cameraImages;
        this.context = context;
    }

    @Override
    public CameraViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_select_photo, null);
        CameraViewHolder rcv = new CameraViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(SelectPhotoAdapter.CameraViewHolder holder, int position) {
      *//*  String fileImage = null;
        File imageDir = null;
        fileImage = Environment.getExternalStorageDirectory().toString()
                + "/DCIM/Camera/" + cameraImages.get(position);
        imageDir = new File(fileImage);
        Bitmap bmp = convertToBitmap(imageDir);
        holder.photo.setImageBitmap(bmp);*//*

        holder.photo.setImageResource(itemList.get(position).getPhoto());
    }


    *//*@Override
    public void onBindViewHolder(CameraViewHolder holder, int position) {
       // holder.photo.getLayoutParams().height = getRandomIntInRange(250,75);
        String fileImage = null;
        File imageDir = null;
        fileImage = Environment.getExternalStorageDirectory().toString()
                + "/DCIM/Camera/" + cameraImages.get(position);
        imageDir = new File(fileImage);
        Bitmap bmp = convertToBitmap(imageDir);
        holder.photo.setImageBitmap(bmp);
        *//**//*holder.countryName.setText(cameraImages.get(position).getName());
        holder.countryPhoto.setImageResource(cameraImages.get(position).getPhoto());*//**//*
    }*//*

    @Override
    public int getItemCount() {
        return this.cameraImages.size();
    }

    protected int getRandomIntInRange(int max, int min){
        return mRandom.nextInt((max-min)+min)+min;
    }

    public static Bitmap convertToBitmap(File file) {
        URL url = null;
        try {
            url = file.toURL();
        } catch (MalformedURLException e1) {
            //Log.d(TAG, e1.toString());
        }//catch

        Bitmap bmp = null;
        try {
            bmp = BitmapFactory.decodeStream(url.openStream());
            //bmp.recycle();
        } catch (Exception e) {
            //Log.d(TAG, "Exception: "+e.toString());
        }//catch
        return bmp;
    }


    public class CameraViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView photo;

        public CameraViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            photo = (ImageView) itemView.findViewById(R.id.showImg_odd);
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
        }
    }
*/

    private List<ItemObjects> itemList;
    private Context context;

    public SelectPhotoAdapter(SelectPhotoActivity context, List<String> cameraImages) {
        this.cameraImages = cameraImages;
        this.context = context;
    }


    @Override
    public CameraViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_select_photo, null);
        CameraViewHolder rcv = new CameraViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(CameraViewHolder holder, int position) {
        holder.card_view.getLayoutParams().height = getRandomIntInRange(375,175);
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;

        String fileImage = null;
        File imageDir = null;
       // holder.txt.setText(position);
        Bitmap bm = BitmapFactory.decodeFile(cameraImages.get(position),options);
        holder.photo.setImageBitmap(bm);
        /*Bitmap bitmap = BitmapFactory.decodeFile(cameraImages.get(position));
       // File f = new File(listOfImages.get(position));

       // holder.txtTitle.setText(f.getName());
        holder.photo.setImageBitmap(bitmap);*/




       /* fileImage = Environment.getExternalStorageDirectory().toString()
                + "/DCIM/Camera/" + cameraImages.get(position);
        imageDir = new File(fileImage);
        Bitmap bmp = convertToBitmap(imageDir);
        holder.photo.setImageBitmap(bmp);*/
    }


    public static Bitmap convertToBitmap(File file) {
        URL url = null;
        try {
            url = file.toURL();
        } catch (MalformedURLException e1) {
            //Log.d(TAG, e1.toString());
        }//catch

        Bitmap bmp = null;
        try {
            bmp = BitmapFactory.decodeStream(url.openStream());
            //bmp.recycle();
        } catch (Exception e) {
            //Log.d(TAG, "Exception: "+e.toString());
        }//catch
        return bmp;
    }

    protected int getRandomIntInRange(int max, int min){
        return mRandom.nextInt((max-min)+min)+min;
    }


    @Override
    public int getItemCount() {
        return this.cameraImages.size();
    }
}