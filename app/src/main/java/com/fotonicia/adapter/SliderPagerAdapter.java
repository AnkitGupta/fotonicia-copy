package com.fotonicia.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fotonicia.R;
import com.fotonicia.category.ItemActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Wasim on 11-06-2015.
 */
public class SliderPagerAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private Context context;
    private Activity activity;
  //  List<String> image_arraylist;
    List<Integer> image_arraylist;

   /* public SliderPagerAdapter(Activity activity, ArrayList<String> image_arraylist) {
        this.activity = activity;
        this.image_arraylist = image_arraylist;
    }*/

    public SliderPagerAdapter(ItemActivity activity, ArrayList<Integer> imagesArray) {
        this.activity = activity;
        this.image_arraylist = imagesArray;
    }

    /*public SliderPagerAdapter(Activity activity, List<BannerDetails> bannerDet) {
        this.activity = activity;
        this.image_arraylist = bannerDet;
    }*/

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.row_sliding_items, container, false);
        ImageView im_slider = (ImageView) view.findViewById(R.id.im_slider);
        im_slider.setScaleType(ImageView.ScaleType.FIT_XY);
        Picasso.with(activity.getApplicationContext())
                .load(image_arraylist.get(position))
                .placeholder(R.mipmap.ic_launcher) // optional
               // .error(R.mipmap.ic_launcher)         // optional
                .into(im_slider);


      /*  im_slider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(getApplicationContext(),MedicinesListActivity.class);
                intent.putExtra("subCatName", "SubCat Medicines");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getApplicationContext().startActivity(intent);
            }
        });*/
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return image_arraylist.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}