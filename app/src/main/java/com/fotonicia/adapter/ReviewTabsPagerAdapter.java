package com.fotonicia.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.fotonicia.category.RatingsAndReviewsFragment;
import com.fotonicia.category.WriteAReviewFragment;

public class ReviewTabsPagerAdapter extends FragmentPagerAdapter {

    public ReviewTabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int pos) {
        switch(pos) {

            case 0: return RatingsAndReviewsFragment.newInstance("FirstFragment, Instance 1");
            case 1: return WriteAReviewFragment.newInstance("SecondFragment, Instance 1");
            default: return RatingsAndReviewsFragment.newInstance("ThirdFragment, Default");
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

}