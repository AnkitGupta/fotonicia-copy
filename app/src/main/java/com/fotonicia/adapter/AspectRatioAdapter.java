package com.fotonicia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.fotonicia.R;
import com.fotonicia.category.ItemActivity;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.model.SubCategoryItemDetails;

import java.util.List;

/**
 * Created by moumita on 3/24/2017.
 */
public class AspectRatioAdapter extends RecyclerView.Adapter<AspectRatioAdapter.MyViewHolder> {

    // private List<SubCategoryItemDetails> items;
    private List<String> items;
    private Context context;

    public AspectRatioAdapter(List<String> strAspectRatio) {
        this.items = strAspectRatio;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CustomRegularHGRTextView txtRatio;
        private RelativeLayout layAspectRatio;


        public MyViewHolder(View view) {
            super(view);
            txtRatio = (CustomRegularHGRTextView) itemView.findViewById(R.id.txtRatio);
            layAspectRatio = (RelativeLayout) itemView.findViewById(R.id.layAspectRatio);
        }
    }

   /* public AspectRatioAdapter(List<SubCategoryItemDetails> items) {
        this.items = items;
    }*/


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_aspect_ratio, parent, false);

        context = parent.getContext();
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        //SubCategoryItemDetails details = items.get(position);
        holder.txtRatio.setText(items.get(position));
        /*holder.txtCategoryName.setText(details.getSubCatName());
        Picasso.with(context).load(R.drawable.category).into(holder.imgSubCat);*/

        holder.layAspectRatio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //holder.layAspectRatio.setBackgroundResource(context.getResources().getColor(R.drawable.create_oval_filled_blue));
            }
        });


    }

    @Override
    public int getItemCount() {
        return items.size();
    }


}
