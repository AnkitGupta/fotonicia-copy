package com.fotonicia.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fotonicia.R;
import com.fotonicia.category.SubCategoryItemActivity;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.model.SubCategoryDetails;
import com.fotonicia.model.SubCategoryItemDetails;
import com.fotonicia.user.OTPActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by moumita on 3/24/2017.
 */
public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.CustomViewHolder> {
    private List<SubCategoryDetails> feedItemList;
    private Context mContext;

    public SubCategoryAdapter(Context context, List<SubCategoryDetails> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;

    }

    @Override
    public int getItemViewType(int position) {

        /*if (position % 2 == 1) {
            return 1;
        } else if (position % 2 == 0) {
            return 2;
        } else {
            return 3;
        }*/

        if (position % 2 == 0) {
            return 1;
        }
        else
        return 2;
        /*else if (position % 2 == 0) {
            return 2;
        } else {
            return 3;
        }*/
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case 1:
                View viewONE = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_odd_subcategory, parent, false);
                CustomViewHolder rowONE = new CustomViewHolder(viewONE);
                return rowONE;

            case 2:
                View viewTWO = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_even_subcategory, parent, false);
                CustomViewHolder rowTWO = new CustomViewHolder(viewTWO);
                return rowTWO;

           /* case 3:
                View viewTHREE = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_even_subcategory, parent, false);
                CustomViewHolder rowTHREE = new CustomViewHolder(viewTHREE);
                return rowTHREE;*/

        }
        return null;
    }


    @Override
    public void onBindViewHolder(final CustomViewHolder holder, int position) {
       // SubCategoryDetails dc_list = feedItemList.get(position);
        final SubCategoryDetails details = feedItemList.get(position);
        final int pos = position * 2;

        holder.txtCategoryName.setText(details.getSubCatName());
        Picasso.with(mContext).load(R.drawable.category).into(holder.imgSubCat);

        holder.laySubCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, details.getSubCatName(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, SubCategoryItemActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    private void setAnimation(FrameLayout container, int position) {
        Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
        container.startAnimation(animation);
    }

    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0 / 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout laySubCat;
        private ImageView imgSubCat;
        private CustomRegularHGRTextView txtCategoryName;

        public CustomViewHolder(View itemView) {
            super(itemView);
            laySubCat = (LinearLayout) itemView.findViewById(R.id.laySubCat);
            imgSubCat = (ImageView) itemView.findViewById(R.id.imgSubCat);
            txtCategoryName = (CustomRegularHGRTextView) itemView.findViewById(R.id.txtCategoryName);

        }
    }
}
