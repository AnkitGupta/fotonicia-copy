package com.fotonicia;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fotonicia.home.HomeActivity;
import com.fotonicia.user.LoginActivity;
import com.fotonicia.user.RegisterActivity;
import com.fotonicia.util.TrackGPS;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class IntroActivity extends Activity implements View.OnClickListener {
    private LinearLayout layLogin, laySignUp, laySkip;
    private Activity activity;
    private TrackGPS gps;
    private double longitude, latitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_intro);
        activity = this;
        CheckEnableGPS();
        getContents();
        dummy();
    }

    private void dummy() {

    }

    private void getContents() {

        layLogin = (LinearLayout) findViewById(R.id.layLogin);
        layLogin.setOnClickListener(this);

        laySignUp = (LinearLayout) findViewById(R.id.laySignUp);
        laySignUp.setOnClickListener(this);

        laySkip = (LinearLayout) findViewById(R.id.laySkip);
        laySkip.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layLogin:
                Intent intent_login = new Intent(this, LoginActivity.class);
                startActivity(intent_login);
                break;

            case R.id.laySignUp:
                Intent intent_signup = new Intent(this, RegisterActivity.class);
                startActivity(intent_signup);
                break;

            case R.id.laySkip:
                Intent intent_home = new Intent(this, HomeActivity.class);
                startActivity(intent_home);
                finish();
                break;
        }
    }

    private void CheckEnableGPS() {
        gps = new TrackGPS(IntroActivity.this);
        if (gps.canGetLocation()) {
            longitude = gps.getLongitude();
            latitude = gps.getLatitude();
            Geocoder geocoder = new Geocoder(IntroActivity.this, Locale.getDefault());
            //  Toast.makeText(getApplicationContext(), "Longitude:" + Double.toString(longitude) + "\nLatitude:" + Double.toString(latitude), Toast.LENGTH_SHORT).show();

            TrackGPS getLocation = new TrackGPS(activity);
            double latitude = getLocation.getLatitude();
            double longitude = getLocation.getLongitude();

            getAddress(latitude, longitude);

        } else {
            gps.showSettingsAlert();
        }
    }


    public void getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(activity, Locale.getDefault());

        try {
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(lat, lng, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses.size() > 0) {
                String countryName = addresses.get(0).getCountryName();
                Locale.getDefault().getCountry();
                Toast.makeText(activity, "Country Name >> " +countryName, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
