package com.fotonicia.controllers;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.fotonicia.common.Constants;
import com.fotonicia.interfaces.ConnManagerInterface;
import com.fotonicia.interfaces.DisplableInterface;
import com.fotonicia.model.LoginDetails;
import com.fotonicia.model.RegistrationDetails;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class MainController implements ConnManagerInterface {

    private DisplableInterface iDispInterface;
    private Context mContext;
    private VolleyConnection connection;
    private byte TYPE = -1;
    private DataParser iParser;
    private String methodName = null;

    // private DatabaseFunctions databaseFunctions;
    public MainController(Activity activity, String methodName, DisplableInterface dispInterface, byte type) {
        this.mContext = activity.getApplicationContext();
        this.iDispInterface = dispInterface;
        this.TYPE = type;
        iParser = DataParser.getParseInstance(activity);
        this.methodName = methodName;
        connection = new VolleyConnection(activity, this);
    }

    public void RequestService(JSONObject jsonRequest) {
        connection.executeRequest(methodName, jsonRequest, mContext);
    }

    public void RequestServicePUT(JSONObject jsonRequest) {
        connection.executeRequestPUT(methodName, jsonRequest, mContext);
    }

    public void RequestServiceVerifyOTPGET(JSONObject jsonRequest) {
        connection.executeRequestVerifyOTPGET(methodName, jsonRequest, mContext);
    }

    public void RequestServiceResendOTPGET(JSONObject jsonRequest) {
        connection.executeRequestResendOTPGET(methodName, jsonRequest, mContext);
    }

    public void RequestServiceServiceGet(JSONObject jsonRequest) {
        connection.executeRequestServiceGet(methodName, jsonRequest, mContext);
    }

    public void RequestServiceBasicProfieGet(JSONObject jsonRequest) {
        connection.executeRequestBasicProfieGet(methodName, jsonRequest, mContext);
    }

    public void RequestServiceEditProfileGet(JSONObject jsonRequest) {
        connection.executeRequestEditProfileGET(methodName, jsonRequest, mContext);
    }

    public void RequestServiceProximityGet(JSONObject jsonRequest) {
        connection.executeRequestProximityGET(methodName, jsonRequest, mContext);
    }

    public void RequestServicePutUpdateProfile(JSONObject jobj) {
        connection.executeRequestUpdateProfilePut(methodName, jobj, mContext);
    }

    public void RequestServiceAvailabilityGet(JSONObject jsonRequest) {
        connection.executeRequestAvailabilityGET(methodName, jsonRequest, mContext);
    }

    public void RequestServiceReviewsGet(JSONObject jsonRequest) {
        connection.executeRequestReviewsGET(methodName, jsonRequest, mContext);
    }


    @Override
    public void successResponseProcess(String response) {
        // TODO Auto-generated method stub
        switch (TYPE) {
            case Constants.Login:
                System.out.println("response" +response);
                if (response != null) {
                    /*{"MaxJsonLength":null,"Data":{"StatusCode":400,"Result":null,"Status":"Failed","UserPhoneNumber":null,"ErrorMessage":"C405","ID":null,"UserRegdID":null,"UserToken":null,"UserName":null},"Settings":null,"RecursionLimit":null,"ContentEncoding":null,"JsonRequestBehavior":0,"ContentType":null}*/
                    LoginDetails loginMessage = iParser.getLoginDetails(response);
                    Log.e("Login Message", response);
                    iDispInterface.setScreenData(loginMessage, Constants.Login, response);
                }
                break;


            case Constants.Registration:
                if (response != null) {
                    System.out.println(response);
                    RegistrationDetails regMessage = iParser.getRegistrationDetails(response);
                    Log.e("Registration", response);
                    iDispInterface.setScreenData(regMessage, Constants.Registration, response);
                }
                break;

/*
            case Constants.Dashboard:
                if (response != null) {
                    System.out.println(response);
                    //DashboardDetails dashboardMessage = iParser.getDashboardDetails(response);
                    AssetManager assetManager = mContext.getAssets();
                    // To load text file
                    InputStream input;
                    try {
                        input = assetManager.open("dashboard_api.txt");

                        int size = input.available();
                        byte[] buffer = new byte[size];
                        input.read(buffer);
                        input.close();

                        // byte buffer into a string
                        String text = new String(buffer);
                        DashboardDetails dashboardMessage = iParser.getDashboardDetails(text);
                        Log.e("Dashboard", text);
                        iDispInterface.setScreenData(dashboardMessage, Constants.Dashboard, response);
                       // txtContent.setText(text);
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }



                }
                break;*/

        }
    }

    @Override
    public void failedResponseProcess(String response) {
        if (response != null) {
            Log.e("Response", "Failed" + response);
            // NotificationUtils.showNotificationToast(mContext,response);
        }
    }

    @Override
    public void excuteError(String error) {

    }
}