package com.fotonicia.controllers;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fotonicia.common.Constants;
import com.fotonicia.common.LoaderView;
import com.fotonicia.interfaces.ConnManagerInterface;
import com.fotonicia.util.Utils;
import com.android.volley.Request;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyConnection {

    String iContent = null;
    private ConnManagerInterface iConnInterface;
    private String tag_string_req = "string_req1";
    private RequestQueue requestQueue;
    private Activity mActivity;
    private LoaderView loader;
    private String URL;

    public VolleyConnection(Activity activity, MainController connInterface) {
        this.iConnInterface = connInterface;
        mActivity = activity;
        loader = LoaderView.getInstance(mActivity);
    }

    public void executeRequest(String url, JSONObject parm, Context context) {
        makeJsonRequest(url, parm, context);
    }

    public void executeRequestPUT(String url, JSONObject parm, Context context) {
        makeJsonRequestPUT(url, parm, context);
    }

    public void executeRequestVerifyOTPGET(String url, JSONObject parm, Context context) {
        makeJsonRequestVerifyOTPGET(url, parm, context);
    }

    public void executeRequestResendOTPGET(String url, JSONObject parm, Context context) {
        makeJsonRequestResendOTPGET(url, parm, context);
    }

    public void executeRequestServiceGet(String url, JSONObject parm, Context context) {
        makeJsonRequestServiceGET(url, parm, context);
    }

    public void executeRequestBasicProfieGet(String url, JSONObject parm, Context context) {
        makeJsonRequestbasicProfileGET(url, parm, context);
    }


    public void executeRequestEditProfileGET(String url, JSONObject parm, Context context) {
        makeJsonRequestEditProfileGET(url, parm, context);
    }

    public void executeRequestProximityGET(String url, JSONObject parm, Context context) {
        makeJsonRequestProximityGET(url, parm, context);
    }

    public void executeRequestUpdateProfilePut(String methodName, JSONObject jobj, Context mContext) {
        makeJsonRequestUpdateProfilePut(methodName, jobj, mContext);
    }

    public void executeRequestAvailabilityGET(String url, JSONObject parm, Context context) {
        makeJsonRequestAvailabilityGET(url, parm, context);
    }
    public void executeRequestReviewsGET(String url, JSONObject parm, Context context) {
        makeJsonRequestReviewsGET(url, parm, context);
    }

    private void showProgressDialog() {
        loader.run(false);
    }

    private void hideProgressDialog() {
        loader.dismis();
    }


    public void makeJsonRequest(String url, JSONObject jsonObject, final Context context) {
        URL = Constants.REQUEST_BASE_URL + url;
        showProgressDialog();
        requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("", "Response json: " + jsonObject);
                iConnInterface.successResponseProcess("" + jsonObject);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                if (volleyError instanceof TimeoutError) {
                    iConnInterface.failedResponseProcess("Connection Time Out");
                    Utils.showToast("Connection time out", context);

                } else if (volleyError instanceof NoConnectionError) {
                    iConnInterface.failedResponseProcess("No Connection found");
                    Utils.showToast("No Connection found", context);

                } else {
                    iConnInterface.failedResponseProcess("Try Again");
                    Utils.showToast("Please try again", context);

                }
                hideProgressDialog();
                return;
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*if(authRequiredInHeader)
                    return getHeader();
                else*/
                return super.getHeaders();
            }
        };
        try {
            Log.e("", "Request begin: " + request.getHeaders());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        request.setShouldCache(false);
        requestQueue.add(request);

    }


    public void makeJsonRequestPUT(String url, JSONObject jsonObject, Context context) {
        URL = Constants.REQUEST_BASE_URL + url;
        showProgressDialog();
        requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("", "Response json: " + jsonObject);
                iConnInterface.successResponseProcess("" + jsonObject);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                if (volleyError instanceof TimeoutError) {
                    iConnInterface.failedResponseProcess("Connection Time Out");

                } else if (volleyError instanceof NoConnectionError) {
                    iConnInterface.failedResponseProcess("No Connection found");

                } else {
                    iConnInterface.failedResponseProcess("Try Again");
                }
                hideProgressDialog();
                return;
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*if(authRequiredInHeader)
                    return getHeader();
                else*/
                return super.getHeaders();
            }
        };
        try {
            Log.e("", "Request begin: " + request.getHeaders());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        request.setShouldCache(false);
        requestQueue.add(request);

    }


    public void makeJsonRequestVerifyOTPGET(String url, final JSONObject jsonObject, Context context) {
        if (jsonObject != null) {
            try {
                String userOTP = null, userRegdID = null, QuickBloxID = null;
                userOTP = jsonObject.getString("userOTP");
                userRegdID = jsonObject.getString("userRegdID");
                QuickBloxID = jsonObject.getString("QuickBloxID");

                url = url + "?userOTP=" + userOTP + "&userRegdID=" + userRegdID + "&QuickBloxID=" + QuickBloxID;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        URL = Constants.REQUEST_BASE_URL + url;
        showProgressDialog();
        requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, jsonObject, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("", "Response json: " + jsonObject);
                iConnInterface.successResponseProcess("" + jsonObject);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                if (volleyError instanceof TimeoutError) {
                    iConnInterface.failedResponseProcess("Connection Time Out");

                } else if (volleyError instanceof NoConnectionError) {
                    iConnInterface.failedResponseProcess("No Connection found");

                } else {
                    iConnInterface.failedResponseProcess("Try Again");


                }
                hideProgressDialog();
                return;
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*if(authRequiredInHeader)
                    return getHeader();
                else*/
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                if (jsonObject != null) {
                    try {
                        String userOTP = null, userRegdID = null, QuickBloxID = null;
                        userOTP = jsonObject.getString("userOTP");
                        userRegdID = jsonObject.getString("userRegdID");
                        QuickBloxID = jsonObject.getString("QuickBloxID");
                        params.put("userOTP", userOTP);
                        params.put("userRegdID", userRegdID);
                        params.put("QuickBloxID", QuickBloxID);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                return params;
            }
        };
        try {
            Log.e("", "Request begin: " + request.getHeaders());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        request.setShouldCache(false);
        requestQueue.add(request);

    }

    public void makeJsonRequestResendOTPGET(String url, final JSONObject jsonObject, Context context) {
        if (jsonObject != null) {
            try {
                String UserRegdID = null, NewNumber = null;
                UserRegdID = jsonObject.getString("UserRegdID");
                NewNumber = jsonObject.getString("NewNumber");

                url = url + "?UserRegdID=" + UserRegdID + "&NewNumber=" + NewNumber;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        URL = Constants.REQUEST_BASE_URL + url;
        showProgressDialog();
        requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, jsonObject, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("", "Response json: " + jsonObject);
                iConnInterface.successResponseProcess("" + jsonObject);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                if (volleyError instanceof TimeoutError) {
                    iConnInterface.failedResponseProcess("Connection Time Out");

                } else if (volleyError instanceof NoConnectionError) {
                    iConnInterface.failedResponseProcess("No Connection found");

                } else {
                    iConnInterface.failedResponseProcess("Try Again");


                }
                hideProgressDialog();
                return;
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*if(authRequiredInHeader)
                    return getHeader();
                else*/
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

				/*if(jsonObject!=null)
                {
					try {
						String userOTP = null, userRegdID = null, QuickBloxID = null;
						userOTP = jsonObject.getString("userOTP");
						userRegdID = jsonObject.getString("userRegdID");
						QuickBloxID = jsonObject.getString("QuickBloxID");
						params.put("userOTP", userOTP);
						params.put("userRegdID", userRegdID);
						params.put("QuickBloxID", QuickBloxID);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}*/

                return params;
            }
        };
        try {
            Log.e("", "Request begin: " + request.getHeaders());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        request.setShouldCache(false);
        requestQueue.add(request);

    }


    public void makeJsonRequestServiceGET(String url, final JSONObject jsonObject, Context context) {
        if (jsonObject != null) {
            try {
                String isFreeVersion = null;
                isFreeVersion = jsonObject.getString("isFreeVersion");

                url = url + "?isFreeVersion=" + true;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        URL = Constants.REQUEST_BASE_URL + url;
        showProgressDialog();
        requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, jsonObject, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("", "Response json: " + jsonObject);
                iConnInterface.successResponseProcess("" + jsonObject);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                if (volleyError instanceof TimeoutError) {
                    iConnInterface.failedResponseProcess("Connection Time Out");

                } else if (volleyError instanceof NoConnectionError) {
                    iConnInterface.failedResponseProcess("No Connection found");

                } else {
                    iConnInterface.failedResponseProcess("Try Again");


                }
                hideProgressDialog();
                return;
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*if(authRequiredInHeader)
                    return getHeader();
                else*/
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

				/*if(jsonObject!=null)
				{
					try {
						String userOTP = null, userRegdID = null, QuickBloxID = null;
						userOTP = jsonObject.getString("userOTP");
						userRegdID = jsonObject.getString("userRegdID");
						QuickBloxID = jsonObject.getString("QuickBloxID");
						params.put("userOTP", userOTP);
						params.put("userRegdID", userRegdID);
						params.put("QuickBloxID", QuickBloxID);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}*/

                return params;
            }
        };
        try {
            Log.e("", "Request begin: " + request.getHeaders());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        request.setShouldCache(false);
        requestQueue.add(request);

    }


    public void makeJsonRequestbasicProfileGET(String url, final JSONObject jsonObject, Context context) {
        if (jsonObject != null) {
            try {
                String userRegdID = null;
                userRegdID = jsonObject.getString("userRegdID");

                url = url + userRegdID;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        URL = Constants.REQUEST_BASE_URL + url;
        showProgressDialog();
        requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, jsonObject, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("", "Response json: " + jsonObject);
                iConnInterface.successResponseProcess("" + jsonObject);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                if (volleyError instanceof TimeoutError) {
                    iConnInterface.failedResponseProcess("Connection Time Out");

                } else if (volleyError instanceof NoConnectionError) {
                    iConnInterface.failedResponseProcess("No Connection found");

                } else {
                    iConnInterface.failedResponseProcess("Try Again");


                }
                hideProgressDialog();
                return;
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*if(authRequiredInHeader)
                    return getHeader();
                else*/
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

				/*if(jsonObject!=null)
				{
					try {
						String userOTP = null, userRegdID = null, QuickBloxID = null;
						userOTP = jsonObject.getString("userOTP");
						userRegdID = jsonObject.getString("userRegdID");
						QuickBloxID = jsonObject.getString("QuickBloxID");
						params.put("userOTP", userOTP);
						params.put("userRegdID", userRegdID);
						params.put("QuickBloxID", QuickBloxID);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}*/

                return params;
            }
        };
        try {
            Log.e("", "Request begin: " + request.getHeaders());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        request.setShouldCache(false);
        requestQueue.add(request);

    }


    public void makeJsonRequestEditProfileGET(String url, final JSONObject jsonObject, Context context) {
        if (jsonObject != null) {
            try {
                String UserRegdID = null;
                UserRegdID = jsonObject.getString("UserID");

                //url = url+"?UserRegdID="+UserRegdID;
                url = url + UserRegdID;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        URL = Constants.REQUEST_BASE_URL + url;
        showProgressDialog();
        requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, jsonObject, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("", "Response json: " + jsonObject);
                iConnInterface.successResponseProcess("" + jsonObject);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                if (volleyError instanceof TimeoutError) {
                    iConnInterface.failedResponseProcess("Connection Time Out");

                } else if (volleyError instanceof NoConnectionError) {
                    iConnInterface.failedResponseProcess("No Connection found");

                } else {
                    iConnInterface.failedResponseProcess("Try Again");


                }
                hideProgressDialog();
                return;
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*if(authRequiredInHeader)
                    return getHeader();
                else*/
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

			/*	if(jsonObject!=null)
				{
					try {
						String userRegdID = null;
						userRegdID = jsonObject.getString("UserID");
						params.put("UserID", userRegdID);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
*/
                return params;
            }
        };
        try {
            Log.e("", "Request begin: " + request.getHeaders());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        request.setShouldCache(false);
        requestQueue.add(request);

    }


    public void makeJsonRequestProximityGET(String url, final JSONObject jsonObject, Context context) {
        if (jsonObject != null) {
            try {
                String UserRegdID = null;
                UserRegdID = jsonObject.getString("UserID");

                url = url + "?UserID=" + UserRegdID;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        URL = Constants.REQUEST_BASE_URL + url;
        showProgressDialog();
        requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, jsonObject, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("", "Response json: " + jsonObject);
                iConnInterface.successResponseProcess("" + jsonObject);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                if (volleyError instanceof TimeoutError) {
                    iConnInterface.failedResponseProcess("Connection Time Out");

                } else if (volleyError instanceof NoConnectionError) {
                    iConnInterface.failedResponseProcess("No Connection found");

                } else {
                    iConnInterface.failedResponseProcess("Try Again");


                }
                hideProgressDialog();
                return;
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*if(authRequiredInHeader)
                    return getHeader();
                else*/
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                if (jsonObject != null) {
                    try {
                        String userRegdID = null;
                        userRegdID = jsonObject.getString("UserID");
                        params.put("UserID", userRegdID);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                return params;
            }
        };
        try {
            Log.e("", "Request begin: " + request.getHeaders());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        request.setShouldCache(false);
        requestQueue.add(request);

    }


   /* public void makeJsonRequestPUT(String url, JSONObject jsonObject, Context context) {
        URL = Constants.REQUEST_BASE_URL + url;
        showProgressDialog();
        requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("", "Response json: " + jsonObject);
                iConnInterface.successResponseProcess("" + jsonObject);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                if (volleyError instanceof TimeoutError) {
                    iConnInterface.failedResponseProcess("Connection Time Out");

                } else if (volleyError instanceof NoConnectionError) {
                    iConnInterface.failedResponseProcess("No Connection found");

                } else {
                    iConnInterface.failedResponseProcess("Try Again");
                }
                hideProgressDialog();
                return;
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }
        };
        try {
            Log.e("", "Request begin: " + request.getHeaders());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        request.setShouldCache(false);
        requestQueue.add(request);

    }*/


    public void makeJsonRequestUpdateProfilePut(String url, final JSONObject jsonObject, Context context) {
        if (jsonObject != null) {
            try {
                String UserRegdID = null, profile_name = null, dob = null, gender = null, aboutyou = null, proximity = null, user_lat = null, user_long = null;

                if (!(jsonObject.getString("UserID") == "" || jsonObject.getString("UserID") == null))
                    UserRegdID = jsonObject.getString("UserID");
                else
                    UserRegdID = "";
                if (!(jsonObject.getString("ProfileName") == "" || jsonObject.getString("ProfileName") == null))
                    profile_name = jsonObject.getString("ProfileName");
                else
                    profile_name = "";
                if (!(jsonObject.getString("DOB") == "" || jsonObject.getString("DOB") == null))
                    dob = jsonObject.getString("DOB");
                else
                    dob = "";
                if (!(jsonObject.getString("UserGender") == "" || jsonObject.getString("UserGender") == null))
                    gender = jsonObject.getString("UserGender");
                else
                    gender = "";
                if (!(jsonObject.getString("UserAboutYou") == "" || jsonObject.getString("UserAboutYou") == null))
                    aboutyou = jsonObject.getString("UserAboutYou");
                else
                    aboutyou = "";
                if (!(jsonObject.getString("UserDefaultProximity") == "" || jsonObject.getString("UserDefaultProximity") == null))
                    proximity = jsonObject.getString("UserDefaultProximity");
                else
                    proximity = "";
                user_lat = "28.539934";
                user_long = "77.401408";


                url = url + "?UserID=" + UserRegdID + "&ProfileName=" + profile_name + "&DOB=" + "2016-07-12" + "&UserGender=" + gender + "&UserAboutYou=" + aboutyou + "&UserDefaultProximity=" + proximity + "&sUserLatCoordinates=" + user_lat + "&sUserLonCoordinates=" + user_long;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        URL = Constants.REQUEST_BASE_URL + url;
        showProgressDialog();
        requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, URL, jsonObject, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("", "Response json: " + jsonObject);
                iConnInterface.successResponseProcess("" + jsonObject);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                if (volleyError instanceof TimeoutError) {
                    iConnInterface.failedResponseProcess("Connection Time Out");

                } else if (volleyError instanceof NoConnectionError) {
                    iConnInterface.failedResponseProcess("No Connection found");

                } else {
                    iConnInterface.failedResponseProcess("Try Again");


                }
                hideProgressDialog();
                return;
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*if(authRequiredInHeader)
                    return getHeader();
                else*/
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                if (jsonObject != null) {
                    try {
                        String UserRegdID = null, profile_name = null, dob = null, gender = null, aboutyou = null, proximity = null;
                        UserRegdID = jsonObject.getString("UserID");
                        profile_name = jsonObject.getString("ProfileName");
                        dob = jsonObject.getString("DOB");
                        gender = jsonObject.getString("UserGender");
                        aboutyou = jsonObject.getString("UserAboutYou");
                        proximity = jsonObject.getString("UserDefaultProximity");
                        params.put("UserID", UserRegdID);
                        params.put("ProfileName", profile_name);
                        params.put("DOB", "2016-07-12");
                        params.put("UserGender", gender);
                        params.put("UserAboutYou", "ajhdkjsha");
                        params.put("UserDefaultProximity", proximity);
                        params.put("sUserLatCoordinates", "28.539934");
                        params.put("sUserLonCoordinates", "77.401408");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                return params;
            }
        };
        try {
            Log.e("", "Request begin: " + request.getHeaders());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        request.setShouldCache(false);
        requestQueue.add(request);


    }
	
	
	
	public void makeJsonRequestAvailabilityGET(String url, final JSONObject jsonObject, Context context)
    {
        if(jsonObject!=null)
        {
            try {
                String UserRegdID = null;
                UserRegdID = jsonObject.getString("UserID");

                url = url+"?UserID="+UserRegdID;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        URL = Constants.REQUEST_BASE_URL + url;
        showProgressDialog();
        requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, jsonObject, new Response.Listener<JSONObject>() {



            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("", "Response json: " + jsonObject);
                iConnInterface.successResponseProcess(""+jsonObject);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                if (volleyError instanceof TimeoutError){
                    iConnInterface.failedResponseProcess("Connection Time Out");

                }else if (volleyError instanceof NoConnectionError){
                    iConnInterface.failedResponseProcess("No Connection found");

                }
                else
                {
                    iConnInterface.failedResponseProcess("Try Again");


                }
                hideProgressDialog();
                return;
            }

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*if(authRequiredInHeader)
                    return getHeader();
                else*/
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }



            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                if(jsonObject!=null)
                {
                    try {
                        String userRegdID = null;
                        userRegdID = jsonObject.getString("UserID");
                        params.put("UserID", userRegdID);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                return params;
            }
        };
        try {
            Log.e("", "Request begin: " + request.getHeaders());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        request.setShouldCache(false);
        requestQueue.add(request);

    }
public void makeJsonRequestReviewsGET(String url, final JSONObject jsonObject, Context context)
    {
        if(jsonObject!=null)
        {
            try {
                String UserRegdID = null,SubServiceID = null;
                UserRegdID = jsonObject.getString("ServiceProviderID");
                SubServiceID = jsonObject.getString("SubServiceID");

                url = url+"?ServiceProviderID="+UserRegdID+"&SubServiceID="+SubServiceID;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        URL = Constants.REQUEST_BASE_URL + url;
        showProgressDialog();
        requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, jsonObject, new Response.Listener<JSONObject>() {



            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.e("", "Response json: " + jsonObject);
                iConnInterface.successResponseProcess(""+jsonObject);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                if (volleyError instanceof TimeoutError){
                    iConnInterface.failedResponseProcess("Connection Time Out");

                }else if (volleyError instanceof NoConnectionError){
                    iConnInterface.failedResponseProcess("No Connection found");

                }
                else
                {
                    iConnInterface.failedResponseProcess("Try Again");


                }
                hideProgressDialog();
                return;
            }

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*if(authRequiredInHeader)
                    return getHeader();
                else*/
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return super.getHeaders();
            }



            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                if(jsonObject!=null)
                {
                    try {
                        String userRegdID = null;
                        userRegdID = jsonObject.getString("ServiceProviderID");
                        params.put("ServiceProviderID", userRegdID);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                return params;
            }
        };
        try {
            Log.e("", "Request begin: " + request.getHeaders());
        } catch (AuthFailureError authFailureError) {
            authFailureError.printStackTrace();
        }
        request.setShouldCache(false);
        requestQueue.add(request);

    }


}
