package com.fotonicia.controllers;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.fotonicia.model.LoginDetails;
import com.fotonicia.model.RegistrationDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DataParser {

    private static DataParser iInstance;
    private static Activity mActivity = null;
    private Context context;

    public DataParser(Activity iActivity) {
        mActivity = iActivity;
        context = mActivity.getApplication();
    }

    public DataParser() {

    }

    public static DataParser getParseInstance(Activity iActivity) {

        if (iInstance == null) {
            if (mActivity != null) {
                iInstance = new DataParser(mActivity);
            } else
                iInstance = new DataParser();
        }
        return iInstance;
    }


    public LoginDetails getLoginDetails(String responce) {//{"status_code":"200","message":"Login successful","user_id":"3"}
        // TODO Auto-generated method stub
        LoginDetails list = new LoginDetails();
        try {
            JSONObject st = new JSONObject(responce);
            //  if(status != null) {

            String statusCode = st.getString("status_code");
            if (statusCode != null) {
                list.setStatusCode(statusCode);
                if (statusCode.equals("200")) {
                    String status = st.getString("message");
                    if (status != null && status.equals("Login successful")) {
                        list.setStatus(st.getString("message"));

                        String userId = st.getString("user_id");
                        //String userRegId = data.getString("e803b99c6cce496788f8b5ad7a09e914");
                        if (userId != null) {
                            list.setUserId(userId);
                        }
                    }
                }
            }


        } catch (JSONException e) {
            Log.e("JsonException", "" + e.getMessage());
            e.printStackTrace();
        }
        return list;
    }


    public RegistrationDetails getRegistrationDetails(String responce) {
        // TODO Auto-generated method stub
        RegistrationDetails list = new RegistrationDetails();
        try {
            JSONObject st = new JSONObject(responce);
            //  if(status != null) {
            String statusCode = st.getString("status_code");
            if (statusCode != null) {
                list.setStatusCode(statusCode);
                if (statusCode.equals("200")) {
                    String status = st.getString("message");
                    if (status != null && status.equals("Login successful")) {
                        list.setStatus(st.getString("Status"));

                        String userId = st.getString("user_id");
                        //String userRegId = data.getString("e803b99c6cce496788f8b5ad7a09e914");
                        if (userId != null) {
                            list.setUserId(userId);
                        }
                    }
                }
            }

        } catch (JSONException e) {
            Log.e("JsonException", "" + e.getMessage());
            e.printStackTrace();
        }
        return list;
    }


    /*public ForgotPasswordDetails getFPDetails(String responce) {
        // TODO Auto-generated method stub
        ForgotPasswordDetails list = new ForgotPasswordDetails();
        try {
            JSONObject st = new JSONObject(responce);
            //  if(status != null) {
            JSONObject data = st.getJSONObject("Data");
            if (data != null) {
                String statusCode = data.getString("StatusCode");
                if (statusCode != null) {
                    list.setStatusCode(statusCode);
                    if (statusCode.equals("200")) {
                        String status = data.getString("Status");
                        if (status != null && status.equals("Success")) {
                            list.setStatus(data.getString("Status"));
                        }
                    }
                }

            }
        } catch (JSONException e) {
            Log.e("JsonException", "" + e.getMessage());
            e.printStackTrace();
        }
        return list;
    }*/


 /*   public DashboardDetails getDashboardDetails(String responce) {
        // TODO Auto-generated method stub
        DashboardDetails list = new DashboardDetails();
        try {
            JSONObject st = new JSONObject(responce);
            //  if(status != null) {
            String statusCode = st.getString("status");
            if (statusCode != null) {
                list.setStatusCode(statusCode);
                if (statusCode.equals("200")) {
                    String status = st.getString("message");
                    if (status != null && status.equals("Successful")) {
                        list.setMessage(st.getString("message"));

                        JSONArray jsonArray = new JSONArray(st.getString("main_category"));
                        List<CategoryDetails> catList = new ArrayList<CategoryDetails>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            CategoryDetails catDetails = new CategoryDetails();
                            JSONObject jsonobject = jsonArray.getJSONObject(i);

                            *//*Get Category Details*//*
                            catDetails.setEntityId(jsonobject.get("cats_id").toString());
                            catDetails.setCategory(jsonobject.get("name").toString());
                            catDetails.setImageUrl(jsonobject.get("image_url").toString());

                            // get the subcategory details
                            if (jsonobject.has("sub_category")) {
                                JSONArray jsonSubArray = new JSONArray(jsonobject.getString("sub_category"));
                                List<SubCategoryDetails> subCat = new ArrayList<SubCategoryDetails>();

                                for (int j = 0; j < jsonSubArray.length(); j++) {
                                    SubCategoryDetails subCategoryDetails = new SubCategoryDetails();
                                    JSONObject jsonSubObject = jsonSubArray.getJSONObject(j);
                                    subCategoryDetails.setSubCategory(jsonSubObject.get("name").toString());
                                    subCategoryDetails.setSubCatId(jsonSubObject.get("subcats_id").toString());
                                    subCategoryDetails.setImageUrl(jsonSubObject.get("image_url").toString());
                                    subCat.add(subCategoryDetails);
                                }
                                catDetails.setSubCategoryList(subCat);
                            }
                            catList.add(catDetails);

                        }
                        list.setCategoryList(catList);

                        *//*Banner list*//*
                        JSONArray jsonBannerArray = new JSONArray(st.getString("banner_details"));
                        List<BannerDetails> bannerList = new ArrayList<BannerDetails>();
                        for (int k = 0; k < jsonBannerArray.length(); k++) {
                            BannerDetails bannerDetails = new BannerDetails();
                            JSONObject jsonBannerObject = jsonBannerArray.getJSONObject(k);

                            *//*Get Banner Details*//*
                            bannerDetails.setUrl(jsonBannerObject.get("banner_url").toString());
                            bannerList.add(bannerDetails);
                        }
                        list.setBannerList(bannerList);

                        JSONObject couponDet = st.getJSONObject("coupon_details");
                        String couponId = couponDet.getString("coupon_id");
                        System.out.println("couponId >> " +couponId);


                    *//*    *//**//*article list*//**//*
                        JSONArray jsonArray_article = new JSONArray(st.getString("article_details"));
                        List<ArticlesDetails> articleList = new ArrayList<ArticlesDetails>();
                        for (int x = 0; x < jsonArray_article.length(); x++) {
                            ArticlesDetails articleDetails = new ArticlesDetails();
                            JSONObject jsonobject_article = jsonArray.getJSONObject(x);

                            *//**//*Get Article Details*//**//*
                            articleDetails.setArticleId(jsonobject_article.get("article_id").toString());
                            articleDetails.setArticleHeader(jsonobject_article.get("article_header").toString());
                            articleDetails.setArticleDescription(jsonobject_article.get("article_description").toString());
                            articleDetails.setImageUrl(jsonobject_article.get("image_url").toString());
                            articleList.add(articleDetails);
                        }
                        list.setArticlesList(articleList);*//*

                        *//*1st article*//*
                            JSONObject article_first = st.getJSONObject("article_details_first");
                        //JSONObject article_first = new JSONObject(st.getString("article_details_first"));
                        String article_id_first = article_first.getString("article_id");
                        String article_header_first = article_first.get("article_header").toString();
                        String article_description_first = article_first.get("article_description").toString();
                        String image_url_first = article_first.get("image_url").toString();

                        list.setArticleIdFirst(article_id_first);
                        list.setArticleHeaderFirst(article_header_first);
                        list.setArticleDescFirst(article_description_first);
                        list.setArticleImageURLFirst(image_url_first);


                        *//*2nd article*//*
                        JSONObject article_second = st.getJSONObject("article_details_second");
                        //JSONObject article_second = new JSONObject(st.getString("article_details_second"));
                        String article_id_second = article_second.get("article_id").toString();
                        String article_header_second = article_second.get("article_header").toString();
                        String article_description_second = article_second.get("article_description").toString();
                        String image_url_second = article_second.get("image_url").toString();

                        list.setArticleIdSecond(article_id_second);
                        list.setArticleHeaderSecond(article_header_second);
                        list.setArticleDescSecond(article_description_second);
                        list.setArticleImageURLSecond(image_url_second);


                        *//*3rd article*//*
                        JSONObject article_third = st.getJSONObject("article_details_third");
                       // JSONObject article_third = new JSONObject(st.getString("article_details_third"));
                        String article_id_third = article_third.get("article_id").toString();
                        String article_header_third = article_third.get("article_header").toString();
                        String article_description_third = article_third.get("article_description").toString();
                        String image_url_third = article_third.get("image_url").toString();

                        list.setArticleIdThird(article_id_third);
                        list.setArticleHeaderThird(article_header_third);
                        list.setArticleDescThird(article_description_third);
                        list.setArticleImageURLThird(image_url_third);


                    }
                }
            }

        } catch (JSONException e) {
            Log.e("JsonException", "" + e.getMessage());
            e.printStackTrace();
        }
        return list;
    }*/

}