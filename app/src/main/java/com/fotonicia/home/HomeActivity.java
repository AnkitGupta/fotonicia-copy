package com.fotonicia.home;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.fotonicia.R;
import com.fotonicia.adapter.HomeCouponsAdapter;
import com.fotonicia.adapter.HomeFormsAdapter;
import com.fotonicia.adapter.LeftNavigationAdapter;
import com.fotonicia.category.SubCategoryActivity;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.photoselection.SelectPhotoActivity;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends ActionBarActivity {

    //First We Declare Titles And Icons For Our Navigation Drawer List View
    //This Icons And Titles Are holded in an Array as you can see

    /*String TITLES[] = {"Home","Help","Account","Like Us","Rate Us","Follow Us","Invite Friends","Sign Out"};
    int ICONS[] = {R.drawable.ic_home_black,R.drawable.help,R.drawable.myprofile,
            R.drawable.like,R.drawable.rate,R.drawable.follow, R.drawable.share,R.drawable.cancel};*/

    String TITLES[] = {"About Us","My Account","Refer and Earn","Offers","Donate to Educate","Help & FAQ","Like Us","Follow Us", "Rate Us", "Contact Us", "Feedback"};
    int ICONS[] = {R.drawable.ic_home_black,R.drawable.myprofile,R.drawable.help, R.drawable.like,R.drawable.rate,R.drawable.follow, R.drawable.share,
            R.drawable.cancel, R.drawable.help,R.drawable.like,R.drawable.rate};
    private List<Integer> arrForms = new ArrayList<>();
    private List<Integer> arrCoupons = new ArrayList<>();
    int coupons[] = {R.drawable.coupon_a, R.drawable.coupon_b, R.drawable.coupon_a, R.drawable.coupon_b, R.drawable.coupon_a, R.drawable.coupon_b};
    int trending_items[] = {R.drawable.trending_item_a, R.drawable.trending_item_a, R.drawable.trending_item_a, R.drawable.trending_item_a, R.drawable.trending_item_a};

    //Similarly we Create a String Resource for the name and email in the header view
    //And we also create a int resource for profile picture in the header view

    String NAME = "Akash Bangad";
    String EMAIL = "akash.bangad@android4devs.com";
    int PROFILE = R.drawable.google;

    private Toolbar toolbar;                              // Declaring the Toolbar Object

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;                  // Declaring Action Bar Drawer Toggle


    private Activity activity;
    private CustomRegularHGRTextView txtCategory;
    private LinearLayout layFeatured, layHandmade;

    private RecyclerView banners_recycler_view;
    private HomeFormsAdapter formsAdapter;

    private RecyclerView coupons_recycler_view;
    private HomeCouponsAdapter couponsAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home);
        activity = this;

        /*Toolbar start */

    /* Assinging the toolbar object ot the view
    and setting the the Action bar to our toolbar
     */
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size

        mAdapter = new LeftNavigationAdapter(TITLES,ICONS,NAME,EMAIL,PROFILE);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager
        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager

        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);

        mDrawerToggle = new ActionBarDrawerToggle(this,Drawer,toolbar,R.string.openDrawer,R.string.closeDrawer){


            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }



        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State

        mDrawerToggle.setDrawerIndicatorEnabled(true);// Drawer object Assigned to the view
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationIcon(R.drawable.back_icon_grey);
        toolbar.setTitle("My App");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        /*Toolbar end*/

/*

        */
/*Banner recyclerview start*//*

        banners_recycler_view = (RecyclerView) findViewById(R.id.banners_recycler_view);
        arrForms.add(R.drawable.banner_a);
        arrForms.add(R.drawable.banner_b);
        arrForms.add(R.drawable.banner_a);
        arrForms.add(R.drawable.banner_b);
        arrForms.add(R.drawable.banner_a);
        arrForms.add(R.drawable.banner_b);
        formsAdapter = new HomeFormsAdapter(arrForms);
        RecyclerView.LayoutManager catLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        banners_recycler_view.setLayoutManager(catLayoutManager);
        banners_recycler_view.setItemAnimator(new DefaultItemAnimator());
        banners_recycler_view.setAdapter(formsAdapter);
        banners_recycler_view.setHasFixedSize(true);


        coupons_recycler_view = (RecyclerView) findViewById(R.id.coupons_recycler_view);
        arrCoupons.add(R.drawable.coupon_a);
        arrCoupons.add(R.drawable.coupon_b);
        arrCoupons.add(R.drawable.coupon_a);
        arrCoupons.add(R.drawable.coupon_b);
        arrCoupons.add(R.drawable.coupon_a);
        arrCoupons.add(R.drawable.coupon_b);
        couponsAdapter = new HomeCouponsAdapter(arrCoupons);
        RecyclerView.LayoutManager catLayoutManager_ = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        coupons_recycler_view.setLayoutManager(catLayoutManager_);
        coupons_recycler_view.setItemAnimator(new DefaultItemAnimator());
        coupons_recycler_view.setAdapter(couponsAdapter);
        coupons_recycler_view.setHasFixedSize(true);
*/


        layFeatured = (LinearLayout) findViewById(R.id.layFeatured);
        layFeatured.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, SubCategoryActivity.class);
                startActivity(intent);
            }
        });

        layHandmade = (LinearLayout) findViewById(R.id.layHandmade);
        layHandmade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, SelectPhotoActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        if (id == R.id.action_user) {
            return true;
        }

        if (id == R.id.action_cart) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}