package com.fotonicia.user;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.fotonicia.R;
import com.fotonicia.common.Constants;
import com.fotonicia.common.WebConstants;
import com.fotonicia.components.CustomBoldTextView;
import com.fotonicia.components.CustomRegularEditText;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.components.CustomRegularTextView;
import com.fotonicia.controllers.MainController;
import com.fotonicia.home.HomeActivity;
import com.fotonicia.interfaces.DisplableInterface;
import com.fotonicia.model.LoginDetails;
import com.fotonicia.util.NotificationUtils;
import com.fotonicia.util.PreferenceManager;
import com.fotonicia.util.SharedPreferenceUtil;
import com.fotonicia.util.Utils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class LoginActivity extends FragmentActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, DisplableInterface {
    PreferenceManager preferenceManager;
    private Activity activity;
    private RelativeLayout layBack, layLogin;
    private LinearLayout layForgotPassword, layOTP, layFacebook, layGoogle;
    private CustomRegularHGRTextView txtHeader;
    private CustomRegularTextView txtSignUp;
    private ImageView checkForOTP;
    private boolean isOTP;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private SignInButton sign_in_button;
    private boolean mSignInClicked, isLogin, mIntentInProgress;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;
    private ConnectionResult mConnectionResult;
    private ProgressDialog mProgressDialog;
    private CustomRegularEditText txtEmail_Mobile, txtPassword;
    String email_login, password_login;
    private int mobile_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        preferenceManager = new PreferenceManager(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.layout_title_bar);
        activity = this;

        getContents();
    }

    private void getContents() {

        layBack = (RelativeLayout) findViewById(R.id.layBack);
        layBack.setOnClickListener(this);

        txtHeader = (CustomRegularHGRTextView) findViewById(R.id.txtHeader);
        txtHeader.setText("Login");

        checkForOTP = (ImageView) findViewById(R.id.checkForOTP);
        checkForOTP.setOnClickListener(this);
        checkForOTP.setBackgroundResource(R.drawable.uncheckbox);
        isOTP = false;

        txtSignUp = (CustomRegularTextView) findViewById(R.id.txtSignUp);
        txtSignUp.setOnClickListener(this);

        layForgotPassword = (LinearLayout) findViewById(R.id.layForgotPassword);
        layForgotPassword.setOnClickListener(this);

        layOTP = (LinearLayout) findViewById(R.id.layOTP);
        layOTP.setOnClickListener(this);

        txtEmail_Mobile = (CustomRegularEditText) findViewById(R.id.txtEmail_Mobile);
        txtPassword = (CustomRegularEditText) findViewById(R.id.txtPassword);

        layLogin = (RelativeLayout) findViewById(R.id.layLogin);
        layLogin.setOnClickListener(this);

        layFacebook = (LinearLayout) findViewById(R.id.layFacebook);
        layFacebook.setOnClickListener(this);

        sign_in_button = (SignInButton) findViewById(R.id.sign_in_button);
        layGoogle = (LinearLayout) findViewById(R.id.layGoogle);
        layGoogle.setOnClickListener(this);
        /*GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();*/

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(new Scope("profile"))
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();

        facebookLogin();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layBack:
                finish();
                break;

            case R.id.txtSignUp:
                Intent intent_reg = new Intent(this, RegisterActivity.class);
                startActivity(intent_reg);
                finish();
                break;

            case R.id.checkForOTP:
                if (!isOTP) {
                    checkForOTP.setBackgroundResource(R.drawable.checkbox);
                    isOTP = true;
                    Intent intent_otp = new Intent(this, OTPActivity.class);
                    intent_otp.putExtra("isOTP", isOTP);
                    startActivityForResult(intent_otp, 2);
                } else {
                    checkForOTP.setBackgroundResource(R.drawable.uncheckbox);
                    isOTP = false;
                }

                break;

            case R.id.layForgotPassword:
                callForgotPasswordDialog();
                break;

            case R.id.layFacebook:
                if (Utils.isNetworkAvailable(activity)) {
                    if (LoginManager.getInstance() != null)
                        LoginManager.getInstance().logOut();
                    loginButton.performClick();
                } else
                    NotificationUtils.showNetworkStatusToast(activity);
                break;

            case R.id.layGoogle:
                signIn();
                sign_in_button.performClick();
                break;

            case R.id.sign_in_button:
                signIn();
                break;

            case R.id.layLogin:
                final InputMethodManager immm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                immm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                loginValidation();
                break;

        }

    }

    private void callForgotPasswordDialog() {
        final android.app.Dialog dialog = new android.app.Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_forgot_password);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

               /*DisplayMetrics displaymetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                int width = (int) ((int)displaymetrics.widthPixels * 0.9);
                int height = (int) ((int)displaymetrics.heightPixels * 0.7);
                dialog.getWindow().setLayout(width,height);*/

        ImageView imgCancel = (ImageView) dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        RelativeLayout laySubmit = (RelativeLayout) dialog.findViewById(R.id.laySubmit);
        laySubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomRegularEditText txtEmail = (CustomRegularEditText) dialog.findViewById(R.id.txtEmail);
                String email = txtEmail.getText().toString().trim();
                if (email != null) {
                    if (Utils.isNetworkAvailable(activity)) {
                        Toast.makeText(activity, "Submitted", Toast.LENGTH_SHORT).show();
                        setDataModelForgot(email_login);
                    } else {
                        NotificationUtils.showNetworkStatusToast(activity);
                    }
                }
            }
        });

        // this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    private void setDataModelForgot(String email) {
        JSONObject obj1 = new JSONObject();

        try {
            obj1.put("email", email);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
               /* MainController controller = new MainController(activity, C.FORGOT_PASSWORD_URL, this, WebServiceConstants.FORGOT_PASSWORD, true,"1");
                controller.RequestService(obj1.toString(),WebServiceConstants.POST);
                Utils.printValue(obj1.toString(), "");*/
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /*Login Form Validation*/
    private void loginValidation() {
        String email = null;
        email_login = txtEmail_Mobile.getText().toString().trim();
        password_login = txtPassword.getText().toString().trim();


        if (email_login == null || email_login.length() < 1) { //username empty
            NotificationUtils.showNotificationToast(this, "Please enter your email or phone number.");
            txtEmail_Mobile.requestFocus();
        } else if (password_login == null || password_login.length() < 1) {
            NotificationUtils.showNotificationToast(this, "Please enter the password.");
            txtPassword.requestFocus();
        } else if (password_login.length() < 7) {
            NotificationUtils.showNotificationToast(this, "Password should be 6-8 characters.");
            txtPassword.requestFocus();
        } else {
            if (!Utils.isValidEmail(email_login)) {
                mobile_number = Integer.parseInt(email_login);
                email_login = "";
            } else {
                email = email_login;
                mobile_number = 0;
            }
            /*if (Utils.isNetworkAvailable(this)) {
                setDataModelLogin(email_login, mobile_number, password_login, Constants.REGISTRATION_CUSTOM);
            } else {
                NotificationUtils.showNetworkStatusToast(this);
            }*/
        }
    }


    /*Set data for the creation of request params*/
    public void setDataModelLogin(String email, String password, String login_Type) {
        ArrayList<String> Data = new ArrayList<String>();
        JSONObject obj1 = new JSONObject();
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            int verCode = pInfo.versionCode;

            obj1.put("hash_key", "api123456789");
            obj1.put("email", email.trim());
            obj1.put("password", password);
            obj1.put("loginType", login_Type);
            obj1.put("social_id", "fghfg1235");

         /*   if (preferenceManager.getRegId() != null)
                obj1.put("DeviceRegistrationID", preferenceManager.getRegId());
            obj1.put("DeviceType", "Android");
            obj1.put("AppVersion", verCode);
            obj1.put("DeviceModel", Build.MODEL);
            obj1.put("DeviceName", Build.DEVICE);
            obj1.put("DeviceOSversion", System.getProperty("os.version"));*/

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject jobj = new JSONObject(obj1.toString());
            // MainController controller = new MainController(LoginActivity.this, WebConstants.LOGIN, this, Constants.Login);
            // controller.RequestService(jobj);
            Log.e("Json Object", "" + jobj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


   /* @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }*/

    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }


    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
        }
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            checkForOTP.setBackgroundResource(R.drawable.uncheckbox);
            isOTP = false;
        }

        if (requestCode == RC_SIGN_IN) {
            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else
        /*Facebook Sign In*/
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    /*Facebook Sign In*/
    private void facebookLogin() {
        loginButton = (LoginButton) findViewById(R.id.login_button);
        List<String> permissions = new ArrayList<>();

        permissions.add("public_profile");
        permissions.add("user_friends");
        permissions.add("email");
        //if(LoginManager.getInstance()!=null)

        loginButton.setReadPermissions(permissions);
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                if (loginResult != null) {
                    try {

                        GraphRequest graphRequest = GraphRequest.newMeRequest(

                                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                                    public void onCompleted(JSONObject jsonObject, GraphResponse response) {

                                        Utils.logDebug("TEST", response.toString());
                                        if (response.getError() != null) {
                                            // handle error
                                        } else {
                                            if (jsonObject != null) {
                                                Utils.logDebug("Response", jsonObject.toString());
                                                String email = jsonObject.optString("email");
                                                String user_id = jsonObject.optString("id");
                                                Utils.logDebug("User Email", "Email:" + email);
                                               /* if (email != null)
                                                    setDataModelLogin(email, user_id, Constants.REGISTRATION_FACEBOOK);
                                                else
                                                    NotificationUtils.showNotificationToast(activity, "Login Failed");*/

                                            }
                                        }
                                    }
                                })/*.executeAsync()*/;
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email");
                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();
                        Utils.printValue("Authentication Token :", "" + loginResult.getAccessToken().getToken());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onCancel() {
                // App code
                Utils.printValue("Facebook Login failed!!", "");
                // NotificationUtils.showNotificationToast(activity,"Failed to login with facebook");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Utils.printValue("Facebook Login failed!!", "");
                // NotificationUtils.showNotificationToast(activity, "Error in login with facebook");
            }

        });
    }


    /*Google Sign In*/
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void getProfileInformation() {
        try {
            mSignInClicked = false;
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String user_id = currentPerson.getId();
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                final String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                Utils.printValue("Gmail Email-", email + " & " + personName);
                //redirectOnHomePage();
               /* if(Utils.isNetworkAvailable(activity))*//*NotificationUtils.showNotificationToast(activity,"Name="+personName+" Email="+email);*//*
                    setDataModelLogin(email, user_id, Constants.REGISTRATION_GOOGLE);
                else
                    NotificationUtils.showNetworkStatusToast(activity);*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // updateUI(false);
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getDisplayName());

            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();

            Log.e(TAG, "Name: " + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl);

           /* txtName.setText(personName);
            txtEmail.setText(email);
            Glide.with(getApplicationContext()).load(personPhotoUrl)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgProfilePic);

            updateUI(true);*/
        } else {
            // Signed out, show unauthenticated UI.
            //  updateUI(false);
        }
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void setScreenData(Object obj, byte type, String Responce) {
        switch (type) {
            case Constants.Login:
                System.out.println("responce >> " + Responce);
                if (obj != null) {
                    LoginDetails loginDet = (LoginDetails) obj;
                    String status = loginDet.getStatusCode();
                    if (status.equals("200")) {
                        preferenceManager.setEmail(email_login);
                        preferenceManager.setUserid(loginDet.getUserId());

                        Intent intentHome = new Intent(LoginActivity.this, HomeActivity.class);
                        intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intentHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        SharedPreferenceUtil.savePreferences(activity, SharedPreferenceUtil.IS_LOGIN, true);
                        startActivity(intentHome);
                        finish();

                    } else {
                        Utils.showToast("Login Unsuccessful", LoginActivity.this);
                    }
                }
                break;


            case Constants.Forgot_Password:
                break;
        }


    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        mSignInClicked = false;
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    0).show();
            return;
        }
        if (!mIntentInProgress) {
            /* Store the ConnectionResult for later usage */
            mConnectionResult = result;

            // The user has already clicked 'sign-in' so we attempt to
            // resolve all
            // errors until the user is signed in, or they cancel.l
            if (mSignInClicked) {
                resolveSignInError();
            }
        }
    }


    private void resolveSignInError() {
        if (mConnectionResult.hasResolution())
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mSignInClicked = false;
        getProfileInformation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}