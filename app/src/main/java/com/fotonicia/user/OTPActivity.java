package com.fotonicia.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fotonicia.R;
import com.fotonicia.common.Constants;
import com.fotonicia.components.CustomRegularEditText;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.components.CustomRegularTextView;
import com.fotonicia.home.HomeActivity;
import com.fotonicia.interfaces.DisplableInterface;
import com.fotonicia.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class OTPActivity extends FragmentActivity implements View.OnClickListener, DisplableInterface {
    private Activity activity;
    private RelativeLayout layBack, laySendOTP;
    private LinearLayout layGoBack, laySkip, layOTP;
    private CustomRegularHGRTextView txtHeader;
    private CustomRegularEditText txtMobileNo;
    private CustomRegularHGRTextView txtResend;
    private ImageView checkForOTP;
    private String mobileNumber;
    private boolean isOTP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        // preferenceManager = new PreferenceManager(getApplicationContext());
        setContentView(R.layout.activity_otp);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.layout_title_bar);
        activity = this;

        getContents();
    }

    private void getContents() {

        layBack = (RelativeLayout) findViewById(R.id.layBack);
        layBack.setOnClickListener(this);

        layGoBack = (LinearLayout) findViewById(R.id.layGoBack);
        layGoBack.setOnClickListener(this);

        txtHeader = (CustomRegularHGRTextView) findViewById(R.id.txtHeader);
        txtHeader.setText("OTP");

        laySkip = (LinearLayout) findViewById(R.id.laySkip);
        laySkip.setOnClickListener(this);

        checkForOTP = (ImageView) findViewById(R.id.checkForOTP);
        checkForOTP.setOnClickListener(this);
        checkForOTP.setBackgroundResource(R.drawable.uncheckbox);
        isOTP = false;

        Bundle bundle = getIntent().getExtras();
        if (bundle.containsKey("isOTP")) {
            boolean otpStatus = bundle.getBoolean("isOTP");
            if (otpStatus)
                checkForOTP.setBackgroundResource(R.drawable.checkbox);
            isOTP = false;
        }

        txtMobileNo = (CustomRegularEditText) findViewById(R.id.txtMobileNo);

        txtResend = (CustomRegularHGRTextView) findViewById(R.id.txtResend);
        txtResend.setOnClickListener(this);

        laySendOTP = (RelativeLayout) findViewById(R.id.laySendOTP);
        laySendOTP.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layGoBack:
                finish();
                break;

            case R.id.laySkip:
                Intent intent_reg = new Intent(this, HomeActivity.class);
                startActivity(intent_reg);
                finish();
                break;

            case R.id.layBack:
                finish();
                break;

            case R.id.checkForOTP:
                if (!isOTP) {
                    checkForOTP.setBackgroundResource(R.drawable.checkbox);
                    isOTP = true;
                } else {
                    checkForOTP.setBackgroundResource(R.drawable.uncheckbox);
                    isOTP = false;
                    Intent intent=new Intent();
                    setResult(2,intent);
                    finish();
                }
                break;

            case R.id.txtResend:
                resendOTP();
                break;

            case R.id.laySendOTP:
                sendOTP();
                break;
        }

    }

    private void sendOTP() {
        mobileNumber = txtMobileNo.getText().toString().trim();
        if(mobileNumber.length()>0)
            setDataModelOTP(mobileNumber);
    }

    private void resendOTP() {
        //setDataModelResendOTP(preference.getUderId);
    }

    private void setDataModelOTP(String mobileNumber) {
            JSONObject obj1 = new JSONObject();

            try {
                obj1.put("mobile", mobileNumber);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
               /* MainController controller = new MainController(activity, Constants.Verify_OTP, this, WebServiceConstants.FORGOT_PASSWORD, true,"1");
                controller.RequestService(obj1.toString(),WebServiceConstants.POST);
                Utils.printValue(obj1.toString(), "");*/
            }
            catch (Exception e){
                e.printStackTrace();
            }
    }

    private void setDataModelResendOTP(String userId) {
        JSONObject obj1 = new JSONObject();

        try {
            obj1.put("userId", userId);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
               /* MainController controller = new MainController(activity, Constants.Resend_OTP, this, WebServiceConstants.FORGOT_PASSWORD, true,"1");
                controller.RequestService(obj1.toString(),WebServiceConstants.POST);
                Utils.printValue(obj1.toString(), "");*/
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void setScreenData(Object obj, byte type, String Responce) {
        switch(type)
        {
            case Constants.Verify_OTP:
                break;

            case Constants.Resend_OTP:
                break;
        }

    }
}