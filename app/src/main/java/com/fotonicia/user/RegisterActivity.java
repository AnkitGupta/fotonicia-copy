package com.fotonicia.user;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fotonicia.R;
import com.fotonicia.common.Constants;
import com.fotonicia.common.WebConstants;
import com.fotonicia.components.CustomBoldTextView;
import com.fotonicia.components.CustomRegularEditText;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.components.CustomRegularTextView;
import com.fotonicia.controllers.MainController;
import com.fotonicia.home.HomeActivity;
import com.fotonicia.interfaces.DisplableInterface;
import com.fotonicia.util.NotificationUtils;
import com.fotonicia.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends FragmentActivity implements View.OnClickListener, DisplableInterface {
    private Activity activity;
    private RelativeLayout layBack, laySignUp;
    private LinearLayout layCountryCode;
    private CustomRegularHGRTextView txtHeader;
    private CustomRegularTextView txtLogin, txtCountryCode;
    private RadioGroup radioGender;
    private RadioButton radioMale, radioFemale;
    private CustomRegularEditText txtFirstName, txtLastName, txtMobileNo, txtEmail_Mobile, txtPassword;
    String email_reg, password_reg, name_reg, last_name_reg, phoneNumber_reg, gender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_register);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.layout_title_bar);
        activity = this;

        getContents();
    }

    private void getContents() {

        layBack = (RelativeLayout) findViewById(R.id.layBack);
        layBack.setOnClickListener(this);

        txtHeader = (CustomRegularHGRTextView) findViewById(R.id.txtHeader);
        txtHeader.setText("Sign Up");

        txtLogin = (CustomRegularTextView) findViewById(R.id.txtLogin);
        txtLogin.setOnClickListener(this);

        layCountryCode = (LinearLayout) findViewById(R.id.layCountryCode);
        layCountryCode.setOnClickListener(this);

        radioGender = (RadioGroup) findViewById(R.id.radioGender);
        radioMale = (RadioButton) findViewById(R.id.radioMale);
        radioFemale = (RadioButton) findViewById(R.id.radioFemale);

        radioGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = radioGender.getCheckedRadioButtonId();

                // find which radio button is selected
                if (checkedId == R.id.radioMale) {
                    gender = "Male";
                    Toast.makeText(getApplicationContext(), "choice: Male", Toast.LENGTH_SHORT).show();
                }  else {
                    gender = "Female";
                    Toast.makeText(getApplicationContext(), "choice: Female", Toast.LENGTH_SHORT).show();
                }

/*
                if(selectedId == radioMale.getId()) {
                    textView.setText("You chose 'Sound' option");
                } else if(selectedId == radioFemale.getId()) {
                    textView.setText("You chose 'Vibration' option");
                } else {
                    textView.setText("You chose 'Silent' option");
                }*/
            }
        });


        /*
        * public void onSubmit(View v) {
        RadioButton rb = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
        Toast.makeText(MainActivity.this, rb.getText(), Toast.LENGTH_SHORT).show();
    }
        * */

        txtFirstName = (CustomRegularEditText) findViewById(R.id.txtFirstName);
        txtLastName = (CustomRegularEditText) findViewById(R.id.txtLastName);
        txtMobileNo = (CustomRegularEditText) findViewById(R.id.txtMobileNo);
        txtEmail_Mobile = (CustomRegularEditText) findViewById(R.id.txtEmail_Mobile);
        txtPassword = (CustomRegularEditText) findViewById(R.id.txtPassword);

        laySignUp = (RelativeLayout) findViewById(R.id.laySignUp);
        laySignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layBack:
                finish();
                break;

            case R.id.txtLogin:
                Intent intent_login = new Intent(this, LoginActivity.class);
                startActivity(intent_login);
                finish();
                break;

            case R.id.laySignUp:
                registrationValidation();
                break;

            case R.id.layCountryCode:
                getCountryCodeDialog();
                break;

        }

    }

    private void getCountryCodeDialog() {
        final android.app.Dialog dialog = new android.app.Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_forgot_password);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

               /*DisplayMetrics displaymetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                int width = (int) ((int)displaymetrics.widthPixels * 0.9);
                int height = (int) ((int)displaymetrics.heightPixels * 0.7);
                dialog.getWindow().setLayout(width,height);*/

        ImageView imgCancel = (ImageView) dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        RelativeLayout laySubmit = (RelativeLayout) dialog.findViewById(R.id.laySubmit);
        laySubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomRegularEditText txtEmail = (CustomRegularEditText) dialog.findViewById(R.id.txtEmail);
                String email = txtEmail.getText().toString().trim();
                if(email!=null){
                    if (Utils.isNetworkAvailable(activity)) {
                        Toast.makeText(activity, "Submitted", Toast.LENGTH_SHORT).show();
                      //  setDataModelForgot(email_login);
                    } else {
                        NotificationUtils.showNetworkStatusToast(activity);
                    }
                }
            }
        });

        // this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }


    /*Registration Validation*/
    private void registrationValidation() {

        name_reg = txtFirstName.getText().toString().trim();
        last_name_reg = txtLastName.getText().toString().trim();
        email_reg = txtEmail_Mobile.getText().toString().trim();
        password_reg = txtPassword.getText().toString().trim();
        phoneNumber_reg = txtMobileNo.getText().toString().trim();
        int selectedId = radioGender.getCheckedRadioButtonId();

        if (!Utils.isNetworkAvailable(this))
            NotificationUtils.showNetworkStatusToast(this);
        else if (TextUtils.isEmpty(name_reg)) {
            NotificationUtils.showNotificationToast(this, "Please enter your first name.");
            txtFirstName.requestFocus();
        }
        else if (TextUtils.isEmpty(last_name_reg)) {
            NotificationUtils.showNotificationToast(this, "Please enter your last name name.");
            txtLastName.requestFocus();
        }
        else if (TextUtils.isEmpty(email_reg)) {
            NotificationUtils.showNotificationToast(this, "Please enter your email.");
            txtEmail_Mobile.requestFocus();
        }
        else if (!Utils.isValidEmail(email_reg)) {
            NotificationUtils.showNotificationToast(this, "Please enter your valid email.");
            txtEmail_Mobile.requestFocus();
        }
        else if (TextUtils.isEmpty(phoneNumber_reg)) {
            NotificationUtils.showNotificationToast(this, "Please enter your phone number");
            txtMobileNo.requestFocus();
        }
        else if (phoneNumber_reg.length() < 10) {
            NotificationUtils.showNotificationToast(this, "Phone number cannot be less than 10 digits");
            txtMobileNo.requestFocus();
        }
        else if (password_reg.length() < 7) {
            NotificationUtils.showNotificationToast(this, "Password cannot be less than 6 characters");
            txtPassword.requestFocus();
        }
        else if (selectedId==-1)
            NotificationUtils.showNotificationToast(this, "Please select your gender");
        else {
            //calling web service for account validation
            // NotificationUtils.showNotificationToast(activity, getString(R.string.successful_register));
            //  callRegistrationService(name, email, "", phoneNumber, city, Constants.REGISTRATION_CUSTOM);
            setDataModelRegistration(name_reg, last_name_reg, email_reg, password_reg,phoneNumber_reg, gender, "0", "ejhfejfh112" );
        }
    }


    private void setDataModelRegistration(String firstName, String lastName, String email, String password,
                                          String phoneNumber, String gender, String login_Type, String socialId) {
        JSONObject obj1 = new JSONObject();
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            int verCode = pInfo.versionCode;
            // obj2 = Utils.getDeviceInfo(this,WebConstants.LOGIN);


            obj1.put("hash_key", "api123456789");
            obj1.put("firstname", firstName);
            obj1.put("lastname", lastName);
           /* if (email != null && email.equalsIgnoreCase("fb")) {
                obj1.put("UserPassword", fbLoginDet.getFbId());
            } else {
                obj1.put("UserPassword", pswd);
            }*/

            obj1.put("email", email);
            obj1.put("password", password);
            obj1.put("phone", phoneNumber);
            obj1.put("gender", gender);
            obj1.put("loginType", "0");
            obj1.put("social_id", "fghfg1235");


          /*  if(preferenceManager.getRegId()!=null)
                obj1.put("DeviceRegistrationID", preferenceManager.getRegId());
            obj1.put("DeviceType", "Android");
            obj1.put("AppVersion", verCode);
            obj1.put("DeviceModel", Build.MODEL);
            obj1.put("DeviceName", Build.DEVICE);
            obj1.put("DeviceOSversion", System.getProperty("os.version"));*/


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject jobj = new JSONObject(obj1.toString());
           /* MainController controller = new MainController(RegisterActivity.this, WebConstants.REGISTRATION, this, Constants.Registration);
            controller.RequestService(jobj);*/
            Log.e("Json Object", "" + jobj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void setScreenData(Object obj, byte type, String Responce) {

    }
}