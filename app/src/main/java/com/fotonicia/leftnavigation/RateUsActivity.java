package com.fotonicia.leftnavigation;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import com.fotonicia.R;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.util.Utils;

/**
 * Created by moumita on 5/11/2017.
 */

public class RateUsActivity extends Activity implements View.OnClickListener {
    private Activity activity;
    private WebView myWebView;
    private ImageView imgBack;
    private CustomRegularHGRTextView txtSubCatName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_rateus);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.layout_title_bar_subcat);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        activity = this;
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);

        txtSubCatName = (CustomRegularHGRTextView) findViewById(R.id.txtSubCatName);
        txtSubCatName.setText("RATE US ON GOOGLE PLAY STORE");
        myWebView = (WebView) findViewById(R.id.webViewRateUs);

  /*      myWebView.loadUrl("https://www.facebook.com/fotonicia");
        myWebView.getSettings().setJavaScriptEnabled(true);


        myWebView.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                view.loadUrl(url);
                return true;
            }
        });*/

       /* myWebView.setWebViewClient(new MyWebViewClient());

        WebSettings webSettings = myWebView.getSettings();

        webSettings.setJavaScriptEnabled(true);*/


        String url = "https://play.google.com/store/apps/details?id=in.kpis.immortalize";

        if (Utils.isNetworkAvailable(activity)) {
            myWebView.setWebViewClient(new MyWebViewClient());
            myWebView.setWebChromeClient(new WebChromeClient());

            myWebView.getSettings().setJavaScriptEnabled(true);
            myWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
            myWebView.getSettings().setBuiltInZoomControls(true);
            myWebView.getSettings().setSupportZoom(true);

            myWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            myWebView.setScrollbarFadingEnabled(false);

            myWebView.setInitialScale(30);
            myWebView.loadUrl(url);

        } else {
            Toast.makeText(activity, "No internet", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                finish();
                break;



        }
    }

    // Use When the user clicks a link from a web page in your WebView
    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            /*if (!pDialog.isShowing()) {
                pDialog.show();
            }*/

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            //view.loadUrl(url);
            System.out.println("on finish");
           /* if (pDialog.isShowing()) {
                pDialog.dismiss();
            }*/

        }
    }
}
