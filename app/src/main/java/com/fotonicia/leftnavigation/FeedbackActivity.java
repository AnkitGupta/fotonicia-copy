package com.fotonicia.leftnavigation;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.fotonicia.R;
import com.fotonicia.adapter.AboutUsAdapter;
import com.fotonicia.adapter.OffersAdapter;
import com.fotonicia.components.CustomRegularHGREditText;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.model.AboutUsDetails;
import com.fotonicia.model.OffersDetails;
import com.fotonicia.model.SubCategoryItemDetails;
import com.fotonicia.util.NotificationUtils;
import com.fotonicia.util.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FeedbackActivity extends Activity implements View.OnClickListener {

    PreferenceManager preferenceManager;
    private Activity activity;
    private ImageView imgBack, imgYoutube;
    private RelativeLayout laySubmit;

    private CustomRegularHGRTextView txtSubCatName;
    private String strRateService, strOfferSatisfaction, strRatePrices, strDeliveryTimeline, strCustomerSupport, strProductRecommendation, strDesignOptions, strOrderProcess,
            strNextIntroduce, strExpectations;
    private RadioGroup rgRateService, rgOfferSatisfaction, rgRatePrices, rgDeliveryTimeline, rgCustomerSupport, rgProductRecommendation, rgDesignOptions, rgOrderProcess;
    private RadioButton rbRS_VeryGood, rbRS_Good, rbRS_Fair, rbRS_Poor, rbRS_VeryPoor,
            rbOS_VeryGood, rbOS_Good, rbOS_Fair, rbOS_Poor, rbOS_VeryPoor,
            rbRP_VeryGood, rbRP_Good, rbRP_Fair, rbRP_Poor, rbRP_VeryPoor,
            rbDT_VeryGood, rbDT_Good, rbDT_Fair, rbDT_Poor, rbDT_VeryPoor,
            rbCS_VeryGood, rbCS_Good, rbCS_Fair, rbCS_Poor, rbCS_VeryPoor,
            rbPR_Yes, rbPR_No,
            rbDO_VeryGood, rbDO_Good, rbDO_Fair, rbDO_Poor, rbDO_VeryPoor,
            rbOP_VeryGood, rbOP_Good, rbOP_Fair, rbOP_Poor, rbOP_VeryPoor;
    private CustomRegularHGREditText edtNextIntroduce, edtExpectations;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        activity = this;
        preferenceManager = new PreferenceManager(getApplicationContext());

        setContentView(R.layout.activity_feedback);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.layout_title_bar_subcat);
      /*  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

        getContents();

    }

    private void getContents() {

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);

        txtSubCatName = (CustomRegularHGRTextView) findViewById(R.id.txtSubCatName);
        txtSubCatName.setText("FEEDBACK FORM");


        /*1 How would you rate your overall experience with our services?*/
        rgRateService = (RadioGroup) findViewById(R.id.rgRateService);
        rbRS_VeryGood = (RadioButton) findViewById(R.id.rbRS_VeryGood);
        rbRS_Good = (RadioButton) findViewById(R.id.rbRS_Good);
        rbRS_Fair = (RadioButton) findViewById(R.id.rbRS_Fair);
        rbRS_Poor = (RadioButton) findViewById(R.id.rbRS_Poor);
        rbRS_VeryPoor = (RadioButton) findViewById(R.id.rbRS_VeryPoor);


        rgRateService.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = rgRateService.getCheckedRadioButtonId();

                // find which radio button is selected
                if (checkedId == R.id.rbRS_VeryGood) {
                    strRateService = "Very Good";
                    Toast.makeText(getApplicationContext(), "choice: Very Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbRS_Good) {
                    strRateService = "Good";
                    Toast.makeText(getApplicationContext(), "choice: Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbRS_Fair) {
                    strRateService = "Fair";
                    Toast.makeText(getApplicationContext(), "choice: Fair", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbRS_Poor) {
                    strRateService = "Poor";
                    Toast.makeText(getApplicationContext(), "choice: Poor", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbRS_VeryPoor) {
                    strRateService = "Very Poor";
                    Toast.makeText(getApplicationContext(), "choice: Very Poor", Toast.LENGTH_SHORT).show();
                }
            }
        });



        /*2 How satisfied are you with the comprehensiveness of our offer?*/
        rgOfferSatisfaction = (RadioGroup) findViewById(R.id.rgOfferSatisfaction);
        rbOS_VeryGood = (RadioButton) findViewById(R.id.rbOS_VeryGood);
        rbOS_Good = (RadioButton) findViewById(R.id.rbOS_Good);
        rbOS_Fair = (RadioButton) findViewById(R.id.rbOS_Fair);
        rbOS_Poor = (RadioButton) findViewById(R.id.rbOS_Poor);
        rbOS_VeryPoor = (RadioButton) findViewById(R.id.rbOS_VeryPoor);


        rgOfferSatisfaction.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = rgOfferSatisfaction.getCheckedRadioButtonId();

                // find which radio button is selected
                if (checkedId == R.id.rbOS_VeryGood) {
                    strOfferSatisfaction = "Very Good";
                    Toast.makeText(getApplicationContext(), "choice: Very Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbOS_Good) {
                    strOfferSatisfaction = "Good";
                    Toast.makeText(getApplicationContext(), "choice: Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbOS_Fair) {
                    strOfferSatisfaction = "Fair";
                    Toast.makeText(getApplicationContext(), "choice: Fair", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbOS_Poor) {
                    strOfferSatisfaction = "Poor";
                    Toast.makeText(getApplicationContext(), "choice: Poor", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbOS_VeryPoor) {
                    strOfferSatisfaction = "Very Poor";
                    Toast.makeText(getApplicationContext(), "choice: Very Poor", Toast.LENGTH_SHORT).show();
                }
            }
        });



            /*3 How would you rate our prices?*/
        rgRatePrices = (RadioGroup) findViewById(R.id.rgRatePrices);
        rbRP_VeryGood = (RadioButton) findViewById(R.id.rbRP_VeryGood);
        rbRP_Good = (RadioButton) findViewById(R.id.rbRP_Good);
        rbRP_Fair = (RadioButton) findViewById(R.id.rbRP_Fair);
        rbRP_Poor = (RadioButton) findViewById(R.id.rbRP_Poor);
        rbRP_VeryPoor = (RadioButton) findViewById(R.id.rbRP_VeryPoor);


        rgRatePrices.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = rgRatePrices.getCheckedRadioButtonId();

                // find which radio button is selected
                if (checkedId == R.id.rbRP_VeryGood) {
                    strRatePrices = "Very Good";
                    Toast.makeText(getApplicationContext(), "choice: Very Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbRP_Good) {
                    strRatePrices = "Good";
                    Toast.makeText(getApplicationContext(), "choice: Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbRP_Fair) {
                    strRatePrices = "Fair";
                    Toast.makeText(getApplicationContext(), "choice: Fair", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbRP_Poor) {
                    strRatePrices = "Poor";
                    Toast.makeText(getApplicationContext(), "choice: Poor", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbRP_VeryPoor) {
                    strRatePrices = "Very Poor";
                    Toast.makeText(getApplicationContext(), "choice: Very Poor", Toast.LENGTH_SHORT).show();
                }
            }
        });



            /*4 How satisfied are you with the timelines of order delivery?*/
        rgDeliveryTimeline = (RadioGroup) findViewById(R.id.rgDeliveryTimeline);
        rbDT_VeryGood = (RadioButton) findViewById(R.id.rbDT_VeryGood);
        rbDT_Good = (RadioButton) findViewById(R.id.rbDT_Good);
        rbDT_Fair = (RadioButton) findViewById(R.id.rbDT_Fair);
        rbDT_Poor = (RadioButton) findViewById(R.id.rbDT_Poor);
        rbDT_VeryPoor = (RadioButton) findViewById(R.id.rbDT_VeryPoor);


        rgDeliveryTimeline.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = rgDeliveryTimeline.getCheckedRadioButtonId();

                // find which radio button is selected
                if (checkedId == R.id.rbDT_VeryGood) {
                    strDeliveryTimeline = "Very Good";
                    Toast.makeText(getApplicationContext(), "choice: Very Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbDT_Good) {
                    strDeliveryTimeline = "Good";
                    Toast.makeText(getApplicationContext(), "choice: Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbDT_Fair) {
                    strDeliveryTimeline = "Fair";
                    Toast.makeText(getApplicationContext(), "choice: Fair", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbDT_Poor) {
                    strDeliveryTimeline = "Poor";
                    Toast.makeText(getApplicationContext(), "choice: Poor", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbDT_VeryPoor) {
                    strDeliveryTimeline = "Very Poor";
                    Toast.makeText(getApplicationContext(), "choice: Very Poor", Toast.LENGTH_SHORT).show();
                }
            }
        });



            /*5 How satisfied are you with the customer support?*/
        rgCustomerSupport = (RadioGroup) findViewById(R.id.rgCustomerSupport);
        rbCS_VeryGood = (RadioButton) findViewById(R.id.rbCS_VeryGood);
        rbCS_Good = (RadioButton) findViewById(R.id.rbCS_Good);
        rbCS_Fair = (RadioButton) findViewById(R.id.rbCS_Fair);
        rbCS_Poor = (RadioButton) findViewById(R.id.rbCS_Poor);
        rbCS_VeryPoor = (RadioButton) findViewById(R.id.rbCS_VeryPoor);


        rgCustomerSupport.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = rgCustomerSupport.getCheckedRadioButtonId();

                // find which radio button is selected
                if (checkedId == R.id.rbCS_VeryGood) {
                    strCustomerSupport = "Very Good";
                    Toast.makeText(getApplicationContext(), "choice: Very Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbCS_Good) {
                    strCustomerSupport = "Good";
                    Toast.makeText(getApplicationContext(), "choice: Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbCS_Fair) {
                    strCustomerSupport = "Fair";
                    Toast.makeText(getApplicationContext(), "choice: Fair", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbCS_Poor) {
                    strCustomerSupport = "Poor";
                    Toast.makeText(getApplicationContext(), "choice: Poor", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbCS_VeryPoor) {
                    strCustomerSupport = "Very Poor";
                    Toast.makeText(getApplicationContext(), "choice: Very Poor", Toast.LENGTH_SHORT).show();
                }
            }
        });


            /*6 Would you recommend our product/service to other people?*/
        rgProductRecommendation = (RadioGroup) findViewById(R.id.rgProductRecommendation);
        rbPR_Yes = (RadioButton) findViewById(R.id.rbPR_Yes);
        rbPR_No = (RadioButton) findViewById(R.id.rbPR_No);


        rgProductRecommendation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = rgProductRecommendation.getCheckedRadioButtonId();

                // find which radio button is selected
                if (checkedId == R.id.rbPR_Yes) {
                    strProductRecommendation = "Yes";
                    Toast.makeText(getApplicationContext(), "choice: Very Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbPR_No) {
                    strProductRecommendation = "No";
                    Toast.makeText(getApplicationContext(), "choice: Good", Toast.LENGTH_SHORT).show();
                }
            }
        });



            /*7 How satisfied are you with our design options?*/
        rgDesignOptions = (RadioGroup) findViewById(R.id.rgDesignOptions);
        rbDO_VeryGood = (RadioButton) findViewById(R.id.rbDO_VeryGood);
        rbDO_Good = (RadioButton) findViewById(R.id.rbDO_Good);
        rbDO_Fair = (RadioButton) findViewById(R.id.rbDO_Fair);
        rbDO_Poor = (RadioButton) findViewById(R.id.rbDO_Poor);
        rbDO_VeryPoor = (RadioButton) findViewById(R.id.rbDO_VeryPoor);


        rgDesignOptions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = rgDesignOptions.getCheckedRadioButtonId();

                // find which radio button is selected
                if (checkedId == R.id.rbDO_VeryGood) {
                    strDesignOptions = "Very Good";
                    Toast.makeText(getApplicationContext(), "choice: Very Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbDO_Good) {
                    strDesignOptions = "Good";
                    Toast.makeText(getApplicationContext(), "choice: Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbDO_Fair) {
                    strDesignOptions = "Fair";
                    Toast.makeText(getApplicationContext(), "choice: Fair", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbDO_Poor) {
                    strDesignOptions = "Poor";
                    Toast.makeText(getApplicationContext(), "choice: Poor", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbDO_VeryPoor) {
                    strDesignOptions = "Very Poor";
                    Toast.makeText(getApplicationContext(), "choice: Very Poor", Toast.LENGTH_SHORT).show();
                }
            }
        });



            /*8 Are you able to follow the order process easily?*/
        rgOrderProcess = (RadioGroup) findViewById(R.id.rgOrderProcess);
        rbOP_VeryGood = (RadioButton) findViewById(R.id.rbOP_VeryGood);
        rbOP_Good = (RadioButton) findViewById(R.id.rbOP_Good);
        rbOP_Fair = (RadioButton) findViewById(R.id.rbOP_Fair);
        rbOP_Poor = (RadioButton) findViewById(R.id.rbOP_Poor);
        rbOP_VeryPoor = (RadioButton) findViewById(R.id.rbOP_VeryPoor);


        rgOrderProcess.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = rgOrderProcess.getCheckedRadioButtonId();

                // find which radio button is selected
                if (checkedId == R.id.rbOP_VeryGood) {
                    strOrderProcess = "Very Good";
                    Toast.makeText(getApplicationContext(), "choice: Very Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbOP_Good) {
                    strOrderProcess = "Good";
                    Toast.makeText(getApplicationContext(), "choice: Good", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbOP_Fair) {
                    strOrderProcess = "Fair";
                    Toast.makeText(getApplicationContext(), "choice: Fair", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbOP_Poor) {
                    strOrderProcess = "Poor";
                    Toast.makeText(getApplicationContext(), "choice: Poor", Toast.LENGTH_SHORT).show();
                } else if (checkedId == R.id.rbOP_VeryPoor) {
                    strOrderProcess = "Very Poor";
                    Toast.makeText(getApplicationContext(), "choice: Very Poor", Toast.LENGTH_SHORT).show();
                }
            }
        });


        edtNextIntroduce = (CustomRegularHGREditText) findViewById(R.id.edtNextIntroduce);
        edtExpectations = (CustomRegularHGREditText) findViewById(R.id.edtExpectations);
        laySubmit = (RelativeLayout) findViewById(R.id.laySubmit);
        laySubmit.setOnClickListener(this);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {

        }

    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                finish();
                break;

            case R.id.laySubmit:
                feedbackValidations();
                break;

        }
    }

    private void feedbackValidations() {
        strNextIntroduce = edtNextIntroduce.getText().toString().trim();
        strExpectations = edtExpectations.getText().toString().trim();

        if (rgRateService.getCheckedRadioButtonId() == -1) {
            NotificationUtils.showNotificationToast(this, "Please select your option for How would you rate your overall experience with our services?");
        } else if (rgOfferSatisfaction.getCheckedRadioButtonId() == -1) {
            NotificationUtils.showNotificationToast(this, "Please select your option for How satisfied are you with the comprehensiveness of our offer?");
        } else if (rgRatePrices.getCheckedRadioButtonId() == -1) {
            NotificationUtils.showNotificationToast(this, "Please select your option for How would you rate our prices?");
        } else if (rgDeliveryTimeline.getCheckedRadioButtonId() == -1) {
            NotificationUtils.showNotificationToast(this, "Please select your option for How satisfied are you with the timelines of order delivery?");
        } else if (rgCustomerSupport.getCheckedRadioButtonId() == -1) {
            NotificationUtils.showNotificationToast(this, "Please select your option for How satisfied are you with the customer support?");
        } else if (rgProductRecommendation.getCheckedRadioButtonId() == -1) {
            NotificationUtils.showNotificationToast(this, "Please select your option for Would you recommend our product/service to other people?");
        } else if (rgDesignOptions.getCheckedRadioButtonId() == -1) {
            NotificationUtils.showNotificationToast(this, "Please select your option for How satisfied are you with our design options?");
        } else if (rgOrderProcess.getCheckedRadioButtonId() == -1) {
            NotificationUtils.showNotificationToast(this, "Please select your option for Are you able to follow the order process easily?");
        } else if (TextUtils.isEmpty(edtNextIntroduce.getText().toString().trim())) {
            NotificationUtils.showNotificationToast(this, "Please fill in which products we should introduce next?");
        } else if (TextUtils.isEmpty(edtExpectations.getText().toString().trim())) {
            NotificationUtils.showNotificationToast(this, "Please fill in What should we change in order to live upto your expectations?");
        } else {
            setDataModelFeedback(strRateService, strOfferSatisfaction, strRatePrices, strDeliveryTimeline, strCustomerSupport, strProductRecommendation, strDesignOptions,
                    strOrderProcess, strNextIntroduce, strExpectations);
        }
    }

    private void setDataModelFeedback(String strRateService, String strOfferSatisfaction, String strRatePrices, String strDeliveryTimeline, String strCustomerSupport,
                                      String strProductRecommendation, String strDesignOptions, String strOrderProcess, String strNextIntroduce, String strExpectations) {

        JSONObject obj1 = new JSONObject();
        try {
            obj1.put("hash_key", "api123456789");
            obj1.put("user_id", preferenceManager.getUserid());
            obj1.put("service_experience", strRateService);
            obj1.put("offer_comprehensiveness", strOfferSatisfaction);
            obj1.put("rate_prices", strRatePrices);
            obj1.put("delivery_timelines", strDeliveryTimeline);
            obj1.put("customer_support", strCustomerSupport);
            obj1.put("product_recommendations", strProductRecommendation);
            obj1.put("design_options", strDesignOptions);
            obj1.put("order_process", strOrderProcess);
            obj1.put("next_introduce", strNextIntroduce);
            obj1.put("expectations", strExpectations);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject jobj = new JSONObject(obj1.toString());
           /* MainController controller = new MainController(RegisterActivity.this, WebConstants.REGISTRATION, this, Constants.Registration);
            controller.RequestService(jobj);*/
            Log.e("Json Object", "" + jobj);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }


}
