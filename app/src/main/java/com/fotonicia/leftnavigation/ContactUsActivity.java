package com.fotonicia.leftnavigation;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

import com.fotonicia.R;
import com.fotonicia.components.CustomRegularTextView;

/**
 * Created by moumita on 5/9/2017.
 */

public class ContactUsActivity extends AppCompatActivity {

    private Context mContext;
    private Activity mActivity;

    private CoordinatorLayout mCLayout;
    // private FloatingActionButton mFAB;

    private Toolbar mToolbar;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private CustomRegularTextView txtArticleDesc;
    private WebView webView;
    String mimeType = "text/html";
    String encoding = "utf-8";
    String htmlText = "<html><body><section id=\"post-23935\" class=\"post-23935 post type-post status-publish format-standard hentry category-eat-well tag-healthy-eating tag-vitamins-minerals-nutrients\"><article><header class=\"entry-header\"><h2 class=\"entry-title\">7 Reasons Why Gud (Jaggery) Is So Good For You!</h2><!-- .entry-title --></header><div class=\"entry-meta-bar clearfix\"><div class=\"entry-meta\"><span class=\"byline\"> <span class=\"author vcard\"><a class=\"url fn n\" href=\"http://www.1mg.com/articles/author/1mgofficial/\">1mg Team</a></span></span><span class=\"posted-on\"><a href=\"http://www.1mg.com/articles/7-reasons-why-gud-jaggery-is-so-good-for-you/\" rel=\"bookmark\"><time class=\"entry-date published\" datetime=\"2017-03-26T15:30:56+00:00\">March 26, 2017</time><time class=\"updated\" datetime=\"2017-03-24T15:57:15+00:00\">March 24, 2017</time></a></span>   \t\t<span class=\"category\"><a href=\"http://www.1mg.com/articles/category/eat-well/\" rel=\"category tag\">Eat Well</a></span></div><!-- .entry-meta --></div><div class=\"entry-content clearfix\"><p><img class=\"alignnone size-full wp-image-23937\" src=\"http://www.1mg.com/articles/wp-content/uploads/2017/03/rsz_shutterstock_384556114.jpg\" alt=\"rsz_shutterstock_384556114\" width=\"630\" height=\"380\"></p><p>Jaggery or <em>Gud</em>&nbsp;has been an integral part of Indian culture for centuries. It is prepared by boiling sugarcane juice or date palm syrup or even the sap of coconut trees. With the introduction of refined white sugar, the use of this amber colored sweetener in our cuisine has diminished over the years. However, unlike white sugar, jaggery is unrefined and packed with nutrients.</p><p>Here are some amazing health benefits of gud (jaggery):</p><p><b>1 . Relieves Constipation</b></p><p>Jaggery is known to activate our digestive enzymes enabling proper digestion of food. It facilitates bowel movement and relieves constipation. Have a glass of warm milk with half a spoonful of jaggery to relieve constipation. Or else, sprinkle black pepper on a bite size piece of jaggery and have it after dinner for best results.</p><p><strong>2 . Prevents&nbsp;</strong><b>Anemia</b></p><p>Thanks to its unrefined nature, Jaggery is loaded with iron which is the building block of haemoglobin in blood and thus boosts the red blood cell count. It works well for pregnant women.</p><p><strong>3 . Strengthens Bones</strong></p><p>Jaggery has a high calcium content owing to which it imparts solidity to the bones. Try pieces of jaggery with salads for the bursts of flavour and goodness for the body.</p><p><strong>4 . Boosts Immunity</strong></p><p>Being rich in essential minerals like zinc and selenium jaggery boosts our immune systems against infections. The antioxidants contained in this sweet treat fights free radicals in our body slowing the ageing process. Have two teaspoons of grated jaggery with warm milk daily to improve Immunity.</p><p><b>5 . Elevates The Mood</b></p><p>Feeling low due to something bad that happened or suffering from frequent mood swings before or during periods? Binging on a small piece of jaggery can be the unlikely cure as it induces the production of endorphins or happy hormones which relaxes the nerves easing tension off the muscles.</p><p><strong>6 . Nourishes The Skin</strong></p><p>Jaggery is rich in many essential minerals that nourish the skin and prevents the occurrence of acne and pimples. The antioxidants keep the skin glowing and wrinkle free. Apply a mixture of jaggery and ground seasame seeds on the face once a week for healthy skin.</p><p><strong>7 . Regulates Blood Pressure</strong></p><p>Jaggery contains potassium and sodium which keep our blood pressure in check.&nbsp;Also, potassium reduces the water retention capability of the body thereby keeping the weighing scale on a check. A habit of consuming jaggery daily in any form will work wonders for the body.</p><p>Patients with complaints of poor heart health, blood sugar, diabetes and obesity related issues should follow their doctor’s advice about consumption of jaggery. One can expect to reap &nbsp;benefits by limiting the consumption to about 2-3 teaspoonful daily.</p><div id=\"wpdevar_comment_1\" style=\"width:100%;text-align:left;\"><span style=\"padding: 10px;font-size:22px;font-family:Times New Roman,Times,Georgia,serif;color:#000000;\">Facebook Comments</span><div class=\"fb-comments fb_iframe_widget fb_iframe_widget_fluid\" data-href=\"http://www.1mg.com/articles/7-reasons-why-gud-jaggery-is-so-good-for-you/\" data-order-by=\"social\" data-numposts=\"5\" data-width=\"100%\" style=\"display:block;\" fb-xfbml-state=\"rendered\"><span style=\"height: 173px;\"><iframe id=\"f2862043d50073\" name=\"f32b23444234714\" scrolling=\"no\" title=\"Facebook Social Plugin\" class=\"fb_ltr fb_iframe_widget_lift\" src=\"https://www.facebook.com/plugins/comments.php?api_key=212401972496595&amp;channel_url=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2F1FegrZjPbq3.js%3Fversion%3D42%23cb%3Df3842a920c3c62c%26domain%3Dwww.1mg.com%26origin%3Dhttps%253A%252F%252Fwww.1mg.com%252Ff3f91835bfba928%26relation%3Dparent.parent&amp;href=http%3A%2F%2Fwww.1mg.com%2Farticles%2F7-reasons-why-gud-jaggery-is-so-good-for-you%2F&amp;locale=en_US&amp;numposts=5&amp;order_by=social&amp;sdk=joey&amp;version=v2.3&amp;width=100%25\" style=\"border: none; overflow: hidden; height: 173px; width: 100%;\"></iframe></span></div></div><div class=\"tags\"><a href=\"http://www.1mg.com/articles/tag/healthy-eating/\" rel=\"tag\">Healthy Eating</a>, <a href=\"http://www.1mg.com/articles/tag/vitamins-minerals-nutrients/\" rel=\"tag\">VITAMINS MINERALS NUTRIENTS</a></div></div><ul class=\"default-wp-page clearfix\"><li class=\"previous\"><a href=\"http://www.1mg.com/articles/high-bmi-could-be-a-reason-for-an-early-death-study-reveals/\" rel=\"prev\"><span class=\"meta-nav\">←</span> High BMI Could Be A Reason For An Early Death, Study Reveals!</a></li><li class=\"next\"><a href=\"http://www.1mg.com/articles/7-amazing-health-benefits-of-adding-lean-proteins-to-your-diet/\" rel=\"next\">7 Amazing Health Benefits Of Adding Lean Proteins To Your Diet <span class=\"meta-nav\">→</span></a></li></ul><div id=\"comments\" class=\"comments-area\"></div><!-- #comments .comments-area --></article></section></body></html>";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus_collapsingtoolbar);

        // Get the application context
        mContext = getApplicationContext();
        mActivity = ContactUsActivity.this;
        /*android.app.ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/


       /* getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);*/

        // Get the widget reference from XML layout
        mCLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        //   mFAB = (FloatingActionButton) findViewById(R.id.fab);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        mToolbar.setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_layout);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        // txtArticleDesc = (CustomRegularTextView) findViewById(R.id.txtArticleDesc);

        /*txtArticleDesc.setText(Html.fromHtml("<html>\n" +
                "<body>\n" +
                "<section id=\"post-23935\" class=\"post-23935 post type-post status-publish format-standard hentry category-eat-well tag-healthy-eating tag-vitamins-minerals-nutrients\">\n" +
                "\t\t<article>\n" +
                "\n" +
                "\t\t\t\n" +
                "\t\t\t<header class=\"entry-header\">\n" +
                "    \t\t\t<h2 class=\"entry-title\">\n" +
                "    \t\t\t\t7 Reasons Why Gud (Jaggery) Is So Good For You!    \t\t\t</h2><!-- .entry-title -->\n" +
                "  \t\t</header>\n" +
                "\n" +
                "  \t\t\n" +
                "  \t\t\n" +
                "  \t\t\t<div class=\"entry-meta-bar clearfix\">\n" +
                "    \t\t\t<div class=\"entry-meta\">\n" +
                "\t    \t\t\t\t<span class=\"byline\"> <span class=\"author vcard\"><a class=\"url fn n\" href=\"http://www.1mg.com/articles/author/1mgofficial/\">1mg Team</a></span></span><span class=\"posted-on\"><a href=\"http://www.1mg.com/articles/7-reasons-why-gud-jaggery-is-so-good-for-you/\" rel=\"bookmark\"><time class=\"entry-date published\" datetime=\"2017-03-26T15:30:56+00:00\">March 26, 2017</time><time class=\"updated\" datetime=\"2017-03-24T15:57:15+00:00\">March 24, 2017</time></a></span>\t    \t\t\t\t\t             \t\t<span class=\"category\"><a href=\"http://www.1mg.com/articles/category/eat-well/\" rel=\"category tag\">Eat Well</a></span>\n" +
                "\t             \t\t    \t\t\t\t    \t\t\t</div><!-- .entry-meta -->\n" +
                "    \t\t</div>\n" +
                "\n" +
                "\t\t\t\t\n" +
                "\t\t\t\t\n" +
                "  \t\t\t<div class=\"entry-content clearfix\">\n" +
                "    \t\t\t<p><img class=\"alignnone size-full wp-image-23937\" src=\"http://www.1mg.com/articles/wp-content/uploads/2017/03/rsz_shutterstock_384556114.jpg\" alt=\"rsz_shutterstock_384556114\" width=\"630\" height=\"380\"></p>\n" +
                "<p>Jaggery or <em>Gud</em>&nbsp;has been an integral part of Indian culture for centuries. It is prepared by boiling sugarcane juice or date palm syrup or even the sap of coconut trees. With the introduction of refined white sugar, the use of this amber colored sweetener in our cuisine has diminished over the years. However, unlike white sugar, jaggery is unrefined and packed with nutrients.</p>\n" +
                "<p>Here are some amazing health benefits of gud (jaggery):</p>\n" +
                "<p><b>1 . Relieves Constipation</b></p>\n" +
                "<p>Jaggery is known to activate our digestive enzymes enabling proper digestion of food. It facilitates bowel movement and relieves constipation. Have a glass of warm milk with half a spoonful of jaggery to relieve constipation. Or else, sprinkle black pepper on a bite size piece of jaggery and have it after dinner for best results.</p>\n" +
                "<p><strong>2 . Prevents&nbsp;</strong><b>Anemia</b></p>\n" +
                "<p>Thanks to its unrefined nature, Jaggery is loaded with iron which is the building block of haemoglobin in blood and thus boosts the red blood cell count. It works well for pregnant women.</p>\n" +
                "<p><strong>3 . Strengthens Bones</strong></p>\n" +
                "<p>Jaggery has a high calcium content owing to which it imparts solidity to the bones. Try pieces of jaggery with salads for the bursts of flavour and goodness for the body.</p>\n" +
                "<p><strong>4 . Boosts Immunity</strong></p>\n" +
                "<p>Being rich in essential minerals like zinc and selenium jaggery boosts our immune systems against infections. The antioxidants contained in this sweet treat fights free radicals in our body slowing the ageing process. Have two teaspoons of grated jaggery with warm milk daily to improve Immunity.</p>\n" +
                "<p><b>5 . Elevates The Mood</b></p>\n" +
                "<p>Feeling low due to something bad that happened or suffering from frequent mood swings before or during periods? Binging on a small piece of jaggery can be the unlikely cure as it induces the production of endorphins or happy hormones which relaxes the nerves easing tension off the muscles.</p>\n" +
                "<p><strong>6 . Nourishes The Skin</strong></p>\n" +
                "<p>Jaggery is rich in many essential minerals that nourish the skin and prevents the occurrence of acne and pimples. The antioxidants keep the skin glowing and wrinkle free. Apply a mixture of jaggery and ground seasame seeds on the face once a week for healthy skin.</p>\n" +
                "<p><strong>7 . Regulates Blood Pressure</strong></p>\n" +
                "<p>Jaggery contains potassium and sodium which keep our blood pressure in check.&nbsp;Also, potassium reduces the water retention capability of the body thereby keeping the weighing scale on a check. A habit of consuming jaggery daily in any form will work wonders for the body.</p>\n" +
                "<p>Patients with complaints of poor heart health, blood sugar, diabetes and obesity related issues should follow their doctor’s advice about consumption of jaggery. One can expect to reap &nbsp;benefits by limiting the consumption to about 2-3 teaspoonful daily.</p>\n" +
                "<div id=\"wpdevar_comment_1\" style=\"width:100%;text-align:left;\">\n" +
                "\t\t<span style=\"padding: 10px;font-size:22px;font-family:Times New Roman,Times,Georgia,serif;color:#000000;\">Facebook Comments</span>\n" +
                "\t\t<div class=\"fb-comments fb_iframe_widget fb_iframe_widget_fluid\" data-href=\"http://www.1mg.com/articles/7-reasons-why-gud-jaggery-is-so-good-for-you/\" data-order-by=\"social\" data-numposts=\"5\" data-width=\"100%\" style=\"display:block;\" fb-xfbml-state=\"rendered\"><span style=\"height: 173px;\"><iframe id=\"f2862043d50073\" name=\"f32b23444234714\" scrolling=\"no\" title=\"Facebook Social Plugin\" class=\"fb_ltr fb_iframe_widget_lift\" src=\"https://www.facebook.com/plugins/comments.php?api_key=212401972496595&amp;channel_url=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2F1FegrZjPbq3.js%3Fversion%3D42%23cb%3Df3842a920c3c62c%26domain%3Dwww.1mg.com%26origin%3Dhttps%253A%252F%252Fwww.1mg.com%252Ff3f91835bfba928%26relation%3Dparent.parent&amp;href=http%3A%2F%2Fwww.1mg.com%2Farticles%2F7-reasons-why-gud-jaggery-is-so-good-for-you%2F&amp;locale=en_US&amp;numposts=5&amp;order_by=social&amp;sdk=joey&amp;version=v2.3&amp;width=100%25\" style=\"border: none; overflow: hidden; height: 173px; width: 100%;\"></iframe></span></div></div>\t\t\t\t\t\t\t<div class=\"tags\">\n" +
                "\t\t\t\t\t\t\t\t<a href=\"http://www.1mg.com/articles/tag/healthy-eating/\" rel=\"tag\">Healthy Eating</a>, <a href=\"http://www.1mg.com/articles/tag/vitamins-minerals-nutrients/\" rel=\"tag\">VITAMINS MINERALS NUTRIENTS</a>\t\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t\t  \t\t\t</div>\n" +
                "\n" +
                "  \t\t\t\t\t\t<ul class=\"default-wp-page clearfix\">\n" +
                "\t\t\t\t<li class=\"previous\"><a href=\"http://www.1mg.com/articles/high-bmi-could-be-a-reason-for-an-early-death-study-reveals/\" rel=\"prev\"><span class=\"meta-nav\">←</span> High BMI Could Be A Reason For An Early Death, Study Reveals!</a></li>\n" +
                "\t\t\t\t<li class=\"next\"><a href=\"http://www.1mg.com/articles/7-amazing-health-benefits-of-adding-lean-proteins-to-your-diet/\" rel=\"next\">7 Amazing Health Benefits Of Adding Lean Proteins To Your Diet <span class=\"meta-nav\">→</span></a></li>\n" +
                "\t\t\t</ul>\n" +
                "\t\t\n" +
                "<div id=\"comments\" class=\"comments-area\">\n" +
                "\n" +
                "\t\n" +
                "\t\n" +
                "\t\n" +
                "</div><!-- #comments .comments-area -->\n" +
                "\t\t</article>\n" +
                "\t</section>" +
                "\t</body>\n" +
                "</html>"));//less than nougat

       *//* txtArticleDesc.setText(Html.fromHtml("<html>\n" +
                "\" +\n" +
                "                \"<body>\\n\" +\n" +
                "                \"<section id=\\\"post-23935\\\" class=\\\"post-23935 post type-post status-publish format-standard hentry category-eat-well tag-healthy-eating tag-vitamins-minerals-nutrients\\\">\\n\" +\n" +
                "                \"\\t\\t<article>\\n\" +\n" +
                "                \"\\n\" +\n" +
                "                \"\\t\\t\\t\\n\" +\n" +
                "                \"\\t\\t\\t<header class=\\\"entry-header\\\">\\n\" +\n" +
                "                \"    \\t\\t\\t<h2 class=\\\"entry-title\\\">\\n\" +\n" +
                "                \"    \\t\\t\\t\\t7 Reasons Why Gud (Jaggery) Is So Good For You!    \\t\\t\\t</h2><!-- .entry-title -->\\n\" +\n" +
                "                \"  \\t\\t</header>\\n\" +\n" +
                "                \"\\n\" +\n" +
                "                \"  \\t\\t\\n\" +\n" +
                "                \"  \\t\\t\\n\" +\n" +
                "                \"  \\t\\t\\t<div class=\\\"entry-meta-bar clearfix\\\">\\n\" +\n" +
                "                \"    \\t\\t\\t<div class=\\\"entry-meta\\\">\\n\" +\n" +
                "                \"\\t    \\t\\t\\t\\t<span class=\\\"byline\\\"> <span class=\\\"author vcard\\\"><a class=\\\"url fn n\\\" href=\\\"http://www.1mg.com/articles/author/1mgofficial/\\\">1mg Team</a></span></span><span class=\\\"posted-on\\\"><a href=\\\"http://www.1mg.com/articles/7-reasons-why-gud-jaggery-is-so-good-for-you/\\\" rel=\\\"bookmark\\\"><time class=\\\"entry-date published\\\" datetime=\\\"2017-03-26T15:30:56+00:00\\\">March 26, 2017</time><time class=\\\"updated\\\" datetime=\\\"2017-03-24T15:57:15+00:00\\\">March 24, 2017</time></a></span>\\t    \\t\\t\\t\\t\\t             \\t\\t<span class=\\\"category\\\"><a href=\\\"http://www.1mg.com/articles/category/eat-well/\\\" rel=\\\"category tag\\\">Eat Well</a></span>\\n\" +\n" +
                "                \"\\t             \\t\\t    \\t\\t\\t\\t    \\t\\t\\t</div><!-- .entry-meta -->\\n\" +\n" +
                "                \"    \\t\\t</div>\\n\" +\n" +
                "                \"\\n\" +\n" +
                "                \"\\t\\t\\t\\t\\n\" +\n" +
                "                \"\\t\\t\\t\\t\\n\" +\n" +
                "                \"  \\t\\t\\t<div class=\\\"entry-content clearfix\\\">\\n\" +\n" +
                "                \"    \\t\\t\\t<p><img class=\\\"alignnone size-full wp-image-23937\\\" src=\\\"http://www.1mg.com/articles/wp-content/uploads/2017/03/rsz_shutterstock_384556114.jpg\\\" alt=\\\"rsz_shutterstock_384556114\\\" width=\\\"630\\\" height=\\\"380\\\"></p>\\n\" +\n" +
                "                \"<p>Jaggery or <em>Gud</em>&nbsp;has been an integral part of Indian culture for centuries. It is prepared by boiling sugarcane juice or date palm syrup or even the sap of coconut trees. With the introduction of refined white sugar, the use of this amber colored sweetener in our cuisine has diminished over the years. However, unlike white sugar, jaggery is unrefined and packed with nutrients.</p>\\n\" +\n" +
                "                \"<p>Here are some amazing health benefits of gud (jaggery):</p>\\n\" +\n" +
                "                \"<p><b>1 . Relieves Constipation</b></p>\\n\" +\n" +
                "                \"<p>Jaggery is known to activate our digestive enzymes enabling proper digestion of food. It facilitates bowel movement and relieves constipation. Have a glass of warm milk with half a spoonful of jaggery to relieve constipation. Or else, sprinkle black pepper on a bite size piece of jaggery and have it after dinner for best results.</p>\\n\" +\n" +
                "                \"<p><strong>2 . Prevents&nbsp;</strong><b>Anemia</b></p>\\n\" +\n" +
                "                \"<p>Thanks to its unrefined nature, Jaggery is loaded with iron which is the building block of haemoglobin in blood and thus boosts the red blood cell count. It works well for pregnant women.</p>\\n\" +\n" +
                "                \"<p><strong>3 . Strengthens Bones</strong></p>\\n\" +\n" +
                "                \"<p>Jaggery has a high calcium content owing to which it imparts solidity to the bones. Try pieces of jaggery with salads for the bursts of flavour and goodness for the body.</p>\\n\" +\n" +
                "                \"<p><strong>4 . Boosts Immunity</strong></p>\\n\" +\n" +
                "                \"<p>Being rich in essential minerals like zinc and selenium jaggery boosts our immune systems against infections. The antioxidants contained in this sweet treat fights free radicals in our body slowing the ageing process. Have two teaspoons of grated jaggery with warm milk daily to improve Immunity.</p>\\n\" +\n" +
                "                \"<p><b>5 . Elevates The Mood</b></p>\\n\" +\n" +
                "                \"<p>Feeling low due to something bad that happened or suffering from frequent mood swings before or during periods? Binging on a small piece of jaggery can be the unlikely cure as it induces the production of endorphins or happy hormones which relaxes the nerves easing tension off the muscles.</p>\\n\" +\n" +
                "                \"<p><strong>6 . Nourishes The Skin</strong></p>\\n\" +\n" +
                "                \"<p>Jaggery is rich in many essential minerals that nourish the skin and prevents the occurrence of acne and pimples. The antioxidants keep the skin glowing and wrinkle free. Apply a mixture of jaggery and ground seasame seeds on the face once a week for healthy skin.</p>\\n\" +\n" +
                "                \"<p><strong>7 . Regulates Blood Pressure</strong></p>\\n\" +\n" +
                "                \"<p>Jaggery contains potassium and sodium which keep our blood pressure in check.&nbsp;Also, potassium reduces the water retention capability of the body thereby keeping the weighing scale on a check. A habit of consuming jaggery daily in any form will work wonders for the body.</p>\\n\" +\n" +
                "                \"<p>Patients with complaints of poor heart health, blood sugar, diabetes and obesity related issues should follow their doctor’s advice about consumption of jaggery. One can expect to reap &nbsp;benefits by limiting the consumption to about 2-3 teaspoonful daily.</p>\\n\" +\n" +
                "                \"\\n\" +\n" +
                "                \"\\t\\t</article>\\n\" +\n" +
                "                \"\\t</section>\\n\" +\n" +
                "                \"\\t</body>\\n\" +\n" +
                "                \"</html>", Html.FROM_HTML_MODE_COMPACT));// equal or above than nougat*/

        webView = (WebView)findViewById(R.id.webview);
        webView.loadData(htmlText, mimeType, encoding);


        // Set the support action bar


        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Set a title for collapsing toolbar layout
        //  mCollapsingToolbarLayout.setTitle("BEAUTIFUL BIRD");


        // Set a click listener for Floating Action Button
     /*   mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Initialize a new Snackbar instance
                Snackbar snackbar = Snackbar.make(mCLayout, "This is a Snackbar", Snackbar.LENGTH_LONG);
                // Display the Snackbar
                snackbar.show();
            }
        });*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}