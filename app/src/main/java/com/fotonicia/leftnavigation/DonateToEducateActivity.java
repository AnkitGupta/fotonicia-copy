package com.fotonicia.leftnavigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.fotonicia.R;
import com.fotonicia.adapter.AboutUsAdapter;
import com.fotonicia.adapter.DonateToEducateAdapter;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.model.AboutUsDetails;
import com.fotonicia.model.DonateToEducateDetails;
import com.fotonicia.model.SubCategoryItemDetails;
import com.fotonicia.util.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

public class DonateToEducateActivity extends Activity implements View.OnClickListener {

    PreferenceManager preferenceManager;
    private Activity activity;
    private ImageView imgBack, imgYoutube;
    private List<SubCategoryItemDetails> subCatDetails = new ArrayList<>();
    private RecyclerView recyclerView;
    private DonateToEducateAdapter mAdapter;
    private RelativeLayout layCreateItem, layDetailsItem, layItemSizes;

    private CustomRegularHGRTextView txtSubCatName;
    private DonateToEducateDetails donateToEducateDetails = new DonateToEducateDetails();
    private List<DonateToEducateDetails> arrDonateToEducate = new ArrayList<>();






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        activity = this;
        preferenceManager = new PreferenceManager(getApplicationContext());

        setContentView(R.layout.activity_donate_to_educate);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.layout_title_bar_subcat);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getContents();

    }

    private void getContents() {

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);

        txtSubCatName = (CustomRegularHGRTextView) findViewById(R.id.txtSubCatName);
        txtSubCatName.setText("DONATE 2 EDUCATE");

        recyclerView = (RecyclerView) findViewById(R.id.donate_educate_recycler_view);
        mAdapter = new DonateToEducateAdapter(arrDonateToEducate);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setHasFixedSize(true);


    }

    private void getSubCategoryItems() {
        SubCategoryItemDetails det = new SubCategoryItemDetails("1", "METAL PRINT");
        subCatDetails.add(det);

        det = new SubCategoryItemDetails("2", "MAGNET PRINT");
        subCatDetails.add(det);

        det = new SubCategoryItemDetails("3", "PHOTO-PAPER PRINT");
        subCatDetails.add(det);

        det = new SubCategoryItemDetails("4", "POSTERS");
        subCatDetails.add(det);

        det = new SubCategoryItemDetails("5", "CALENDERS");
        subCatDetails.add(det);

        det = new SubCategoryItemDetails("6", "GREETING CARDS");
        subCatDetails.add(det);

        det = new SubCategoryItemDetails("7", "CALENDERS");
        subCatDetails.add(det);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {

        }

    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                finish();
                break;



        }
    }





    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }



}
