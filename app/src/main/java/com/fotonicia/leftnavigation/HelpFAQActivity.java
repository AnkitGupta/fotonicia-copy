package com.fotonicia.leftnavigation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.fotonicia.R;
import com.fotonicia.components.CustomRegularHGRTextView;
import com.fotonicia.components.CustomRegularTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by moumita on 5/9/2017.
 */

public class HelpFAQActivity extends Activity implements View.OnClickListener {


    private RelativeLayout layBack, layTopProduct, layTopRefund, layTopFAQ, layTopAbout, layTopLegal, layTopTerms, layTopPrivacy;
    private LinearLayout layBottomProduct, layBottomRefund, layBottomFAQ, layBottomAbout, layBottomLegal, layBottomTerms, layBottomPrivacy;
    private CustomRegularTextView txtDescProduct, txtDescRefund, txtDescFAQ, txtDescAbout, txtDescLegal, txtDescTerms, txtDescPrivacy;
    private CustomRegularHGRTextView txtSubCatName;
    private Activity activity;
    private ImageView imgToggleProduct, imgToggleRefund, imgToggleFAQ, imgToggleAbout, imgToggleLegal, imgToggleTerms, imgTogglePrivacy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        activity = this;

        setContentView(R.layout.activity_helpfaq);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.layout_title_bar_subcat);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        getContents();

    }

    private void getContents() {



        txtSubCatName = (CustomRegularHGRTextView) findViewById(R.id.txtSubCatName);
        txtSubCatName.setText("HELP & FAQ");

        layBack = (RelativeLayout) findViewById(R.id.layBack);
        layBack.setOnClickListener(this);
        layBack.setVisibility(View.VISIBLE);


        /*Products*/
        imgToggleProduct = (ImageView)findViewById(R.id.imgToggleProduct);
        imgToggleProduct.setBackgroundResource(R.drawable.add);

        layBottomProduct = (LinearLayout)findViewById(R.id.layBottomProduct);
        layBottomProduct.setVisibility(View.GONE);

        txtDescProduct = (CustomRegularTextView)findViewById(R.id.txtDescProduct);

        layTopProduct = (RelativeLayout)findViewById(R.id.layTopProduct);
        layTopProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layBottomProduct.getVisibility() == View.VISIBLE) {
                    imgToggleProduct.setBackgroundResource(R.drawable.add);
                    layBottomProduct.setVisibility(View.GONE);
                } else {
                    String bodyData = "";
                    imgToggleProduct.setBackgroundResource(R.drawable.subtract);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        txtDescProduct.setText(Html.fromHtml(getString(R.string.HelpFAQ_Products)));
                    } else {
                        txtDescProduct.setText(Html.fromHtml(getString(R.string.HelpFAQ_Products)));
                    }

                    layBottomProduct.setVisibility(View.VISIBLE);
                }
            }
        });




        /*Return, Refund and Cancellation policy*/
        imgToggleRefund = (ImageView)findViewById(R.id.imgToggleRefund);
        imgToggleRefund.setBackgroundResource(R.drawable.add);

        layBottomRefund = (LinearLayout)findViewById(R.id.layBottomRefund);
        layBottomRefund.setVisibility(View.GONE);

        txtDescRefund = (CustomRegularTextView)findViewById(R.id.txtDescRefund);

        layTopRefund = (RelativeLayout)findViewById(R.id.layTopRefund);
        layTopRefund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layBottomRefund.getVisibility() == View.VISIBLE) {
                    imgToggleRefund.setBackgroundResource(R.drawable.add);
                    layBottomRefund.setVisibility(View.GONE);
                } else {
                    String bodyData = "";
                    imgToggleRefund.setBackgroundResource(R.drawable.subtract);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        txtDescRefund.setText(Html.fromHtml(getString(R.string.HelpFAQ_Refund)));
                    } else {
                        txtDescRefund.setText(Html.fromHtml(getString(R.string.HelpFAQ_Refund)));
                    }

                    layBottomRefund.setVisibility(View.VISIBLE);
                }
            }
        });



        /*FAQ*/
        imgToggleFAQ = (ImageView)findViewById(R.id.imgToggleFAQ);
        imgToggleFAQ.setBackgroundResource(R.drawable.add);

        layBottomFAQ = (LinearLayout)findViewById(R.id.layBottomFAQ);
        layBottomFAQ.setVisibility(View.GONE);

        txtDescFAQ = (CustomRegularTextView)findViewById(R.id.txtDescFAQ);

        layTopFAQ = (RelativeLayout)findViewById(R.id.layTopFAQ);
        layTopFAQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layBottomFAQ.getVisibility() == View.VISIBLE) {
                    imgToggleFAQ.setBackgroundResource(R.drawable.add);
                    layBottomFAQ.setVisibility(View.GONE);
                } else {
                    String bodyData = "";
                    imgToggleFAQ.setBackgroundResource(R.drawable.subtract);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        txtDescFAQ.setText(Html.fromHtml(getString(R.string.HelpFAQ_FAQ)));
                    } else {
                        txtDescFAQ.setText(Html.fromHtml(getString(R.string.HelpFAQ_FAQ)));
                    }

                    layBottomFAQ.setVisibility(View.VISIBLE);
                }
            }
        });



        /*About Us*/
        imgToggleAbout = (ImageView)findViewById(R.id.imgToggleAboutUs);
        imgToggleAbout.setBackgroundResource(R.drawable.add);

        layBottomAbout = (LinearLayout)findViewById(R.id.layBottomAboutUs);
        layBottomAbout.setVisibility(View.GONE);

        txtDescAbout = (CustomRegularTextView)findViewById(R.id.txtDescAboutUs);

        layTopAbout = (RelativeLayout)findViewById(R.id.layTopAboutUs);
        layTopAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layBottomAbout.getVisibility() == View.VISIBLE) {
                    imgToggleAbout.setBackgroundResource(R.drawable.add);
                    layBottomAbout.setVisibility(View.GONE);
                } else {
                    String bodyData = "";
                    imgToggleAbout.setBackgroundResource(R.drawable.subtract);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        txtDescAbout.setText(Html.fromHtml(getString(R.string.HelpFAQ_About)));
                    } else {
                        txtDescAbout.setText(Html.fromHtml(getString(R.string.HelpFAQ_About)));
                    }

                    layBottomAbout.setVisibility(View.VISIBLE);
                }
            }
        });



        /*Legal Notice*/
        imgToggleLegal = (ImageView)findViewById(R.id.imgToggleLegal);
        imgToggleLegal.setBackgroundResource(R.drawable.add);

        layBottomLegal = (LinearLayout)findViewById(R.id.layBottomLegal);
        layBottomLegal.setVisibility(View.GONE);

        txtDescLegal = (CustomRegularTextView)findViewById(R.id.txtDescLegal);

        layTopLegal = (RelativeLayout)findViewById(R.id.layTopLegal);
        layTopLegal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layBottomLegal.getVisibility() == View.VISIBLE) {
                    imgToggleLegal.setBackgroundResource(R.drawable.add);
                    layBottomLegal.setVisibility(View.GONE);
                } else {
                    String bodyData = "";
                    imgToggleLegal.setBackgroundResource(R.drawable.subtract);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        txtDescLegal.setText(Html.fromHtml(getString(R.string.HelpFAQ_Legal)));
                    } else {
                        txtDescLegal.setText(Html.fromHtml(getString(R.string.HelpFAQ_Legal)));
                    }

                    layBottomLegal.setVisibility(View.VISIBLE);
                }
            }
        });



        /*Terms of Use*/
        imgToggleTerms = (ImageView)findViewById(R.id.imgToggleTerms);
        imgToggleTerms.setBackgroundResource(R.drawable.add);

        layBottomTerms = (LinearLayout)findViewById(R.id.layBottomTerms);
        layBottomTerms.setVisibility(View.GONE);

        txtDescTerms = (CustomRegularTextView)findViewById(R.id.txtDescTerms);

        layTopTerms = (RelativeLayout)findViewById(R.id.layTopTerms);
        layTopTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layBottomTerms.getVisibility() == View.VISIBLE) {
                    imgToggleTerms.setBackgroundResource(R.drawable.add);
                    layBottomTerms.setVisibility(View.GONE);
                } else {
                    String bodyData = "";
                    imgToggleTerms.setBackgroundResource(R.drawable.subtract);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        txtDescTerms.setText(Html.fromHtml(getString(R.string.HelpFAQ_Terms)));
                    } else {
                        txtDescTerms.setText(Html.fromHtml(getString(R.string.HelpFAQ_Terms)));
                    }

                    layBottomTerms.setVisibility(View.VISIBLE);
                }
            }
        });



        /*Privacy Policy*/
        imgTogglePrivacy = (ImageView)findViewById(R.id.imgTogglePrivacy);
        imgTogglePrivacy.setBackgroundResource(R.drawable.add);

        layBottomPrivacy = (LinearLayout)findViewById(R.id.layBottomPrivacy);
        layBottomPrivacy.setVisibility(View.GONE);

        txtDescPrivacy = (CustomRegularTextView)findViewById(R.id.txtDescPrivacy);

        layTopPrivacy = (RelativeLayout)findViewById(R.id.layTopPrivacy);
        layTopPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layBottomPrivacy.getVisibility() == View.VISIBLE) {
                    imgTogglePrivacy.setBackgroundResource(R.drawable.add);
                    layBottomPrivacy.setVisibility(View.GONE);
                } else {
                    String bodyData = "";
                    imgTogglePrivacy.setBackgroundResource(R.drawable.subtract);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        txtDescPrivacy.setText(Html.fromHtml(getString(R.string.HelpFAQ_Privacy)));
                    } else {
                        txtDescPrivacy.setText(Html.fromHtml(getString(R.string.HelpFAQ_Privacy)));
                    }

                    layBottomPrivacy.setVisibility(View.VISIBLE);
                }
            }
        });

    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layBack:
                finish();
                break;

        }
    }

    @Override
    public void onBackPressed() {
       /* Intent intent =new Intent(activity,HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }



}
