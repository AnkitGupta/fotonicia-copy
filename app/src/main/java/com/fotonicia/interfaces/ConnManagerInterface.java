package com.fotonicia.interfaces;

public interface ConnManagerInterface {
	public void successResponseProcess(final String response);
	public void failedResponseProcess(final String response);
	public void excuteError(final String error);


}
  