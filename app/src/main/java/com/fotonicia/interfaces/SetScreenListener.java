package com.fotonicia.interfaces;

import org.json.JSONObject;

public interface SetScreenListener {
    public void setScreenData(final JSONObject jsonObject, final byte type, final String Response);
    public void setScreenData(final String msg);
}
