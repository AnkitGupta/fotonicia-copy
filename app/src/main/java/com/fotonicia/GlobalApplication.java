package com.fotonicia;

import android.os.Build;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.facebook.FacebookSdk;
import com.fotonicia.util.SharedPreferenceUtil;

import static com.facebook.FacebookSdk.getApplicationContext;
/*import com.android.volley.Request;
import com.android.volley.RequestQueue;*/

/**
 * Created by moumita on 3/8/2017.
 */

public class GlobalApplication extends MultiDexApplication {
    private SharedPreferenceUtil prefs;
    private static GlobalApplication app;
    private static GlobalApplication mInstance;
  //  private RequestQueue mRequestQueue;
    public static final boolean DEVELOPER_MODE = false;
    private static final String TAG = GlobalApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        MultiDex.install(this);
        prefs = new SharedPreferenceUtil(this);
        mInstance = this;
        if (DEVELOPER_MODE && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyDialog().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyDeath().build());
        }
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    public static synchronized GlobalApplication getInstance() {
        return mInstance;
    }
    /*public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }*/

    public static GlobalApplication getApp() {
        return app;
    }

    public SharedPreferenceUtil getPrefs() {
        return prefs;
    }

    public void setPrefs(SharedPreferenceUtil prefs) {
        this.prefs = prefs;
    }

/*    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }*/
}
