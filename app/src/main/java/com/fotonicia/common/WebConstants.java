package com.fotonicia.common;

public class WebConstants {
    public static final String ICONS = "9998/icons/";
    public static final String THUMBNAIL_IMAGES = "9997/Image/UserProfileThumbnails/";

    public static final String FORGOT_PASSWORD = "9999/api/authorization/PUTForgotPassword";

    public static final String VERIFY_OTP = "9999/api/authorization/GetToken";
    public static final String RESEND_OTP = "9999/api/authorization/GetOTPByRegdID";
    public static final String SERVIVES_LIST = "9998/api/services/GetServiceList/true";
    public static final String PROFILE_IMAAGE_BASE_URL = "9997/Image/UserProfileImage/";
    public static final String GET_BASIC_SERVICE_PROFILE = "9997/api/userprofile/basic/GETUserBasicProfilebyRegdID/";
    public static final String GET_BASIC_PROFILE = "9997/api/userprofile/basic/GETUserBasicProfilebyRegdID/";
    public static final String CHANGE_YOUR_NUMBER = "9999/api/authorization/ChangeUserPhoneNumber";
    public static final String CHANGE_YOUR_PASSWORD = "9999/api/authorization/ChangeUserPassword";
    public static final String DEACTIVATE_ACCOUNT = "9999/api/authorization/PutDeactivatAccount";
    public static final String PROXIMITY_SETTING = "9997/api/userprofile/basic/PutUserProximitySettings";
    public static final String GET_PROXIMITY_SETTING = "9997/api/userprofile/basic/GetUserProximitySettingsByUserID";
    public static final String UPDATE_PROFILE = "9997/api/userprofile/basic/PUTUserBasicProfile";
   // public static final String IMAGE_GALLERY = Constants.REQUEST_BASE_URL + "9997/Image/UserImageGallary/";
    public static final String IMAGE_DELETE_URL = "9997/api/userprofile/basic/DELETEUserProfileImageGalleryByRegdIDImageID";
    public static final String SEARCH_SERVICE_PROVIDER = "9997/api/search_icon_white/PostSearchUserProfiles";
    public static final String IMAGE_UPDATE_URL = "9997/api/userprofile/basic/POSTUploadUserImageGallery";
    public static final String PROFILE_IMAGE_UPDATE_URL = "9997/api/userprofile/basic/PostUploadUserImage";
    public static final String GET_AVAILABILITY = "9997/api/userprofile/basic/GetUserAvailabilitySettingByRegdID";
    public static final String AVAILABILITY_SETTING = "9997/api/userprofile/basic/PutUserAvailability";
    public static final String GET_REVIEWS = "9997/api/userprofile/GetServiceProviderRatingsByUserID";

    /*meds99 constants*/
    public static final String LOGIN = "login";
    public static final String REGISTRATION = "register";
    public static final String DASHBOARD = "getDashboard";
}
