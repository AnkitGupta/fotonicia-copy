 package com.fotonicia.common;

 import android.app.Activity;
 import android.app.Dialog;
 import android.view.Window;
 import android.widget.ImageView;

 import com.fotonicia.R;


 public class LoaderView {

    private static Activity iActivity = null;
    private Dialog dialog = null;
    private static LoaderView loadDialog = null;

    public LoaderView(){

    }

    public static LoaderView getInstance(Activity mActivity){
        iActivity = mActivity;
        loadDialog = new LoaderView();
        return loadDialog;
    }

    public void run(boolean isLocked) {
        // TODO Auto-generated method stub
        dialog = new Dialog(iActivity, android.R.style.Theme_Dialog);
        dialog.setCancelable(false);
        ImageView imageView = new ImageView(iActivity);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setBackgroundResource(R.drawable.image_for_rotation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(imageView);
        dialog.setCanceledOnTouchOutside(false);
        //  dialog.getWindow().setBackgroundDrawable(iActivity.getApplicationContext().getResources().getDrawable(R.drawable.color_drawable_1));

        dialog.setCancelable(isLocked);
        dialog.show();
//       AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
//        animationDrawable.start();
    }

    public void dismis(){
        if(dialog.isShowing())
            dialog.dismiss();
    }
}
