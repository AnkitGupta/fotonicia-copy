package com.fotonicia.common;


public class Constants {

 //Test url
 //public static String REQUEST_BASE_URL = "http://10.10.13.65:";
 //Local url
 //public static String AppBaseURL = "http://192.168.10.15:";

 //public url
 //public static String REQUEST_BASE_URL = "http://demo.meds99.com/medsmobileapi/index/";

 //public url
 //public static String REQUEST_BASE_URL = "http://14.141.174.251:";

 /*public static final byte Login = 1;
 public static final byte Forgot_Password = 2;
 public static final byte Registration = 3;
 public static final byte Verify_OTP = 4;
 public static final byte Resend_OTP = 5;
 public static final byte Services_List = 6;
 public static final byte Home_Basic_Profile = 7;
 public static final byte Change_Your_Number = 8;
 public static final byte Change_Your_Password = 9;
 public static final byte Deactivate_Account = 10;
 public static final byte Proximity_Setting = 11;
 public static final byte Get_Proximity_Setting = 12;
 public static final byte Home_Basic_Profile_Service = 13;
 public static final byte Edit_Profile = 14;
 public static final byte Image_Delete = 15;
 public static final byte Search_Service_Provider = 16;
 public static final byte Images_Update = 17;
 public static final byte Profile_Image_Update = 18;
 public static final byte Get_Availability = 19;
 public static final byte Availability_Setting = 20;
 public static final byte Get_Reviews = 21;

 public static final String Post = "2";


 public static final String CONNECTION_TIMED_OUT = "Connection timed out.";
 public static final String SOCKET_TIMED_OUT = "Socket timed out.";
 public static final String SOME_THING_WENT_WRONG = "Oops something went wrong.";

 // api response messages
 public static final byte CHANGE_MACHINE_MESSAGE = 1;
 public static final byte CHECK_MACHINE_MESSAGE = 2;
 public static final byte ROLL_NOT_EXISTS_MESSAGE = 3;
 public static final byte PROCEED_MESSAGE = 4;
 public static final byte BATCH_CODE_GENERATED_MESSAGE = 5;
 public static final byte ROLL_GENERATED_MESSAGE = 6;
 public static final byte UNREGISTERED_DEVICE_MESSAGE = 7;
 public static final byte DATA_SYNC_MESSAGE = 8;
 public static final byte DATA_SAVE_MESSAeGE = 9;
 public static final byte SUCCESSFUL_MESSAGE = 10;
 public static final byte DATA_FAILED_MESSAGE = 11;
 public static final byte SUCCESSFULLY_LOGIN_MESSAGE = 12;
 public static final byte INVALID_USER_MESSAGE = 13;
 public static final byte LOGGED_OUT_MESSAGE = 14;
 public static final byte SERVER_ERROR_MESSAGE = 15;
 public static final byte INVALID_CUSTOMER_MESSAGE = 16;
*/

 // For Login and Registration Type
 public static final String REGISTRATION_CUSTOM="custom";
 public static final String REGISTRATION_FACEBOOK="facebook";
 public static final String REGISTRATION_GOOGLE="google";

 /*Fotonicia constants*/
 public static String REQUEST_BASE_URL = "";
 public static final byte Login = 1;
 public static final byte Forgot_Password = 2;
 public static final byte Registration = 3;
 public static final byte Verify_OTP = 4;
 public static final byte Resend_OTP = 5;



}
