package com.fotonicia.photoselection;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import android.support.v7.graphics.Palette;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.fotonicia.R;
import com.fotonicia.adapter.SelectPhotoAdapter;
import com.fotonicia.model.DemoItem;
import com.fotonicia.model.ItemObjects;
import com.fotonicia.util.NotificationUtils;
import com.fotonicia.util.Utils;
import com.fotonicia.widget.DefaultListAdapter;
import com.fotonicia.widget.DemoAdapter;
import com.felipecsl.asymmetricgridview.AsymmetricGridView;
import com.felipecsl.asymmetricgridview.AsymmetricGridViewAdapter;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static android.content.ContentValues.TAG;

public class SelectPhotoActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {
    private static final boolean USE_CURSOR_ADAPTER = false;

    private AsymmetricGridView listView;
    private DemoAdapter adapter;
    private final DemoUtils demoUtils = new DemoUtils();
    private RelativeLayout layGallery, layInstagram, layFacebook, layGoogle, showInstagram, showFacebook, showGoogle;
    private LinearLayout showGallery, layoutFacebook, layoutGoogle;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private SignInButton sign_in_button;
    private boolean mSignInClicked, isLogin, mIntentInProgress;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;
    private ConnectionResult mConnectionResult;
    private ProgressDialog mProgressDialog;
    private Button mInstagramSignInButton;
    private Activity activity;
    private List<String> cameraImages = new ArrayList<>();
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    public static final String CAMERA_IMAGE_BUCKET_NAME =
            Environment.getExternalStorageDirectory().toString()
                    + "/DCIM/Camera";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_photos);
        callbackManager = CallbackManager.Factory.create();
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        activity = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationIcon(R.drawable.back_icon_grey);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //handleOnBackPress();
                finish();
            }
        });

        listView = (AsymmetricGridView) findViewById(R.id.listView);
        adapter = new DefaultListAdapter(this, demoUtils.moarItems(50));
        listView.setRequestedColumnCount(3);
        listView.setRequestedHorizontalSpacing(com.felipecsl.asymmetricgridview.Utils.dpToPx(this, 3));
        listView.setAdapter(getNewAdapter());
        listView.setDebugging(true);
        listView.setOnItemClickListener(this);


       // cameraImages = getCameraImages(activity);
       // listView = (AsymmetricGridView) findViewById(R.id.listView);
      //  listView = (RecyclerView) findViewById(R.id.recycler_view);
     //  RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
    /*    recyclerView.setHasFixedSize(true);

        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(3, 1);
        recyclerView.setLayoutManager(gaggeredGridLayoutManager);

        List<ItemObjects> gaggeredList = getListItemData();

       SelectPhotoAdapter rcAdapter = new SelectPhotoAdapter(this, cameraImages);
        recyclerView.setAdapter(rcAdapter);*/



        layGallery = (RelativeLayout) findViewById(R.id.layGallery);
        layGallery.setOnClickListener(this);

        layInstagram = (RelativeLayout) findViewById(R.id.layInstagram);
        layInstagram.setOnClickListener(this);

        layFacebook = (RelativeLayout) findViewById(R.id.layFacebook);
        layFacebook.setOnClickListener(this);

        layGoogle = (RelativeLayout) findViewById(R.id.layGoogle);
        layGoogle.setOnClickListener(this);

        showGallery = (LinearLayout) findViewById(R.id.showGallery);
        showInstagram = (RelativeLayout) findViewById(R.id.showInstagram);
        showFacebook = (RelativeLayout) findViewById(R.id.showFacebook);
        showGoogle = (RelativeLayout) findViewById(R.id.showGoogle);

        showGallery.setVisibility(View.VISIBLE);
        showInstagram.setVisibility(View.GONE);
        showFacebook.setVisibility(View.GONE);
        showGoogle.setVisibility(View.GONE);


        layoutFacebook = (LinearLayout) findViewById(R.id.layoutFacebook);
        layoutFacebook.setOnClickListener(this);

        sign_in_button = (SignInButton) findViewById(R.id.sign_in_button);
        layoutGoogle = (LinearLayout) findViewById(R.id.layoutGoogle);
        layGoogle.setOnClickListener(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(new Scope("profile"))
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();

        facebookLogin();


        mInstagramSignInButton = (Button)findViewById(R.id.instagram_sign_in_button);
        mInstagramSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInWithInstagram();
            }
        });

        //get the bitmap of the drawable image we are using as background
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.amsterdam);


        //using palette, change the color of collapsing toolbar layout
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            public void onGenerated(Palette palette) {
                int mutedColor = palette.getMutedColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
                int mutedDarkColor = palette.getDarkMutedColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark));
                int vibrantColor = palette.getVibrantColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));

                collapsingToolbarLayout.setContentScrimColor(mutedColor);
                collapsingToolbarLayout.setStatusBarScrimColor(mutedDarkColor);
                //  fab.setBackgroundTintList(ColorStateList.valueOf(vibrantColor));

            }
        });

       /* if (USE_CURSOR_ADAPTER) {
            if (savedInstanceState == null) {
                adapter = new DefaultCursorAdapter(this, demoUtils.moarItems(50));
            } else {
                adapter = new DefaultCursorAdapter(this);
            }
        } else {*/
            /*if (savedInstanceState == null) {
                adapter = new DefaultListAdapter(this, demoUtils.moarItems(50));
            } else {
                adapter = new DefaultListAdapter(this);
            }*/

       /* if (savedInstanceState == null) {
            adapter = new DefaultListAdapter(this, cameraImages);
        } else {
            adapter = new DefaultListAdapter(this);
        }*/

        // }
        /*adapter = new DefaultListAdapter(this, demoUtils.moarItems(cameraImages.size()));
        listView.setRequestedColumnCount(3);
        listView.setRequestedHorizontalSpacing(Utils.dpToPx(this, 3));
        listView.setAdapter(getNewAdapter());
        listView.setDebugging(true);
        listView.setOnItemClickListener(this);*/
    }

    /*private List<ItemObjects> getListItemData(){
        List<ItemObjects> listViewItems = new ArrayList<ItemObjects>();
        listViewItems.add(new ItemObjects("Alkane", R.drawable.one));
        listViewItems.add(new ItemObjects("Ethane", R.drawable.two));
        listViewItems.add(new ItemObjects("Alkyne", R.drawable.three));
        listViewItems.add(new ItemObjects("Benzene", R.drawable.four));
        listViewItems.add(new ItemObjects("Amide", R.drawable.one));
        listViewItems.add(new ItemObjects("Amino Acid", R.drawable.two));
        listViewItems.add(new ItemObjects("Phenol", R.drawable.three));
        listViewItems.add(new ItemObjects("Carbonxylic", R.drawable.four));
        listViewItems.add(new ItemObjects("Nitril", R.drawable.one));
        listViewItems.add(new ItemObjects("Ether", R.drawable.two));
        listViewItems.add(new ItemObjects("Ester", R.drawable.three));
        listViewItems.add(new ItemObjects("Alcohol", R.drawable.four));

        return listViewItems;
    }*/

    public static final String CAMERA_IMAGE_BUCKET_ID = getBucketId(CAMERA_IMAGE_BUCKET_NAME);

    public static String getBucketId(String path) {

        return String.valueOf(path.toLowerCase().hashCode());
    }

    public static List<String> getCameraImages(Context context) {
        final String[] projection = { MediaStore.Images.Media.DATA };
        final String selection = MediaStore.Images.Media.BUCKET_ID + " = ?";
        final String[] selectionArgs = { CAMERA_IMAGE_BUCKET_ID };
        final Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                selectionArgs,
                null);
        ArrayList<String> result = new ArrayList<String>(cursor.getCount());
        if (cursor.moveToFirst()) {
            final int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                final String data = cursor.getString(dataColumn);
                result.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

  /*  @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentOffset", demoUtils.currentOffset);
        outState.putInt("itemCount", adapter.getCount());
        for (int i = 0; i < adapter.getCount(); i++) {
            outState.putParcelable("item_" + i, (Parcelable) adapter.getItem(i));
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        demoUtils.currentOffset = savedInstanceState.getInt("currentOffset");
        int count = savedInstanceState.getInt("itemCount");
       *//*List<DemoItem> items = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            items.add((DemoItem) savedInstanceState.getParcelable("item_" + i));
        }
        adapter.setItems(items);*//*
        adapter.setItems(cameraImages);


    }*/

    @Override
    public void onItemClick(@NonNull AdapterView<?> parent, @NonNull View view,
                            int position, long id) {
        Toast.makeText(this, "Item " + position + " clicked", Toast.LENGTH_SHORT).show();
    }

    private AsymmetricGridViewAdapter getNewAdapter() {
        return new AsymmetricGridViewAdapter(this, listView, adapter);
    }

    //https://www.instagram.com/developer/clients/manage/.            //get client id here
    private void signInWithInstagram() {
        final Uri.Builder uriBuilder = new Uri.Builder();
        uriBuilder.scheme("https")
                .authority("api.instagram.com")
                .appendPath("oauth")
                .appendPath("authorize")
                .appendQueryParameter("client_id", "18a8b74da9644bd7a9294caef1c5e76c")
                .appendQueryParameter("redirect_uri", "sociallogin://redirect")
                .appendQueryParameter("response_type", "token");
        final Intent browser = new Intent(Intent.ACTION_VIEW, uriBuilder.build());
        startActivity(browser);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layGallery:
                showGallery.setVisibility(View.VISIBLE);
                showInstagram.setVisibility(View.GONE);
                showFacebook.setVisibility(View.GONE);
                showGoogle.setVisibility(View.GONE);
                break;

            case R.id.layInstagram:
                showGallery.setVisibility(View.GONE);
                showInstagram.setVisibility(View.VISIBLE);
                showFacebook.setVisibility(View.GONE);
                showGoogle.setVisibility(View.GONE);
                break;

            case R.id.layFacebook:
                showGallery.setVisibility(View.GONE);
                showInstagram.setVisibility(View.GONE);
                showFacebook.setVisibility(View.VISIBLE);
                showGoogle.setVisibility(View.GONE);
                break;

            case R.id.layGoogle:
                showGallery.setVisibility(View.GONE);
                showInstagram.setVisibility(View.GONE);
                showFacebook.setVisibility(View.GONE);
                showGoogle.setVisibility(View.VISIBLE);
                break;

            case R.id.layoutFacebook:
                if (Utils.isNetworkAvailable(this)) {
                    if (LoginManager.getInstance() != null)
                        LoginManager.getInstance().logOut();
                    loginButton.performClick();
                } else
                    NotificationUtils.showNetworkStatusToast(this);
                break;

            case R.id.layoutGoogle:
                signIn();
                sign_in_button.performClick();
                break;

            case R.id.sign_in_button:
                signIn();
                break;
        }

    }

    /*Google Sign In*/
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    protected void onStart() {
        super.onStart();
        checkForInstagramData();
        mGoogleApiClient.connect();
    }


    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
        }
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void getProfileInformation() {
        try {
            mSignInClicked = false;
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String user_id = currentPerson.getId();
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                final String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                Utils.printValue("Gmail Email-", email + " & " + personName);
                //redirectOnHomePage();
               /* if(Utils.isNetworkAvailable(activity))*//*NotificationUtils.showNotificationToast(activity,"Name="+personName+" Email="+email);*//*
                    setDataModelLogin(email, user_id, Constants.REGISTRATION_GOOGLE);
                else
                    NotificationUtils.showNetworkStatusToast(activity);*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // updateUI(false);
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getDisplayName());

            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();

            Log.e(TAG, "Name: " + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl);

           /* txtName.setText(personName);
            txtEmail.setText(email);
            Glide.with(getApplicationContext()).load(personPhotoUrl)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgProfilePic);

            updateUI(true);*/
        } else {
            // Signed out, show unauthenticated UI.
            //  updateUI(false);
        }
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {
        mSignInClicked = false;
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    0).show();
            return;
        }
        if (!mIntentInProgress) {
            /* Store the ConnectionResult for later usage */
            mConnectionResult = result;

            // The user has already clicked 'sign-in' so we attempt to
            // resolve all
            // errors until the user is signed in, or they cancel.l
            if (mSignInClicked) {
                resolveSignInError();
            }
        }
    }


    private void resolveSignInError() {
        if (mConnectionResult.hasResolution())
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mSignInClicked = false;
        getProfileInformation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    private void checkForInstagramData() {
        final Uri data = this.getIntent().getData();
        if(data != null && data.getScheme().equals("sociallogin") && data.getFragment() != null) {
            final String accessToken = data.getFragment().replaceFirst("access_token=", "");
            if (accessToken != null) {
                handleSignInResult(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        // Do nothing, just throw the access token away.
                        return null;
                    }
                });
            } else {
                //handleSignInResult(null);
            }
        }
    }

    private void handleSignInResult(Callable<Void> logout) {
        if(logout == null) {
            /* Login error */
            Toast.makeText(getApplicationContext(), "login error", Toast.LENGTH_SHORT).show();
        } else {
            /* Login success */
           /* Application.getInstance().setLogoutCallable(logout);
            startActivity(new Intent(this, LoggedInActivity.class));*/
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == RC_SIGN_IN) {
            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else
        /*Facebook Sign In*/
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /*Facebook Sign In*/
    private void facebookLogin() {
        loginButton = (LoginButton) findViewById(R.id.login_button);
        List<String> permissions = new ArrayList<>();

        Bundle params = new Bundle();
        params.putString("fields", "photos{images}");

        permissions.add("public_profile");
        permissions.add("user_friends");
        permissions.add("email");
        //if(LoginManager.getInstance()!=null)

        loginButton.setReadPermissions(permissions);
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                if (loginResult != null) {
                    try {

                        GraphRequest graphRequest = GraphRequest.newMeRequest(

                                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                                    public void onCompleted(JSONObject jsonObject, GraphResponse response) {

                                        Utils.logDebug("TEST", response.toString());
                                        if (response.getError() != null) {
                                            // handle error
                                        } else {
                                            if (jsonObject != null) {
                                                String user_id = jsonObject.optString("id");
                                               /* Utils.logDebug("Response", jsonObject.toString());
                                                String email = jsonObject.optString("email");
                                                String user_id = jsonObject.optString("id");
                                                Utils.logDebug("User Email", "Email:" + email);*/
                                                //call method to get facebook images


                                            }
                                        }
                                    }
                                })/*.executeAsync()*/;
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email");
                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();
                        Utils.printValue("Authentication Token :", "" + loginResult.getAccessToken().getToken());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    /*
                    * get profile pic of facebook
                    *
                    * */


                    /*ImageView user_picture;
                    userpicture=(ImageView)findViewById(R.id.userpicture);
                    URL img_value = null;
                    img_value = new URL("http://graph.facebook.com/"+user_id+"/picture?type=large");
                    Bitmap mIcon1 = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
                    userpicture.setImageBitmap(mIcon1);*/
                }


            }

            @Override
            public void onCancel() {
                // App code
                Utils.printValue("Facebook Login failed!!", "");
                // NotificationUtils.showNotificationToast(activity,"Failed to login with facebook");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Utils.printValue("Facebook Login failed!!", "");
                // NotificationUtils.showNotificationToast(activity, "Error in login with facebook");
            }

        });
    }




/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
           // return true;
        }
        return super.onOptionsItemSelected(item);
    }*/
}